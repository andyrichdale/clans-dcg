<?php
require APPLICATION_PATH . '/api/controllers/BaseController.php';

class Api_CardController extends Api_BaseController
{
    /* @var $cardService Clans\Service\Card */
    private $cardService;
    
    public function init() {
        parent::init();
        
        $this->cardService = new \Clans\Service\Card();
    }

    public function getAction() {
        if ($this->id) {
            $card = $this->cardService->getCardByID($this->id);
            
            if ($card) {
                $this->returnSuccess($card->toArray());
            } else {
                $this->returnError('Card not found');
            }
        } else {
            $this->returnError('ID not present');
        }
    }

    public function indexAction() {
        $filters = $this->_getParam('filters', '');

        $cards = array();
        
        foreach ($this->cardService->getCards() as $card) {
            $cards[] = $card->toArray(false, null, $this->_getParam('special', false));
        }
        
        $this->returnSuccess($cards);
    }
    
    public function deleteAction() {
    }

    public function postAction() {
        $this->returnError('Permission denied');
    }

    public function putAction() {
        $this->returnError('Permission denied');
    }
}
