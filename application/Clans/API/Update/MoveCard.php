<?php
namespace Clans\API\Update;

//require_once APPLICATION_PATH . "/Classes/API/Update/UpdateBase.php";

//use Clans\API\Update\UpdateBase;

/**
 * Update for a card moving from one zone to another
 */
class MoveCard extends UpdateBase
{
    public function __construct(\Models\Game $game, $gameCardID, $zoneFromID, $zoneToID) {
        $this->id = 4;
        
        $this->params[] = $gameCardID;
        $this->params[] = $zoneFromID;
        $this->params[] = $zoneToID;
    }
}
