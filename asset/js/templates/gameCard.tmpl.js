(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['gameCard.tmpl'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : {};

  return "    <div class=\"card\" data-clan=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.card : depth0)) != null ? stack1.clan : stack1), depth0))
    + "\" data-type=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.card : depth0)) != null ? stack1.type : stack1), depth0))
    + "\">\r\n        <div class=\"card-frame\"></div>\r\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.card : depth0)) != null ? stack1.image : stack1),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        <h3 class=\"card-title card-part\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.card : depth0)) != null ? stack1.name : stack1), depth0))
    + "</h3>\r\n        <div class=\"card-resources card-part card-stat\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.card : depth0)) != null ? stack1.cost : stack1), depth0))
    + "</div>\r\n        <div class=\"card-speed card-part card-stat\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.card : depth0)) != null ? stack1.speed : stack1), depth0))
    + "</div>\r\n        "
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.card : depth0)) != null ? stack1.hasAttack : stack1),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n        "
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.card : depth0)) != null ? stack1.hasDefence : stack1),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n        <p class=\"card-text card-part\">\r\n            "
    + ((stack1 = (helpers.gametextAsHTML || (depth0 && depth0.gametextAsHTML) || helpers.helperMissing).call(alias3,((stack1 = (depth0 != null ? depth0.card : depth0)) != null ? stack1.gametext : stack1),{"name":"gametextAsHTML","hash":{},"data":data})) != null ? stack1 : "")
    + "\r\n        </p>\r\n    </div>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "        <div class=\"card-image\">\r\n            <img src=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.card : depth0)) != null ? stack1.image : stack1), depth0))
    + "\" />\r\n        </div>\r\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"card-attack card-part card-stat\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.card : depth0)) != null ? stack1.attack : stack1), depth0))
    + "</div>";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"card-defence card-part card-stat\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.card : depth0)) != null ? stack1.defence : stack1), depth0))
    + "</div>";
},"8":function(container,depth0,helpers,partials,data) {
    return "    <div class=\"card facedown\">\r\n        <img src=\"/asset/images/card/clans-cardback.jpg\" />\r\n    </div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {};

  return "<div class=\"card-render no-events\" data-id=\""
    + container.escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\r\n"
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0.card : depth0)) != null ? stack1.type : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(8, data, 0),"data":data})) != null ? stack1 : "")
    + "    <div class=\"otherStuff\">\r\n\r\n    </div>\r\n</div>";
},"useData":true});
})();