<?php

class ErrorController extends Zend_Controller_Action
{

    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');
        
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
        
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $this->view->message = 'Page not found';
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $this->view->message = 'Application error';
                break;
        }
        
        // Log exception, if logger available
        /*if ($log = $this->getLog()) {
            $log->crit($this->view->message, $errors->exception);
        }*/
        
        // conditionally display exceptions
        //if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        //}
        
        $this->view->request   = $errors->request;
        
        /*if (false && strpos($_SERVER['HTTP_HOST'], 'local.') === false) { //disabled while private
            $html = $this->view->render('error/error.phtml');

            try {
                $mail = new Zend_Mail();
                $mail->addTo('andyrichdale@gmail.com');
                $mail->setFrom('info@hextcgdb.com', 'Hex TCG DB');
                $mail->setSubject('Error on Hex TCG DB');
                $mail->setBodyHtml($html);
                $mail->send();
            } catch (Exception $e) {

            }

            $this->_helper->viewRenderer->setRender('error-view');
        }*/
    }

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasPluginResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }


}

