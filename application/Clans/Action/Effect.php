<?php

namespace Clans\Action;

interface Effect
{
    /**
     * \Clans\Type\Realm
     * @return integer
     */
    function getRealm();
    
    /**
     * \Clans\Type\Scope
     * @return integer
     */
    function getScope();
    
    /**
     * eg card types
     * @return type 
     */
    function getAffectsType();
    
    /**
     * How many this affects
     * @return integer 
     */
    function getAffectsNumber();
    
    function getTargets();
    
    /**
     * @param \Clans\Ability $boardState
     */
    function resolve(\Clans\Ability $ability);
}
