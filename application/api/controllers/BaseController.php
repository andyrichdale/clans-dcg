<?php
require 'DoctrineInit.php';

abstract class Api_BaseController extends Zend_Rest_Controller
{
    public $id;

    /* @var $whosAsking Models\Login */
    public $whosAsking;

    public function init() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        DoctrineInit::setupDoctrine();

        $usersService = new \Clans\Service\User();

        $this->session = new Zend_Session_Namespace('clanslcg');
        $this->whosAsking = $this->session->login ? $usersService->getLoginByID($this->session->login) : null;

        $this->id = $this->_getParam('id', false);
    }

    public function returnError($message, $returnCode = 400) {
        // TODO: return 4XX header with message
        // OR: just return appropriate 4XX error header with no message
        $this->getResponse()->setHttpResponseCode($returnCode);
        $this->getResponse()->setHeader('Content-Type', 'application/json');

        $return = array('message' => $message);

        echo json_encode($return);
    }

    public function returnSuccess($thing) {
        // TODO: ensure we set the right headers
        $this->getResponse()->setHeader('Content-Type', 'application/json');

        echo json_encode($thing);
    }

    public function deleteAction() {
    }

    public function getAction() {
    }

    public function indexAction() {
    }

    public function postAction() {
    }

    public function putAction() {
    }
}
