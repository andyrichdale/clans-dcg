<?php

namespace Clans\Service;

class User extends BaseService
{
    /**
     * @param integer $id
     * @return \Models\Login
     */
    public function getLoginByID($id) {
        $query = $this->em->createQuery('SELECT l FROM \Models\Login l WHERE l.id = ?1');
        $query->setParameter(1, $id);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return false;
        }
    }

    /**
     * @param string $type
     * @param integer $identifier
     * @return \Models\Login
     */
    public function getLoginByTypeAndIdentifier($type, $identifier) {
        $query = $this->em->createQuery('SELECT l FROM \Models\Login l WHERE l.type = ?1 AND l.identifier = ?2');
        $query->setParameter(1, $type);
        $query->setParameter(2, $identifier);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return false;
        }
    }

    /**
     * @param string $username
     * @return \Models\Login
     */
    public function getLoginByUsername($username, $excludeID = false) {
        if ($excludeID) {
            $query = $this->em->createQuery('SELECT l FROM \Models\Login l WHERE l.username = ?1 AND l.id != ?2');
            $query->setParameter(2, $excludeID);
        } else {
            $query = $this->em->createQuery('SELECT l FROM \Models\Login l WHERE l.username = ?1');
        }
        $query->setParameter(1, $username);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return false;
        }
    }

    /**
     * @param string $email
     * @return \Models\Login
     */
    public function getLoginByEmail($email) {
        $qb = $this->em->createQueryBuilder();
        $qb->addSelect('l');
        /*$qb->addSelect('d');
        $qb->addSelect('dc');*/
        $qb->from('\Models\Login', 'l');
        /*$qb->leftJoin('l.selectedDeck', 'd');
        $qb->leftJoin('d.deckCards', 'dc');*/
        $qb->where($qb->expr()->eq('l.email', '?1'));
        $qb->setParameter(1, $email);

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return false;
        }
    }

    public function loginUser(Hybrid_User_Profile $loggedInUser, $type) {
        $now = date("Y-m-d");

        if ($login = $this->getLoginByTypeAndIdentifier($type, $loggedInUser->identifier)) {
            $login->setLastLogin($now);

            $append = "";
            $username = $login->getUsername() ? $login->getUsername() : $loggedInUser->displayName;
            do {
                $username .= $append;
                $append = $append === "" ? 1 : $append++;
            } while ($this->getLoginByUsername($login->getUsername(), $login->getId()));

            $email = $login->getEmail() ? $login->getEmail() : $loggedInUser->email;

            $login->setUsername($username);
            $login->setEmail($email);

            $this->em->persist($login);
            $this->em->flush();

            return $login->getId();
        } else {
            $username = $loggedInUser->displayName;
            $append = "";
            do {
                $username .= $append;
                $append = $append === "" ? 1 : $append++;
            } while ($this->getLoginByUsername($username));

            $newLogin = new \Models\Login();

            $newLogin->setType($type);
            $newLogin->setIdentifier($loggedInUser->identifier);
            $newLogin->setUsername($username);
            $newLogin->setEmail($loggedInUser->email);
            $newLogin->setFirstLogin($now);

            $this->em->persist($newLogin);
            $this->em->flush();

            return $newLogin->getId();
        }
    }

    public function setSelectedDeck(\Models\Login $login, \Models\Deck $deck) {
        $login->setSelectedDeck($deck);

        $this->em->persist($login);
        $this->em->flush();

        return $login;
    }

    public function saveLogin(\Models\Login $login) {
        $this->em->persist($login);
        $this->em->flush();
    }
}