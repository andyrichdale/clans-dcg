<?php

namespace Clans\Service;

class BaseService
{
    /** @var \Doctrine\ORM\EntityManager */
    protected $em;
    
    /* Whether or not to return a Doctrine object from a query result */
    public $returnObject = true;
    
    public function __construct() {
        $this->em = \Zend_Registry::get('em');
    }
    
    /**
     * Gets the result of a Doctrine Query Builder
     * 
     * @param \Doctrine\ORM\QueryBuilder $qb
     * @return mixed
     */
    public function getQbResult(\Doctrine\ORM\QueryBuilder $qb) {
        if ($this->returnObject) {
            return $qb->getQuery()->getResult();
        } else {
            return $qb->getQuery()->getArrayResult();
        }
    }
}