<?php

namespace Models;

/**
 * @Entity
 * @Table(name="triggers")
 * @HasLifecycleCallbacks
 */
class Trigger
{
    /**
     * @Id
     * @ManyToOne(targetEntity="\Models\Card", inversedBy="triggers")
     * @JoinColumn(name="cardID", referencedColumnName="cardID")
     * @var \Models\Card
     */
    protected $card;
    
    /**
     * @Id
     * @Column(type="integer", name="id")
     */
    protected $id;
    
    /**
     * @Column(type="text", name="trigger_name")
     */
    protected $trigger;
    
    /**
     * @ManyToOne(targetEntity="\Models\Effect", inversedBy="triggers", cascade={"persist"})
     * @JoinColumns({
     *   @JoinColumn(name="effect_cardID", referencedColumnName="cardID"),
     *   @JoinColumn(name="effect_id", referencedColumnName="id")
     * })
     * @var \Models\Effect
     */
    protected $effect;
    
    /**
     * @Column(type="text", name="source")
     */
    protected $source;
    
    public function getCard() {
        return $this->card;
    }

    public function getId() {
        return $this->id;
    }

    public function getTrigger() {
        return $this->trigger;
    }

    /**
     * @return \Models\Effect
     */
    public function getEffect() {
        return $this->effect;
    }

    public function getSource() {
        return $this->source;
    }

    public function setCard(\Models\Card $card) {
        $this->card = $card;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setTrigger($trigger) {
        $this->trigger = $trigger;
    }

    public function setEffect(\Models\Effect $effect) {
        $this->effect = $effect;
    }

    public function setSource($source) {
        $this->source = $source;
    }
        
    public function toArray($shortform = false, $callingModel = null) {
        $result = array();
        
        // TODO: consider a second argument that is the model calling this method
        // Then if \Models\Card is calling for example we can exclude $result['card']
        
        $result['id'] = $this->getId();
        $result['trigger'] = $this->getTrigger();
        $result['source'] = $this->getSource();
        $result['effect'] = $this->getEffect()->toArray(true);
        
        if (!$shortform) {
            $result['card'] = $this->getCard()->toArray(true);
        }
        
        return $result;
    }
}