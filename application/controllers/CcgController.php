<?php
require_once 'MyAction.php';

class CcgController extends MyAction
{
    const SPREADSHEET_KEY = '0AgJwY1XgxM3xdFJPMXJtcVB5ck54RXVHY3lydHJaT0E';
    const GOOGLE_API_KEY = 'AIzaSyDj0fPi_IDdzAQTs3hiflFv7bHu1Tu-09Q';

    const CSV_LINK = 'https://docs.google.com/spreadsheets/d/1yOuenlp8XMrPMXKcICdyK-ROtFce7WnktDlJuvt3-sw/export?format=csv&id=1yOuenlp8XMrPMXKcICdyK-ROtFce7WnktDlJuvt3-sw';
    const ALLIES_SHEET_ID = 11;
    const EQUIPMENT_SHEET_ID = 5;
    const EVENTS_SHEET_ID = 8;
    const STRUCTURES_SHEET_ID = 6;
    const EFFECTS_SHEET_ID = 13;
    const TRIGGERS_SHEET_ID = 15;

    const SPREADSHEET_KEY_2 = '1yOuenlp8XMrPMXKcICdyK-ROtFce7WnktDlJuvt3-sw';
    //https://docs.google.com/spreadsheets/d/1yOuenlp8XMrPMXKcICdyK-ROtFce7WnktDlJuvt3-sw/edit?usp=sharing

    private $types = array(
        'Allies' => self::ALLIES_SHEET_ID,
        //'Equipment' => self::EQUIPMENT_SHEET_ID,
        'Events' => self::EVENTS_SHEET_ID,
        'Structures' => self::STRUCTURES_SHEET_ID,
        //'Effects' => self::EFFECTS_SHEET_ID,
        //'Triggers' => self::TRIGGERS_SHEET_ID
    );

    public function indexAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //$cardTable = new Application_Model_Cards();
        $set = $this->setsService->getSetByAbbrev('HML');

        $count = 0;
        $countEffects = 0;
        $countTriggers = 0;

        foreach ($this->types as $type => $typeSheetID) {
            if ($type !== 'Allies' && $type !== 'Events' && $type !== 'Structures') continue;

            $feed = $this->getCardFeed($typeSheetID);

            //var_dump($feed);

            $headers = array_shift($feed);

            try {
                foreach ($feed as $row) {
                    switch ($type) {
                        case 'Allies':
                        case 'Equipment':
                        case 'Events':
                        case 'Structures':
                            $num = $this->getCell('#', $row, $headers);
                            $name = $this->getCell('Name', $row, $headers);
                            $import = $this->getCell('?', $row, $headers);
                            $imageId = $this->getCell('ImageId', $row, $headers);

                            if (!$name || !$num || $import == '0') {
                                continue;
                            }

                            if (!$card = $this->cardsService->getCardByNumber($set, $num)) {
                                $card = new \Models\Card();
                                $card->setSet($set);
                                $card->setNumber($num);
                                $card->setLore('');

                                echo "New card found '{$name}'<br/>";
                            }

                            $card->setName($name);
                            $card->setImageId($imageId);
                            $card->setCost($this->getCell('Res', $row, $headers));
                            $card->setSpeed($this->getCell('Speed', $row, $headers));
                            $card->setClan($this->getCell('Clan', $row, $headers));
                            $card->setGametext($this->getCell('Gametext', $row, $headers));

                            switch ($type) {
                                case 'Allies':
                                    $card->setType('Ally');

                                    //if ($row['unique']) {
                                        //$card->setUnique(true);
                                    //}

                                    $card->setAttack($this->getCell('Attack', $row, $headers));
                                    $card->setDefence($this->getCell('Defence', $row, $headers));

                                    echo "About to import card '{$name}'<br/>";

                                    $this->cardsService->saveCard($card);

                                    echo "Imported card '{$name}'<br/>";
                                    $count++;
                                break;
                                case 'Equipment':
                                    $card->setType('Equipment');

                                    $this->cardsService->saveCard($card);
                                    echo "Imported card '{$name}'<br/>";
                                    $count++;
                                break;
                                case 'Events':
                                    $card->setType('Event');

                                    //if ($row['instant']) {
                                        //$card->setInstant(true);
                                    //}

                                    $this->cardsService->saveCard($card);
                                    echo "Imported card '{$name}'<br/>";
                                    $count++;
                                break;
                                case 'Structures':
                                    $card->setType('Structure');
                                    $card->setDefence($this->getCell('Defence', $row, $headers));

                                    $this->cardsService->saveCard($card);
                                    echo "Imported card '{$name}'<br/>";
                                    $count++;
                                break;
                                default:
                                break;
                            }

                            //echo "<h1>$name</h1>";
                            //echo "<p>Clan: " . $row[array_search('Clan', $headers)] . "</p>";
                        break;
                        case 'Effects':
                            $setName = $this->getCell('Set', $row, $headers);
                            $num = $this->getCell('#', $row, $headers);
                            $effectName = $this->getCell('Effect', $row, $headers);
                            $id = $this->getCell('ID', $row, $headers);

                            if (!$setName || !$num || !$effectName || !$id) {
                                continue;
                            }

                            if ($set = $this->setsService->getSetByAbbrev($setName)) {
                                if ($card = $this->cardsService->getCardByNumber($set, $num)) {
                                    if (!$effect = $this->effectService->getEffectByNumber($card, $id)) {
                                        $effect = new \Models\Effect();
                                        $effect->setCard($card);
                                        $effect->setId((int) $id);

                                        echo "New effect found '{$effectName}'<br/>";
                                    }

                                    $effect->setEffect($effectName);
                                    $effect->setOngoing($this->getCell('Ongoing', $row, $headers));
                                    $effect->setModifies($this->getCell('Modifies', $row, $headers));
                                    $effect->setIsTriggered($this->getCell('IsTriggered', $row, $headers) > 0 ? 1 : 0);
                                    $effect->setIsSub($this->getCell('IsSub', $row, $headers) > 0 ? 1 : 0);
                                    $effect->setIsExtra($this->getCell('IsExtra', $row, $headers) > 0 ? 1 : 0);
                                    $effect->setCost($this->getCell('Cost', $row, $headers));
                                    $effect->setCost2($this->getCell('Cost2', $row, $headers));
                                    $effect->setSubEffect((int) $this->getCell('SubEffect', $row, $headers));
                                    $effect->setSubOrEffect((int) $this->getCell('SubOrEffect', $row, $headers));
                                    $effect->setExtraEffect((int) $this->getCell('ExtraEffect', $row, $headers));
                                    $effect->setValidDestinations($this->getCell('ValidDestinations', $row, $headers));
                                    $effect->setMinTargets((int) $this->getCell('MinTargets', $row, $headers));
                                    $effect->setMaxTargets((int) $this->getCell('MaxTargets', $row, $headers));
                                    $effect->setOrPrompt($this->getCell('OrPrompt', $row, $headers));
                                    $effect->setAParam($this->getCell('A', $row, $headers));
                                    $effect->setBParam($this->getCell('B', $row, $headers));

                                    $this->effectService->saveEffect($effect);

                                    echo "Imported effect {$id} for card {$card->getName()}<br/>";
                                    $countEffects++;
                                }
                            }
                        break;
                        case 'Triggers':
                            $setName = $this->getCell('Set', $row, $headers);
                            $num = $this->getCell('#', $row, $headers);
                            $triggerName = $this->getCell('Trigger', $row, $headers);
                            $id = $this->getCell('ID', $row, $headers);

                            if (!$setName || !$num || !$triggerName || !$id) {
                                continue;
                            }

                            if ($set = $this->setsService->getSetByAbbrev($setName)) {
                                if ($card = $this->cardsService->getCardByNumber($set, $num)) {
                                    if (!$trigger = $this->triggerService->getTriggerByNumber($card, $id)) {
                                        $trigger = new \Models\Trigger();
                                        $trigger->setCard($card);
                                        $trigger->setId((int) $id);

                                        echo "New trigger found '{$triggerName}'<br/>";
                                    }

                                    $effectNumber = $this->getCell('Effect', $row, $headers);

                                    if ($effect = $this->effectService->getEffectByNumber($card, $effectNumber)) {
                                        $trigger->setEffect($effect);
                                    } else {
                                        echo "EFFECT {$effectNumber} not found for card {$card->getName()}<br/>";
                                    }

                                    $trigger->setSource($this->getCell('Source', $row, $headers));
                                    $trigger->setTrigger($triggerName);

                                    $this->triggerService->saveTrigger($trigger);

                                    echo "Imported trigger {$id} for card {$card->getName()}<br/>";
                                    $countTriggers++;
                                }
                            }
                        break;
                        default:

                        break;
                    }
                }

            } catch (Exception $ex) {
                echo "<div style='color: red'>" . $ex->getMessage() . "</div>";
            }
        }

    }

    private function getCell($name, $row, $headers) {
        return isset($row[array_search($name, $headers)]) ? $row[array_search($name, $headers)] : '';
    }

    public function indexActionOLD() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //$cardTable = new Application_Model_Cards();
        $set = $this->setsService->getSetByAbbrev('HML');

        $feed = $this->getCardFeed();

        $count = 0;
        $countEffects = 0;
        $countTriggers = 0;

        try {

        foreach ($feed->entries as $worksheet) {
            $index = 0;
            foreach ($worksheet->getContentsAsRows() as $row) {
                if (in_array($worksheet->getTitle(), array('Allies', 'Equipment', 'Events', 'Structures'))) {
                    continue;
                    if (!isset($row['name']) || $row['name'] == '' || !isset($row['_cn6ca']) || $row['_cn6ca'] == '') {
                        continue;
                    }

                    $index++;

                    //if (!$card = $cardTable->getCardByNumber('HML', $row['_cn6ca'])) {
                    if (!$card = $this->cardsService->getCardByNumber($set, $row['_cn6ca'])) {
                        $card = new \Models\Card();
                        $card->setSet($set);
                        $card->setNumber($row['_cn6ca']);
                        $new = true;
                    }


                    $card->setName($row['name']);
                    $card->setCost($row['res']);
                    $card->setInfluenceB($row['b']);
                    $card->setInfluenceM($row['m']);
                    $card->setInfluenceR($row['r']);
                    $card->setInfluenceG($row['g']);
                    $card->setClan($row['clan']);
                    $card->setGametext($row['gametext']);
                }

                switch ($worksheet->getTitle()) {
                    case 'Allies':
                        $card->setType('Ally');

                        if ($row['unique']) {
                            //$card->setUnique(true);
                        }

                        $card->setAttack($row['attack']);
                        $card->setDefence($row['health']);

                        $this->cardsService->saveCard($card);
                        echo "Imported card '{$row['name']}'<br/>";
                        $count++;

                    break;
                    case 'Equipment':
                        $card->setType('Equipment');

                        $this->cardsService->saveCard($card);
                        echo "Imported card '{$row['name']}'<br/>";
                        $count++;
                    break;
                    case 'Events':
                        $card->setType('Event');

                        if ($row['instant']) {
                            //$card->setInstant(true);
                        }

                        $this->cardsService->saveCard($card);
                        echo "Imported card '{$row['name']}'<br/>";
                        $count++;
                    break;
                    case 'Structures':
                        $card->setType('Structure');
                        $card->setDefence($row['health']);

                        $this->cardsService->saveCard($card);
                        echo "Imported card '{$row['name']}'<br/>";
                        $count++;
                    break;
                    case 'Effects':
                        if (!isset($row['set']) || !isset($row['_cokwr']) || !isset($row['effect']) || !isset($row['id'])) {
                            continue;
                        }
                        if ($set = $this->setsService->getSetByAbbrev($row['set'])) {
                            if ($card = $this->cardsService->getCardByNumber($set, $row['_cokwr'])) {
                                if (!$effect = $this->effectService->getEffectByNumber($card, $row['id'])) {
                                    $effect = new \Models\Effect();
                                    $effect->setCard($card);
                                    $effect->setId((int) $row['id']);
                                }

                                $effect->setEffect($row['effect']);
                                $effect->setOngoing($row['ongoing']);
                                $effect->setModifies($row['modifies']);
                                $effect->setIsTriggered($row['istriggered'] > 0 ? 1 : 0);
                                $effect->setIsSub($row['issub'] > 0 ? 1 : 0);
                                $effect->setIsExtra($row['isextra'] > 0 ? 1 : 0);
                                $effect->setCost($row['cost']);
                                $effect->setCost2($row['cost2']);
                                $effect->setSubEffect((int) $row['subeffect']);
                                $effect->setSubOrEffect((int) $row['suboreffect']);
                                $effect->setExtraEffect((int) $row['extraeffect']);
                                $effect->setValidDestinations($row['validdestinations']);
                                $effect->setMinTargets((int) $row['mintargets']);
                                $effect->setMaxTargets((int) $row['maxtargets']);
                                $effect->setOrPrompt($row['orprompt']);
                                $effect->setAParam($row['a']);
                                $effect->setBParam($row['b']);

                                $this->effectService->saveEffect($effect);

                                echo "Imported effect {$row['id']} for card {$card->getName()}<br/>";
                                $countEffects++;
                            }
                        }
                    break;
                    case 'Triggers':
                        if (!isset($row['set']) || !isset($row['_cokwr']) || !isset($row['trigger']) || !isset($row['id'])) {
                            continue;
                        }
                        if ($set = $this->setsService->getSetByAbbrev($row['set'])) {
                            if ($card = $this->cardsService->getCardByNumber($set, $row['_cokwr'])) {
                                if (!$trigger = $this->triggerService->getTriggerByNumber($card, $row['id'])) {
                                    $trigger = new \Models\Trigger();
                                    $trigger->setCard($card);
                                    $trigger->setId((int) $row['id']);
                                }

                                if ($effect = $this->effectService->getEffectByNumber($card, $row['effect'])) {
                                    $trigger->setEffect($effect);
                                } else {
                                    echo "EFFECT {$row['effect']} not found for card {$card->getName()}<br/>";
                                }

                                $trigger->setSource($row['source']);
                                $trigger->setTrigger($row['trigger']);

                                $this->triggerService->saveTrigger($trigger);

                                echo "Imported trigger {$row['id']} for card {$card->getName()}<br/>";
                                $countTriggers++;
                            }
                        }
                    break;
                    default:
                        continue;
                    break;
                }



            }
        }
        } catch (Exception $e) {
            echo "EXCEPTION!<br/>" . $e->getMessage() . "<br/>";
        }

        echo "DONE!<br/>Imported $count cards<br/>Imported $countEffects effects<br/>Imported $countTriggers triggers<br/>";
    }

    /**
     * @return Zend_Gdata_Spreadsheets_WorksheetFeed
     */
    protected function getCardFeed($type) {
        /*function _combine_array(&$row, $key, $header) {
            $row = array_combine($header, $row);
        }

        $URL = 'https://docs.google.com/spreadsheet/ccc?key=' . self::SPREADSHEET_KEY_2 . '&output=csv&gid=' . $type;

        $array = array();

        $array = array_map('str_getcsv', file($URL));

        $header = array_shift($array);

        array_walk($array, '_combine_array', $header);

        return $array;*/

        /*$service = Zend_Gdata_Spreadsheets::AUTH_SERVICE_NAME;
        $client = Zend_Gdata_ClientLogin::getHttpClient('andyrichdale@gmail.com', 'c297da8dde05', $service);
        $spreadsheetService = new Zend_Gdata_Spreadsheets($client);

        $query = new Zend_Gdata_Spreadsheets_DocumentQuery();
        $query->setSpreadsheetKey(self::SPREADSHEET_KEY);

        return $spreadsheetService->getWorksheetFeed($query);*/

        /*require_once 'Google/autoload.php';

        $client = new Google_Client();
        $client->setDeveloperKey(self::GOOGLE_API_KEY);
        //$client->setClientId('47744292958-s65s3p4silq7sbh73oja5r9il01dffde.apps.googleusercontent.com');
        //$client->setClientSecret('p_ixVwbFfUIhiOoILnee6kG4');

        $service = new Google_Service_Drive($client);
        /* @var $files Google_Service_Drive_Files_Resource */
        /*$files = $service->files;
        $files->listFiles();*/

        $url = self::CSV_LINK . "&gid=" . $type;

        echo "<h1>Getting CSV from " . $url . "</h1>";

        $csvString = file_get_contents($url);
        
        return $this->parse_csv($csvString);
    }

    // returns a two-dimensional array or rows and fields
    // http://php.net/manual/en/function.str-getcsv.php
    function parse_csv ($csv_string, $delimiter = ",", $skip_empty_lines = true, $trim_fields = true)
    {
        $enc = preg_replace('/(?<!")""/', '!!Q!!', $csv_string);
        $enc = preg_replace_callback(
            '/"(.*?)"/s',
            function ($field) {
                return urlencode(utf8_encode($field[1]));
            },
            $enc
        );
        $lines = preg_split($skip_empty_lines ? ($trim_fields ? '/( *\R)+/s' : '/\R+/s') : '/\R/s', $enc);
        return array_map(
            function ($line) use ($delimiter, $trim_fields) {
                $fields = $trim_fields ? array_map('trim', explode($delimiter, $line)) : explode($delimiter, $line);
                return array_map(
                    function ($field) {
                        return str_replace('!!Q!!', '"', utf8_decode(urldecode($field)));
                    },
                    $fields
                );
            },
            $lines
        );
    }
}
