<?php
require APPLICATION_PATH . '/api/controllers/BaseController.php';

class Api_DeckController extends Api_BaseController
{
    /* @var $deckService Clans\Service\Deck */
    private $deckService;
    
    public function init() {
        parent::init();
        
        $this->deckService = new \Clans\Service\Deck();
    }
    
    public function getAction() {
        if ($this->id) {
            $deck = $this->deckService->getDeckByID($this->id);
            
            if ($deck) {
                $this->returnSuccess($deck->toArray());
            } else {
                $this->returnError('Deck not found');
            }
        } else {
            $this->returnError('ID not present');
        }
    }

    public function indexAction() {
        $decks = array();
        
        foreach ($this->deckService->getDecks() as $deck) {
            $decks[] = $deck->toArray(true);
        }
        
        $this->returnSuccess($decks);
    }
    
    public function deleteAction() {
        // TODO: check if logged in as the deck owner
        if ($this->id) {
            $deck = $this->deckService->getDeckByID($this->id);
            
            if ($deck) {
                $this->deckService->deleteDeck($deck);
            } else {
                $this->returnError('Deck not found');
            }
        } else {
            $this->returnError('ID not present');
        }
    }

    public function postAction() {
        // TODO: check if logged in
        
    }

    public function putAction() {
        // TODO: check if logged in
    }
}
