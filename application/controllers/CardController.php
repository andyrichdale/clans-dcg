<?php
require_once 'MyAction.php';

class CardController extends MyAction
{
    public function viewAction() {
        if ($id = $this->_getParam('id', 0)) {
            $this->view->card = $this->cardsService->getCardByID($id);
        }
    }
    
    /**
     * Render a card image with optional frame & text
     */
    public function renderAction() {
        if ($id = $this->_getParam('id', 0)) {
            if ($isGameCard = $this->_getParam('gameCard', 0)) {
                $gameCard = $this->gamesService->getGameCardByID($id);
                $card = $gameCard->getCard();
                
                $this->renderCard($card, $gameCard);
            } else {
                $card = $this->cardsService->getCardByID($id);
                
                $this->renderCard($card);
            }
        }
        
        exit;
    }
    
    public function renderAllAction() {
        $this->_helper->layout()->disableLayout();
        $this->view->cards = $this->cardsService->getCards();
    }
    
    public function makeCardRenderAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        if ($id = $this->_getParam('id', 0)) {
            if ($isGameCard = $this->_getParam('gameCard', 0)) {
                $gameCard = $this->gamesService->getGameCardByID($id);
                $card = $gameCard->getCard();
                
                $filename = $_SERVER['DOCUMENT_ROOT'] . "/asset/images/card/renders/gamecards/" . $gameCard->getId() . ".png";
                
                $this->makeCardRender($card, $gameCard, $filename);
            } else {
                $card = $this->cardsService->getCardByID($id);
                
                $filename = $_SERVER['DOCUMENT_ROOT'] . "/asset/images/card/renders/" . $card->getId() . ".png";
                
                $this->makeCardRender($card, null, $filename);
            }
        }
        exit;
    }
    
    public function makeCardRender(\Models\Card $card, \Models\Game\Card $gameCard = null, $filename) {
         $debug = $this->_getParam('debug', false);
        //testing
        /*$text = "{1} : Put a gas token on your clan hall 
{5} : Draw a card";

        foreach (explode(" ", $text) as $word) {
            echo "Checking word $word ";
            if (substr(trim($word), 0, 1) == '{' && (substr(trim($word), strlen(trim($word)) - 1, 1) == '}') || substr(trim($word), strlen(trim($word)) - 2, 2) == '},') {
                echo "Found word '$word' ";
            }  
        }

        exit;*/

        // TODO: replace 300/418 size with constants
        if ($card) {

            // Create the image
            if ($imageFile = $this->cardsService->getImageForCard($card, false, true)) { // PLAIN
                $im = imagecreatefromjpeg($imageFile);
            } else {
                $im = imagecreatetruecolor(300, 418);
                if ($this->_getParam('white', false)) {
                    $bgColour = imagecolorallocate($im, 255, 255, 255);
                } else {
                    $bgColour = imagecolorallocate($im, 0, 0, 0);
                }
                imagefill($im, 0, 0, $bgColour);
            }

            // Create the frame
            $frame = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'] . "/asset/images/frames/" . strtolower($card->getClan()) . '.png');
            //$frame = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'] . "/asset/images/frames/plain.png"); // PLAIN

            // Create the new output image
            $output = imagecreatetruecolor(300, 418);

            imagecopyresampled($output, $im, 0, 0, 0, 0, 300, 418, 300, 418);
            imagecopyresampled($output, $frame, 0, 0, 0, 0, 300, 418, 300, 418);

            $white = imagecolorallocate($im, 255, 255, 255);
            $black = imagecolorallocate($im, 0, 0, 0);

            $textColour = $white; // PLAIN
            $iconTextColour = $white;

            imagedestroy($im);
            imagedestroy($frame);

            // Set up the font
            $font = $_SERVER['DOCUMENT_ROOT'] . "/asset/fonts/Ubuntu-R.ttf";
            $fontBold = $_SERVER['DOCUMENT_ROOT'] . "/asset/fonts/Ubuntu-M.ttf";

            // Attach the reputation icons
            /*$influenceXLocation = 262;
            $influenceYLocation = 75;
            foreach (array('bain', 'monzar', 'galmath', 'ronmir') as $clan) {
                if ($influence = $card->getInfluence($clan)) {
                    $influenceIcon = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'] . "/asset/images/flag-{$clan}-sm.png");
                    imagecopyresampled($output, $influenceIcon, $influenceXLocation - 10, $influenceYLocation - 14, 0, 0, 28, 27, 28, 27);
                    imagettftext($output, 11, 0, $influenceXLocation, $influenceYLocation, $iconTextColour, $fontBold, $influence);
                    $influenceYLocation += 30;
                }
            }*/

            // Add the card text to the card image
            $title = $card->getName();
            imagettftext($output, 14, 0, 28, 40, $textColour, $font, $title);

            $type = $card->getType();
            imagettftext($output, 10, 0, 93, 67, $textColour, $font, $type);

            $cost = $card->getCost();
            imagettftext($output, 14, 0, 262, 39, $textColour, $font, $cost);

            $text = $card->getGametext();
            $newText = "";
            $newLine = "";
            $lineWidth = 0;
            $threshold = 246;
            $fontsize = strlen($card->getGametext()) > 150 ? (strlen($card->getGametext()) > 200 ? 8 : 9) : 10;
            $lineNumber = 0;

            $resourceIcon = imagecreatefromjpeg($_SERVER['DOCUMENT_ROOT'] . '/asset/images/card/icons/resource.jpg');
            //$resourceIcon = imagecreatefromjpeg($_SERVER['DOCUMENT_ROOT'] . '/asset/images/card/icons/resource-white.jpg'); // PLAIN

            if ($debug) echo "We have text '$text' ";

            foreach (explode(" ", $text) as $word) {
                if ($debug) echo "Checking word '$word' ";
                $tempLine = $newLine . ($newLine != "" ? " " : "") . $word;
                $typeSpace = imagettfbbox($fontsize, 0, $font, $tempLine);
                $lineWidth = abs($typeSpace[4] - $typeSpace[0]);

                if (strpos($word, "\n") !== false) {
                    $lineNumber ++;
                    $lineWidth = 13;
                }

                // If this word is {X}, replace with resource icon and X inside
                if (substr(trim($word), 0, 1) == '{' && (substr(trim($word), strlen(trim($word)) - 1, 1) == '}') || substr(trim($word), strlen(trim($word)) - 2, 2) == '},') {
                    if ($debug) echo "Found word '$word' at lineNumber $lineNumber and lineWidth $lineWidth ";
                    imagecopyresampled($output, $resourceIcon, 12 + $lineWidth, 306 + ($lineNumber * 16), 0, 0, 17, 17, 17, 17);
                    $word = str_replace(array('{', '}', ','), '', $word);
                    $tempLine = $newLine . ($newLine != "" ? " " : "") . " " . $word . "   ";
                }

                if ($lineWidth > $threshold) {
                    if ($debug) echo "New line! ";
                    $newText .= $newLine . "\n";
                    $newLine = $word;
                    $lineWidth = 0;
                    $lineNumber ++;
                } else {
                    $newLine = $tempLine;
                }
            }

            if ($debug) exit;

            $newText .= $newLine;

            imagettftext($output, $fontsize, 0, 28, 318, $textColour, $font, $newText);

            $attackIcon = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'] . '/asset/images/card/icons/att.png');
            $defenceIcon = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'] . '/asset/images/card/icons/def.png');

            $redIcon = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'] . '/asset/images/card/icons/red-circle.png');
            $greenIcon = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'] . '/asset/images/card/icons/green-circle.png');

            // If this is a game card, display dynamic ATK / DEF
            $att = $gameCard ? $gameCard->getAttack() : $card->getAttack();
            $def = $gameCard ? $gameCard->getDefence() : $card->getDefence();

            if ($card->getType() == 'Ally') {
                $att = $card->getAttack();
                imagecopyresampled($output, $attackIcon, 12, 250, 0, 0, 51, 51, 51, 51);
                imagettftext($output, 14, 0, 30, 278, $iconTextColour, $fontBold, $att);
            }

            if ($card->getType() == 'Ally' || $card->getType() == 'Structure') {
                $def = $card->getDefence();
                imagecopyresampled($output, $defenceIcon, 240, 241, 0, 0, 46, 60, 46, 60);
                imagettftext($output, 14, 0, 257, 278, $iconTextColour, $fontBold, $def);
            }

            if ($gameCard && $gameCard->getPainTokens() > 0) {
                // Show damage counters
                //TODO: damage icon
                imagecopyresampled($output, $redIcon, 247, 204, 0, 0, 30, 30, 30, 30);
                imagettftext($output, 14, 0, 257, 225, $iconTextColour, $fontBold, $gameCard->getPainTokens());
            }

            if ($gameCard && $gameCard->getHealthTokens() > 0) {
                // Show damage counters
                //TODO: damage icon
                imagecopyresampled($output, $greenIcon, 247, 204, 0, 0, 30, 30, 30, 30);
                imagettftext($output, 14, 0, 257, 225, $iconTextColour, $fontBold, $gameCard->getHealthTokens());
            }

            if ($gameCard && $gameCard->getWeaknessTokens() > 0) {
                // Show damage counters
                //TODO: damage icon
                imagecopyresampled($output, $redIcon, 22, 204, 0, 0, 30, 30, 30, 30);
                imagettftext($output, 14, 0, 31, 225, $iconTextColour, $fontBold, $gameCard->getWeaknessTokens());
            }

            if ($gameCard && $gameCard->getStrengthTokens() > 0) {
                // Show damage counters
                //TODO: damage icon
                imagecopyresampled($output, $greenIcon, 22, 204, 0, 0, 30, 30, 30, 30);
                imagettftext($output, 14, 0, 31, 225, $iconTextColour, $fontBold, $gameCard->getStrengthTokens());
            }

            imagealphablending($output, true);
        }

        imagepng($output, $filename);
    }
    
    public function renderCard(\Models\Card $card, \Models\Game\Card $gameCard = null) {
        if ($gameCard) {
            $filename = $_SERVER['DOCUMENT_ROOT'] . "/asset/images/card/renders/gamecards/" . $gameCard->getId() . ".png";
        } else {
            $filename = $_SERVER['DOCUMENT_ROOT'] . "/asset/images/card/renders/" . $card->getId() . ".png";
        }

        if (!file_exists($filename)) {
            //TODO: if gamecard is altered, delete gamecard image
        
           $this->makeCardRender($card, $gameCard, $filename);
        }
        
        /*$opts = array('http'=>array('header' => 'Connection: close'));
        $context = stream_context_create($opts);
            
        header('Content-Type: image/png');
        header('Content-Length: ' . filesize($filename));
        
        readfile($filename, false, $context);*/

        $im = imagecreatefrompng($filename);

        header('Content-Type: image/png');

        imagepng($im);
        imagedestroy($im);
        
        exit;
    }
}
