<?php

namespace Clans\Game\Updated;

class GameCardMoved extends \Clans\Game\BaseUpdated implements \Clans\Game\Updated
{
    private $zoneFrom;

    private $zoneTo;

    private $slotTo;

    public function __construct(\Models\Game\Card $gameCard = null, $zoneFrom = false, $zoneTo = false, $slotTo = false) {
        $this->gameCard = $gameCard;
        $this->zoneFrom = $zoneFrom;
        $this->zoneTo = $zoneTo;
        $this->slotTo = $slotTo;
    }

    public function getKey() {
        return 'GameCardMoved';
    }

    public function getData($private = false) {
        // Unset card details if this is a private request and the card is in a private zone
        $gameCard = $this->gameCard ? $this->gameCard->toArray() : 0;
        if ($gameCard && $private && !$this->gameCard->isInPlay()) {
            unset($gameCard['card']);
        }
        return array(
            'gameCard' => $gameCard,
            'zoneFrom' => $this->zoneFrom,
            'zoneTo' => $this->zoneTo,
            'slotTo' => $this->slotTo
        );
    }
}
