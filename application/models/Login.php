<?php

namespace Models;

/**
 * @Entity
 * @Table(name="logins")
 * @HasLifecycleCallbacks
 */
class Login implements BaseModel
{
    /**
     * @Id
     * @Column(type="integer", name="id")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="text", name="email")
     */
    protected $email;

    /**
     * @Column(type="text", name="username")
     */
    protected $username;

    /**
     * @Column(type="text", name="hash")
     */
    protected $hash;

    /**
     * @Column(type="text", name="type")
     */
    protected $type;

    /**
     * @Column(type="text", name="identifier")
     */
    protected $identifier;

    /**
     * @Column(type="text", name="premium")
     */
    protected $premium;

    /**
     * @Column(type="text", name="firstLogin")
     */
    protected $firstLogin;

    /**
     * @Column(type="text", name="lastLogin")
     */
    protected $lastLogin;

    /**
     * @OneToOne(targetEntity="\Models\Deck", inversedBy="selectedByLogin")
     * @JoinColumn(name="selectedDeck", referencedColumnName="deckID")
     * @var \Models\Deck
     **/
    protected $selectedDeck;

    /**
     * @OneToMany(targetEntity="\Models\Deck", mappedBy="creator", fetch="EAGER")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $decks;

    /**
     * @OneToMany(targetEntity="\Models\Chat", mappedBy="author", fetch="EAGER")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $chatMessagesSent;

    /**
     * @Column(type="integer", name="admin")
     */
    protected $admin;

    public function __construct() {
        $this->decks = new \Doctrine\Common\Collections\ArrayCollection();
        $this->chatMessagesSent = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function getHash() {
        return $this->hash;
    }

    public function setHash($hash) {
        $this->hash = $hash;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function getIdentifier() {
        return $this->identifier;
    }

    public function setIdentifier($identifier) {
        $this->identifier = $identifier;
    }

    public function getPremium() {
        return $this->premium;
    }

    public function setPremium($premium) {
        $this->premium = $premium;
    }

    public function getFirstLogin() {
        return $this->firstLogin;
    }

    public function setFirstLogin($firstLogin) {
        $this->firstLogin = $firstLogin;
        $this->setLastLogin($firstLogin);
    }

    public function getLastLogin() {
        return $this->lastLogin;
    }

    public function setLastLogin($lastLogin) {
        $this->lastLogin = $lastLogin;
    }

    public function getSelectedDeck() {
        return $this->selectedDeck;
    }

    public function setSelectedDeck(\Models\Deck $selectedDeck) {
        $this->selectedDeck = $selectedDeck;
    }

    public function getDecks() {
        return $this->decks;
    }

    function getChatMessagesSent() {
        return $this->chatMessagesSent;
    }

    public function getAdmin() {
        return $this->admin;
    }

    public function setAdmin($admin) {
        $this->admin = $admin;
    }

    public function toArray($shortform = false, $callingModel = null) {
        $result = array();

        $result['id'] = $this->getId();
        $result['username'] = $this->getUsername();

        if (!$shortform) {
            $result['email'] = $this->getEmail();
            $result['hash'] = $this->getHash();
            $result['type'] = $this->getType();
            $result['identifier'] = $this->getIdentifier();
            $result['premium'] = $this->getPremium();
            $result['firstLogin'] = $this->getFirstLogin();
            $result['lastLogin'] = $this->getLastLogin();
            $result['selectedDeck'] = $this->getSelectedDeck()->toArray(true);
            $result['decks'] = array();

            foreach ($this->getDecks() as $deck) {
                $result['decks'][] = $deck->toArray(true);
            }

            $result['chatMessagesSent'] = array();

            foreach ($this->getChatMessagesSent() as $chatMessageSent) {
                $result['chatMessagesSent'][] = $chatMessageSent->toArray(true);
            }
        }

        return $result;
    }
}