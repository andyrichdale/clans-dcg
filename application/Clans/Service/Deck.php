<?php

namespace Clans\Service;

class Deck extends BaseService
{
    /** @var \Clans\Service\Card */
    protected $cardsService;

    public function __construct() {
        parent::__construct();
        $this->cardsService = new \Clans\Service\Card();
    }

    public function getDecks() {
        $qb = $this->em->createQueryBuilder();
        $qb->select('d');
        $qb->from('\Models\Deck', 'd');
        $qb->orderBy('d.created', 'DESC');

        return $this->getQbResult($qb);
    }

    /**
     * @param integer $id
     * @return \Models\Deck
     */
    public function getDeckByID($id) {
        //$query = $this->em->createQuery('SELECT d FROM \Models\Deck d JOIN \Model\DeckCard dc WHERE d.id = ?1');
        //$query->setParameter(1, $id);

        $qb = $this->em->createQueryBuilder();
        $qb->select('d');
        $qb->from('\Models\Deck', 'd');
        $qb->leftJoin('d.deckCards', 'c');
        $qb->where('d.id = ?1');
        $qb->setParameter(1, $id);

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return false;
        }
    }

    /**
     * @param \Models\Login $login
     * @param string $name
     * @return \Models\Deck
     */
    public function getDeckByLoginAndName(\Models\Login $login, $name) {
        $query = $this->em->createQuery('SELECT d FROM \Models\Deck d WHERE d.creator = ?1 AND d.name = ?2');
        $query->setParameter(1, $login);
        $query->setParameter(2, $name);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return false;
        }
    }

    public function createDeck(\Models\Login $login) {
        $deckName = "New Deck";
        $append = 0;
        do {
            $append++;
        } while ($this->getDeckByLoginAndName($login, $deckName . " " . $append));

        $newDeck = new \Models\Deck();
        $newDeck->setName($deckName . " " . $append);
        $newDeck->setCreator($login);
        $newDeck->setMatchDeck(false);
        $newDeck->setCreated(date("Y-m-d H:i:s"));
        $newDeck->setModified(date("Y-m-d H:i:s"));
        $newDeck->setPublished(0);

        $this->em->persist($newDeck);
        $this->em->flush();

        return $newDeck;
    }

    public function deleteDeckById($deckID) {
        $deck = $this->getDeck($deckID);

        if ($deck) {
            $this->deleteDeck($deck);
        }
    }

    public function deleteDeck(\Models\Deck $deck) {
        $this->em->remove($deck);
        $this->em->flush();
    }

    public function addCardToDeck(\Models\Deck $deck, $cardID, $amount) {
        if ($card = $this->cardsService->getCardByID($cardID)) {
            $found = false;
            foreach ($deck->getDeckCards() as $deckCard) {
                if ($deckCard->getCard()->getId() == $cardID) {
                    $deckCard->incrementQuantity($amount);
                    if ($deckCard->getQuantity() <= 0) {
                        $this->em->remove($deckCard);
                    } else {
                        $this->em->persist($deckCard);
                    }
                    $found = true;
                }
            }
            if (!$found) {
                $newDeckCard = new \Models\DeckCard();
                $newDeckCard->setCard($card);
                $newDeckCard->setDeck($deck);
                $newDeckCard->setQuantity($amount);

                $this->em->persist($newDeckCard);
            }

            $this->em->flush();

            return true;
        }

        return false;
    }

    public function saveDeck(\Models\Deck $deck) {
        $this->em->persist($deck);
        $this->em->flush();
    }
}