<?php

namespace Models;

/**
 * @Entity
 * @Table(name="effects")
 * @HasLifecycleCallbacks
 */
class Effect implements BaseModel
{
    /**
     * @Id
     * @ManyToOne(targetEntity="\Models\Card", inversedBy="effects")
     * @JoinColumn(name="cardID", referencedColumnName="cardID")
     * @var \Models\Card
     */
    protected $card;
    
    /**
     * @Id
     * @Column(type="integer", name="id")
     */
    protected $id;
    
    /**
     * @OneToMany(targetEntity="\Models\Trigger", mappedBy="effect", fetch="EAGER")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $triggers;
    
    /**
     * @Column(type="text", name="effect")
     */
    protected $effect;
    
    /**
     * @Column(type="text", name="ongoing")
     */
    protected $ongoing;
    
    /**
     * @Column(type="text", name="mods")
     */
    protected $modifies;
    
    /**
     * @Column(type="boolean", name="is_triggered")
     */
    protected $isTriggered;
    
    /**
     * @Column(type="boolean", name="is_sub")
     */
    protected $isSub;
    
    /**
     * @Column(type="boolean", name="is_extra")
     */
    protected $isExtra;
    
    /**
     * @Column(type="text", name="cost")
     */
    protected $cost;
    
    /**
     * @Column(type="text", name="cost_2")
     */
    protected $cost2;
    
    /**
     * @Column(type="integer", name="sub_effect")
     */
    protected $subEffect;
    
    /**
     * @Column(type="integer", name="sub_or_effect")
     */
    protected $subOrEffect;
    
    /**
     * @Column(type="integer", name="extra_effect")
     */
    protected $extraEffect;
    
    /**
     * @Column(type="text", name="valid_destinations")
     */
    protected $validDestinations;
    
    /**
     * @Column(type="integer", name="min_targets")
     */
    protected $minTargets;
    
    /**
     * @Column(type="integer", name="max_targets")
     */
    protected $maxTargets;
    
    /**
     * @Column(type="text", name="or_prompt")
     */
    protected $orPrompt;
    
    /**
     * @Column(type="text", name="a_param")
     */
    protected $aParam;
    
    /**
     * @Column(type="text", name="b_param")
     */
    protected $bParam;
    
    public function __construct() {
        $this->triggers = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function getCard() {
        return $this->card;
    }

    public function getId() {
        return $this->id;
    }
    
    public function getTriggers() {
        return $this->triggers;
    }

    public function getEffect() {
        return $this->effect;
    }

    public function getOngoing() {
        return $this->ongoing;
    }

    public function getModifies() {
        return $this->modifies;
    }

    public function isTriggered() {
        return $this->isTriggered;
    }

    public function isSub() {
        return $this->isSub;
    }

    public function isExtra() {
        return $this->isExtra;
    }

    public function getCost() {
        return $this->cost;
    }

    public function getCost2() {
        return $this->cost2;
    }

    public function getSubEffect() {
        return $this->subEffect;
    }

    public function getSubOrEffect() {
        return $this->subOrEffect;
    }

    public function getExtraEffect() {
        return $this->extraEffect;
    }

    public function getValidDestinations() {
        return $this->validDestinations;
    }

    public function getMinTargets() {
        return $this->minTargets;
    }

    public function getMaxTargets() {
        return $this->maxTargets;
    }

    public function getOrPrompt() {
        return $this->orPrompt;
    }

    public function getAParam() {
        return $this->aParam;
    }

    public function getBParam() {
        return $this->bParam;
    }

    public function setCard(\Models\Card $card) {
        $this->card = $card;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setEffect($effect) {
        $this->effect = $effect;
    }

    public function setOngoing($ongoing) {
        $this->ongoing = $ongoing;
    }

    public function setModifies($modifies) {
        $this->modifies = $modifies;
    }

    public function setIsTriggered($isTriggered) {
        $this->isTriggered = $isTriggered;
    }

    public function setIsSub($isSub) {
        $this->isSub = $isSub;
    }

    public function setIsExtra($isExtra) {
        $this->isExtra = $isExtra;
    }

    public function setCost($cost) {
        $this->cost = $cost;
    }

    public function setCost2($cost2) {
        $this->cost2 = $cost2;
    }

    public function setSubEffect($subEffect) {
        $this->subEffect = $subEffect;
    }

    public function setSubOrEffect($subOrEffect) {
        $this->subOrEffect = $subOrEffect;
    }

    public function setExtraEffect($extraEffect) {
        $this->extraEffect = $extraEffect;
    }

    public function setValidDestinations($validDestinations) {
        $this->validDestinations = $validDestinations;
    }

    public function setMinTargets($minTargets) {
        $this->minTargets = $minTargets;
    }

    public function setMaxTargets($maxTargets) {
        $this->maxTargets = $maxTargets;
    }

    public function setOrPrompt($orPrompt) {
        $this->orPrompt = $orPrompt;
    }

    public function setAParam($aParam) {
        $this->aParam = $aParam;
    }

    public function setBParam($bParam) {
        $this->bParam = $bParam;
    }
    
    public function toArray($shortform = false, $callingModel = null) {
        $result = array();
        
        $result['id'] = $this->getId();
        
        $result['effect'] = $this->getEffect();
        
        if (!$shortform) {
            $result['card'] = $this->getCard()->toArray(true);
            
            $result['triggers'] = array();

            foreach ($this->getTriggers() as $trigger) {
                /* @var $trigger \Models\Trigger */
                $result['triggers'][] = $trigger->toArray(true);
            }
        
            $result['ongoing'] = $this->getOngoing();
            $result['modifies'] = $this->getModifies();
            $result['isTriggered'] = $this->isTriggered();
            $result['isSub'] = $this->isSub();
            $result['isExtra'] = $this->isExtra();
            $result['cost'] = $this->getCost();
            $result['cost2'] = $this->getCost2();
            $result['subEffect'] = $this->getSubEffect();
            $result['subOrEffect'] = $this->getSubOrEffect();
            $result['extraEffect'] = $this->getExtraEffect();
            $result['validDestination'] = $this->getValidDestinations();
            $result['minTargets'] = $this->getMinTargets();
            $result['maxTargets'] = $this->getMaxTargets();
            $result['orPrompt'] = $this->getOrPrompt();
            $result['aParam'] = $this->getAParam();
            $result['bParam'] = $this->getBParam();
        }
        
        return $result;
    }
}