<?php

namespace Models\Game;

/**
 * @Entity
 * @Table(name="game_cards")
 * @HasLifecycleCallbacks
 */
class Card
{
    const ZONE_P1_HAND = 0;
    const ZONE_P2_HAND = 1;
    const ZONE_P1_DECK = 2;
    const ZONE_P2_DECK = 3;
    const ZONE_P1_DISCARDED = 4;
    const ZONE_P2_DISCARDED = 5;
    const ZONE_P1_SUPPORT = 6;
    const ZONE_P2_SUPPORT = 7;
    const ZONE_P1_ATTACK = 8;
    const ZONE_P2_ATTACK = 9;

    /**
     * @Id
     * @Column(type="integer", name="gameCardID")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="\Models\Game", inversedBy="gameCards", cascade={"persist"})
     * @JoinColumn(name="gameID", referencedColumnName="gameID"),
     * @var \Models\Game
     */
    protected $game;

    /**
     * @Column(type="integer", name="zone")
     */
    protected $zone;

    /**
     * @Column(type="integer", name="indexx")
     */
    protected $index;

    /**
     * @Column(type="integer", name="attackModifiers")
     */
    protected $attackModifiers;

    /**
     * @Column(type="integer", name="defenceModifiers")
     */
    protected $defenceModifiers;

    /**
     * @OneToMany(targetEntity="\Models\Game\Token", mappedBy="gameCard", fetch="EAGER")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $tokens;

    /**
     * @ManyToOne(targetEntity="\Models\Card", cascade={"persist"})
     * @JoinColumn(name="cardID", referencedColumnName="cardID")
     * @var \Models\Card
     */
    protected $card;

    public function __construct() {
        $this->index = 0;
        $this->damage = 0;
        $this->attackModifiers = 0;
        $this->defenceModifiers = 0;
    }

    public function getId() {
        return $this->id;
    }

    public function getGame() {
        return $this->game;
    }

    public function getZone() {
        return $this->zone;
    }

    public function getIndex() {
        return $this->index;
    }

    public function getTokens() {
        return $this->tokens;
    }

    /**
     * @return \Proxies\Card
     */
    public function getCard() {
        return $this->card;
    }

    public function setGame(\Models\Game $game) {
        $this->game = $game;
    }

    public function setZone($zone) {
        $this->zone = $zone;
    }

    public function setIndex($index) {
        $this->index = $index;
    }

    function getAttackModifiers() {
        return $this->attackModifiers;
    }

    function getDefenceModifiers() {
        return $this->defenceModifiers;
    }

    function setAttackModifiers($attackModifiers) {
        $this->attackModifiers = $attackModifiers;
    }

    function setDefenceModifiers($defenceModifiers) {
        $this->defenceModifiers = $defenceModifiers;
    }

    function getStrengthTokens() {
        return $this->getAttackModifiers() > 0 ? $this->getAttackModifiers() : 0;
    }

    function getHealthTokens() {
        return $this->getDefenceModifiers() > 0 ? $this->getDefenceModifiers() : 0;
    }

    function getWeaknessTokens() {
        return $this->getAttackModifiers() < 0 ? abs($this->getAttackModifiers()) : 0;
    }

    function getPainTokens() {
        return $this->getDefenceModifiers() < 0 ? abs($this->getDefenceModifiers()) : 0;
    }

    public function addToken(\Models\Game\Token $token) {
        $this->tokens->add($token);
    }

    public function setCard(\Models\Card $card) {
        $this->card = $card;
    }

    public function isInPlay() {
        return in_array($this->getZone(), array(
            self::ZONE_P1_SUPPORT,
            self::ZONE_P2_SUPPORT,
            self::ZONE_P1_ATTACK,
            self::ZONE_P2_ATTACK,
        ));
    }

    public static function isZonePublic($zone, $playerNumber) {
        if (in_array($zone, array(
                self::ZONE_P1_ATTACK,
                self::ZONE_P1_DISCARDED,
                self::ZONE_P1_SUPPORT,
                self::ZONE_P2_ATTACK,
                self::ZONE_P2_DISCARDED,
                self::ZONE_P2_SUPPORT))) {
            return true;
        }

        if (in_array($zone, array(
                self::ZONE_P1_DECK,
                self::ZONE_P2_DECK))) {
            return false;
        }

        if ($playerNumber == \Models\Game::PLAYER_ONE && $zone == self::ZONE_P1_HAND) {
            return true;
        }


        if ($playerNumber == \Models\Game::PLAYER_TWO && $zone == self::ZONE_P2_HAND) {
            return true;
        }

        return false;
    }

    public function getAttack() {
        // TODO: iterate over effects and apply
        return $this->getCard()->getAttack();
    }

    public function getDefence() {
        // TODO: iterate over effects and apply
        return $this->getCard()->getDefence();
    }

    public function isVisibleToLogin(\Models\Login $login) {
        /*if ($login->getAdmin()) {
            return true;
        } else {*/
            $isP1 = $login->getId() == $this->getGame()->getMatch()->getP1()->getId();
            $isP2 = $login->getId() == $this->getGame()->getMatch()->getP2()->getId();

            if ($isP1 || $isP2) {
                if (in_array($this->getZone(), array(
                    self::ZONE_P1_ATTACK,
                    self::ZONE_P1_SUPPORT,
                    self::ZONE_P1_DISCARDED,
                    self::ZONE_P2_ATTACK,
                    self::ZONE_P2_SUPPORT,
                    self::ZONE_P2_DISCARDED,
                ))) {
                    return true;
                } else {
                    if ($isP1) {
                        if (in_array($this->getZone(), array(
                            self::ZONE_P1_HAND,
                            self::ZONE_P1_DECK
                        ))) {
                            return true;
                        }
                    } else {
                        if (in_array($this->getZone(), array(
                            self::ZONE_P2_HAND,
                            self::ZONE_P2_DECK
                        ))) {
                            return true;
                        }
                    }
                }
            }
        //}

        return false;
    }

    public function getPublicParams() {
        $tokenParams = array();

        foreach ($this->getTokens() as $token) {
            $tokenParams[] = $token->getPublicParams();
        }

        return array(
            'id' => $this->getId(),
            'card' => $this->getCard()->getPublicParams(),
            'strengthTokens' => $this->getStrengthTokens(),
            'healthTokens' => $this->getHealthTokens(),
            'weaknessTokens' => $this->getWeaknessTokens(),
            'painTokens' => $this->getPainTokens(),
            'tokens' => $tokenParams
        );
    }

    public function getPrivateParams() {
        return array(
            'id' => 0
        );
    }

    public function toArray($shortform = false, $callingModel = null) {
        $this->session = new \Zend_Session_Namespace('clanslcg');
        $userService = new \Clans\Service\User();

        /** @var $login \Models\Login */
        $login = $this->session->login ? $userService->getLoginByID($this->session->login) : null;

        $result = array();

        $result['id'] = $this->getId();
        $result['zone'] = $this->getZone();

        if ($login && $this->isVisibleToLogin($login)) {
            $result['card'] = $this->getCard()->toArray(true);
        }

        $result['game'] = $this->getGame()->toArray(true);
        $result['index'] = $this->getIndex();
        $result['strengthTokens'] = $this->getStrengthTokens();
        $result['healthTokens'] = $this->getHealthTokens();
        $result['weaknessTokens'] = $this->getWeaknessTokens();
        $result['painTokens'] = $this->getPainTokens();

        // TODO: shortform and callingModel

        return $result;
    }
}