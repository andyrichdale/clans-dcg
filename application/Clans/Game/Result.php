<?php

namespace Clans\Game;

class Result
{
    /* @var $success bool */
    public $success;

    /* @var $messages array */
    public $messages;

    /* @var $updatedList Updated\UpdatedList */
    public $updatedList;
}
