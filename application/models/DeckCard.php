<?php

namespace Models;

/**
 * @Entity
 * @Table(name="deck_cards")
 * @HasLifecycleCallbacks
 */
class DeckCard implements BaseModel
{
    /**
     * @Id
     * @ManyToOne(targetEntity="\Models\Deck", inversedBy="deckCards")
     * @JoinColumn(name="deckID", referencedColumnName="deckID")
     * @var \Models\Deck
     */
    protected $deck;
    
    /**
     * @Id
     * @ManyToOne(targetEntity="\Models\Card")
     * @JoinColumn(name="cardID", referencedColumnName="cardID")
     * @var \Models\Card
     */
    protected $card;
    
    /**
     * @Column(type="integer", name="quantity")
     */
    protected $quantity;
    
    public function getDeck() {
        return $this->deck;
    }

    public function setDeck(\Models\Deck $deck) {
        $this->deck = $deck;
    }

    public function getCard() {
        return $this->card;
    }

    public function setCard(\Models\Card $card) {
        $this->card = $card;
    }

    public function getQuantity() {
        return $this->quantity;
    }

    public function setQuantity($quantity) {
        $this->quantity = $quantity;
    }
    
    public function incrementQuantity($amount) {
        $this->quantity += $amount;
    }
    
    public function toArray($shortform = false, $callingModel = null) {
        $result = array();
        
        $result['card'] = $this->getCard()->toArray(true);
        $result['quantity'] = $this->getQuantity();
        
        if (!$shortform) {
            $result['deck'] = $this->getDeck()->toArray(true);
        }
        
        return $result;
    }
}