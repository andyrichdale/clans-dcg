<?php
include('PatchHandler.php');

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initDoctype()
    {
        date_default_timezone_set('GMT');

        $front = Zend_Controller_Front::getInstance();
        $router = $front->getRouter();

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/config.ini', 'production');
        $router->addConfig($config, 'routes');

        $restRoute = new Zend_Rest_Route($front, array(), array('api'));
        $router->addRoute('rest', $restRoute);

        $front->registerPlugin(new Zend_Controller_Plugin_PutHandler());
        $front->registerPlugin(new PatchHandler());

        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('XHTML1_STRICT');

        $options = $this->getApplication()->getOptions();
        $db = Zend_Db::factory($options['resources']['db']['adapter'], $options['resources']['db']['params']);
        Zend_Registry::set('db', $db);

        $front->setControllerDirectory(array(
            'default'   => APPLICATION_PATH . '/controllers',
            'api'       => APPLICATION_PATH . '/api/controllers',
        ));
    }

    /*protected function _initDoctype()
    {
        date_default_timezone_set('GMT');

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/config.ini', 'production');
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $router->addConfig($config, 'routes');

        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('XHTML1_STRICT');

        $options = $this->getApplication()->getOptions();
        $db = Zend_Db::factory($options['resources']['db']['adapter'], $options['resources']['db']['params']);
        Zend_Registry::set('db', $db);

        $this->bootstrap('doctrine');
    }

    public function _initDoctrine()
    {
        //require the Doctrine.php file
        require_once 'Doctrine.php';

        //Get a Zend Autoloader instance
        $loader = Zend_Loader_Autoloader::getInstance();

        //Autoload all the Doctrine files
        $loader->pushAutoloader(array('Doctrine', 'autoload'));

        //Get the Doctrine settings from application.ini
        $doctrineConfig = $this->getOption('doctrine');

        //Get a Doctrine Manager instance so we can set some settings
        $manager = Doctrine_Manager::getInstance();

        //set models to be autoloaded and not included (Doctrine_Core::MODEL_LOADING_AGGRESSIVE)
        $manager->setAttribute(
            Doctrine::ATTR_MODEL_LOADING,
            Doctrine::MODEL_LOADING_CONSERVATIVE);

        //enable ModelTable classes to be loaded automatically
        $manager->setAttribute(
            Doctrine_Core::ATTR_AUTOLOAD_TABLE_CLASSES,
            true
        );

        //enable validation on save()
        $manager->setAttribute(
            Doctrine_Core::ATTR_VALIDATE,
            Doctrine_Core::VALIDATE_ALL
        );

        //enable sql callbacks to make SoftDelete and other behaviours work transparently
        $manager->setAttribute(
            Doctrine_Core::ATTR_USE_DQL_CALLBACKS,
            true
        );

        //not entirely sure what this does :)
        $manager->setAttribute(
            Doctrine_Core::ATTR_AUTO_ACCESSOR_OVERRIDE,
            true
        );

        //enable automatic queries resource freeing
        $manager->setAttribute(
            Doctrine_Core::ATTR_AUTO_FREE_QUERY_OBJECTS,
            true
        );

        //connect to database
        $manager->openConnection($doctrineConfig['connection_string']);

        //set to utf8
        $manager->connection()->setCharset('utf8');

        return $manager;
    }
    */
    protected function _initAutoload()
    {
        $loader = function($className) {
            $className = str_replace('\\', '_', $className);
            Zend_Loader_Autoloader::autoload($className);
        };

        $autoLoader = Zend_Loader_Autoloader::getInstance();
        $autoLoader->registerNamespace('Clans_');
        $autoLoader->pushAutoloader($loader, 'Clans\\');

        $resourceLoader = new Zend_Loader_Autoloader_Resource(array(
            'basePath' => APPLICATION_PATH,
            'namespace' => '',
            'resourceTypes' => array(
                'model' => array(
                    'path' => 'models/',
                    'namespace' => 'Model_'
                )
            ),
        ));

        // Return it so that it can be stored by the bootstrap
        return $autoLoader;
    }
}

