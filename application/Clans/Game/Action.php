<?php

namespace Clans\Game;

interface Action
{
    public function getKey();
    
    public function getResolvedTriggerKey();
    
    public function validateAction();
    
    public function doEffect();
    
    public function payCosts();
    
    public function activateTriggers();
}
