<?php
namespace Clans\API\Update;

/**
 * Acquires the information for all game zones
 * as is visible for player $playerNumber
 */
class SyncGame extends UpdateBase
{
    public function __construct(\Models\Game $game, $playerNumber) {
        $this->id = 0;
        
        $gamesService = new \Clans\Service\Game();
        
        /*
         * Params:
         * zone
         *   cardID, gameCardID, cardIndex
         */
        
        $zones = array(
            \Models\Game\Card::ZONE_P1_HAND,
            \Models\Game\Card::ZONE_P1_DECK,
            \Models\Game\Card::ZONE_P1_DISCARDED,
            \Models\Game\Card::ZONE_P1_SUPPORT,
            \Models\Game\Card::ZONE_P1_ATTACK,
            \Models\Game\Card::ZONE_P2_HAND,
            \Models\Game\Card::ZONE_P2_DECK,
            \Models\Game\Card::ZONE_P2_DISCARDED,
            \Models\Game\Card::ZONE_P2_SUPPORT,
            \Models\Game\Card::ZONE_P2_ATTACK
        );
        
        foreach ($zones as $zone) {
            $this->params[] = $zone;
            
            $zoneGameCardParams = array();
            
            $zoneGameCards = $gamesService->getGameCardsForZone($game, $zone);
        
            foreach ($zoneGameCards as $gameCard) {
                /* @var $gameCard \Models\Game\Card */
                $zoneGameCardParams[] = \Models\Game\Card::isZonePublic($zone, $playerNumber) ? $gameCard->getCard()->getId() : 0;
                $zoneGameCardParams[] = \Models\Game\Card::isZonePublic($zone, $playerNumber) ? $gameCard->getId() : 0;
                $zoneGameCardParams[] = $gameCard->getIndex();
            }
            
            $this->params[] = $zoneGameCardParams;
        }
    }
}
