<?php
require_once 'MyAction.php';

class ArticlesController extends MyAction
{
    /**
     * Display the article list
     */
    public function indexAction() {
        $this->view->articles = $this->contentTable->fetchByType('article');
    }
}
