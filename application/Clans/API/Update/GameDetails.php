<?php
namespace Clans\API\Update;

//require_once APPLICATION_PATH . "/Classes/API/Update/UpdateBase.php";

//use Clans\API\Update\UpdateBase;

/**
 * Acquires the information for all game zones
 * as is visible for player $playerNumber
 */
class GameDetails extends UpdateBase
{
    public function __construct(\Models\Game $game) {
        $this->id = 3;
        
        /**
         * Params:
         * timestarted, timenow, ticktime, p1life, p2life, p1resources, p2resource
         */
        $this->params[] = $game->getTimeStarted();
        $this->params[] = date("U");
        $this->params[] = $game->getMatch()->getTickTime();
        $this->params[] = $game->getp1Life();
        $this->params[] = $game->getp2Life();
        $this->parmas[] = $game->getPlayerCurrentResources(1);
        $this->parmas[] = $game->getPlayerCurrentResources(2);
    }
}
