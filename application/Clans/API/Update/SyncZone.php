<?php
namespace Clans\API\Update;

//require_once APPLICATION_PATH . "/Classes/API/Update/UpdateBase.php";

//use Clans\API\Update\UpdateBase;

/**
 * Acquires the information for a zone with id $zoneID
 * as is visible for player $playerNumber
 */
class SyncZone extends UpdateBase
{
    public function __construct(\Models\Game $game, $zoneID, $playerNumber) {
        $this->id = 1;
        $this->params[] = $zoneID;

        $gamesService = new \Clans\Service\Game();

        $zoneGameCards = $gamesService->getGameCardsForZone($game, $zoneID);

        //if (\Models\Game\Card::isZonePublic($zoneID, $playerNumber)) {
        //    // Indicate zone is public
        //    $this->params[] = 1;

            // List zone game cards and their indices
            foreach ($zoneGameCards as $gameCard) {
                /* @var $gameCard \Models\Game\Card */
                //TODO: instead of "if public else" just make facedown cards id = 0
                $this->params[] = \Models\Game\Card::isZonePublic($zoneID, $playerNumber) ? $gameCard->getCard()->getId() : 0;
                $this->params[] = $gameCard->getIndex();
            }
        //} else {
            // Indicate zone is private
        //    $this->params[] = 0;

            // Specify number of cards in this private zone
        //    $this->params[] = count($zoneGameCards);
        //}
    }
}
