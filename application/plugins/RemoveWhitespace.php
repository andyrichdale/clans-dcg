<?php
class Application_Plugins_RemoveWhitespace extends Zend_Controller_Plugin_Abstract
{
    public $prefix;
    
    public function __construct($prefix = "") {
        $this->prefix = $prefix;
    }
    
    public function dispatchLoopShutdown() {
        $body = $this->getResponse()->getBody();

        $this->getResponse()->setBody($this->prefix . preg_replace('/\n/', '', $body));
    }
}
