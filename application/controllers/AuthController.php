<?php
require_once 'MyAction.php';

class AuthController extends MyAction
{
    public function indexAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $type = $this->getRequest()->getParam('type', false);

        if (!$type) return;
        
        $config = APPLICATION_PATH . '/../library/hybridauth/config.php';

        try {
            $hybridauth = new Hybrid_Auth($config);

            $auth = $hybridauth->authenticate($type);

            $this->session->login = $this->usersService->loginUser($auth->getUserProfile(), $type);

            header("Location: /");
            exit;
        }
        catch( Exception $e ){
            //header("Location: /?error=Error logging in, please try again");
            //exit;
            die("Ooophs, we got an error: " . $e->getMessage());
        }
    }
    
    public function endpointAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        require_once( "hybridauth/Hybrid/Auth.php" );
        require_once( "hybridauth/Hybrid/Endpoint.php" ); 

        Hybrid_Endpoint::process();
    }
}
