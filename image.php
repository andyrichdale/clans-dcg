<?php

if (isset($_GET['id'])) {
    $id = intval($_GET['id']);
    $gameCard = isset($_GET['gameCard']);
    
    if ($gameCard) {
        $filename = $_SERVER['DOCUMENT_ROOT'] . "/asset/images/card/renders/gamecards/{$id}.png";
    } else {
        $filename = $_SERVER['DOCUMENT_ROOT'] . "/asset/images/card/renders/{$id}.png";
    }
    
    if (!file_exists($filename)) {
        $url = "http://" . $_SERVER['HTTP_HOST'] . "/card/render/{$id}" . ($gameCard ? "?gameCard=1" : "");
        
        header('Location: ' . $url);
    }
    
    $im = imagecreatefrompng($filename);

    header('Content-Type: image/png');

    imagepng($im);
    imagedestroy($im);

    exit;
}