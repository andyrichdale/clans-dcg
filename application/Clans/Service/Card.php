<?php

namespace Clans\Service;

class Card extends BaseService
{
    public function getCards($filters = array(), $search = false, $sort = false, $sortDir = "ASC") {
        $qb = $this->em->createQueryBuilder();
        $qb->addSelect('c');
        $qb->addSelect('s');
        $qb->from('\Models\Card', 'c');
        $qb->join('c.set', 's');
        
        $where = $qb->expr()->andX();
        $where->add('1=1');
        
        if ($search) {
            $where->add(
                $qb->expr()->orX(
                    $qb->expr()->like('c.name', $qb->expr()->literal("%{$search}%")),
                    $qb->expr()->like('c.gametext', $qb->expr()->literal("%{$search}%"))
                )
            );
        }
        
        foreach ($filters as $name => $values) {
            $filterWhere = $qb->expr()->orX();
            foreach ($values as $value) {
                switch ($name) {
                    case "keywords":
                        $filterWhere->add($qb->expr()->like('c.gametext', $qb->expr()->literal("%{$value}%")));
                    break;
                    case "type":
                        $filterWhere->add($qb->expr()->like("c.{$name}", $qb->expr()->literal("%{$value}%")));
                    break;
                    default:
                        $filterWhere->add($qb->expr()->eq("c.{$name}", $qb->expr()->literal($value)));
                    break;
                }
            }
            $where->add($filterWhere);
        }
        
        if ($where->count()) {
            $qb->where($where);
        }
        
        if ($sort) {
            $qb->addOrderBy("c.{$sort}", $sortDir);
        } else {
            $qb->addOrderBy('c.clan', 'ASC');
        }
        
        $qb->addOrderBy('c.number', 'ASC');

        return $qb->getQuery()->getResult();
    }
    
    /**
     * @param \Models\Card $card
     * @param boolean $small
     * @return string
     */
    public function getImageForCard(\Models\Card $card, $small = false, $file = false) {
        $imageName = str_replace(" ", "-", $card->getName());
        /*if ($small) {
            $imageName .= "-sm";
        }*/
        $location = "/asset/images/card/{$card->getSet()->getAbbrev()}/$imageName.jpg";
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $location)) {
            if ($file) {
                return $_SERVER['DOCUMENT_ROOT'] . $location;
            } else {
                return $location;
            }
        }
        return false;
    }
    
    /**
     * @param integer $id
     * @return \Models\Card
     */
    public function getCardByID($id) {
        $query = $this->em->createQuery('SELECT c FROM \Models\Card c WHERE c.id = ?1');
        $query->setParameter(1, $id);
        
        $this->em->flush();
        
        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return false;
        }
    }
    
    /**
     * @param \Models\Set $set
     * @param integer $number
     * @return \Models\Card
     */
    public function getCardByNumber(\Models\Set $set, $number) {
        $query = $this->em->createQuery('SELECT c FROM \Models\Card c WHERE c.set = ?1 AND c.number = ?2');
        $query->setParameter(1, $set);
        $query->setParameter(2, $number);
        
        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return false;
        }
    }
    
    public function saveCard(\Models\Card $card) {
        $this->em->persist($card);
        $this->em->flush();
    }
}