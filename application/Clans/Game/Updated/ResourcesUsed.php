<?php

namespace Clans\Game\Updated;

class ResourcesUsed extends \Clans\Game\BaseUpdated implements \Clans\Game\Updated
{
    private $resourceChange;

    private $playerNumber;

    public function __construct($resourceChange, $playerNumber) {
        $this->resourceChange = $resourceChange;
        $this->playerNumber = $playerNumber;
    }

    public function getKey() {
        return 'ResourcesUsed';
    }

    public function getData() {
        return array(
            'resourceChange' => $this->resourceChange,
            'playerNumber' => $this->playerNumber
        );
    }
}
