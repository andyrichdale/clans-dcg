function Card(card, facedown) {
    this.facedown = facedown;
    
    // TODO: make GameCard.js - give it a Card property, tokens etc.
    
    if (!this.facedown) {
        this.id = card.id;
        this.name = card.name;
        this.clan = card.clan;
        this.type = card.type;
        this.attack = card.attack;
        this.defence = card.defence;
        this.cost = card.cost;
        this.influenceB = card.influenceB;
        this.influenceG = card.influenceG;
        this.influenceM = card.influenceM;
        this.influenceR = card.influenceR;
        this.gametext = card.gametext;
    }
    
    this.cssBg = function() {
        var imageName = this.name.replace(" ", "-");
        var imgHref = "/asset/images/card/HML/" + imageName + ".jpg";
        return 'url("/asset/images/card-bg-' + this.clan.toLowerCase() + '-81.png"), url("' + imgHref + '")';
    };
    
    this.getHtml = function() {
        var cardDiv = $('<div></div>').addClass('card').addClass('small').data('clan', this.clan).css('background-image', this.cssBg());
        
        var cardName = $('<h3></h3>').addClass('card-title').addClass('card-part').html(this.name).appendTo(cardDiv);
        
        var cardRes = $('<div></div>').addClass('card-resources').addClass('card-part').html(this.cost).appendTo(cardDiv);
        
        var cardType = $('<div></div>').addClass('card-type').addClass('card-part').html(this.type).appendTo(cardDiv);
        
        var cardRes = $('<div></div>').addClass('card-resources').addClass('card-part').html(this.cost).appendTo(cardDiv);
        
        var cardInfluence = $('<div></div>').addClass('card-influence').addClass('card-part');
        var bainInfluence = this.influenceB > 0 ? $('<div></div>').addClass('influence').addClass('Bain').html(this.influenceB).appendTo(cardInfluence) : '';
        var monzarInfluence = this.influenceM > 0 ? $('<div></div>').addClass('influence').addClass('Monzar').html(this.influenceM).appendTo(cardInfluence) : '';
        var ronmirInfluence = this.influenceR > 0 ? $('<div></div>').addClass('influence').addClass('Ronmir').html(this.influenceR).appendTo(cardInfluence) : '';
        var galmathInfluence = this.influenceG > 0 ? $('<div></div>').addClass('influence').addClass('Galmath').html(this.influenceG).appendTo(cardInfluence) : '';
        cardInfluence.appendTo(cardDiv);
        
        var cardStats = $('<div></div>').addClass('card-stats').addClass('card-part');
        if (this.type == 'Ally') {
            var cardAttack = $('<div></div>').addClass('card-attack').html('Attack: <strong>' + this.attack + '</strong>').appendTo(cardStats);
            var cardDefence = $('<div></div>').addClass('card-defence').html('Attack: <strong>' + this.defence + '</strong>').appendTo(cardStats);
        }
        cardStats.appendTo(cardDiv);
        
        var cardGametext = $('<p></p>').addClass('card-text').addClass('card-part').html(this.gametext).appendTo(cardDiv);
        
        return cardDiv;
    };
    
    this.render = function() {
        if (this.id == 0) {
            // Return facedown card image
            return "<img class='card-render no-events facedown' src='/asset/images/card/clans-cardback.jpg' />";
        } else {
            // Return card render
            return "<img class='card-render no-events' src='/asset/images/card/renders/" + this.id + ".png' data-id='" + this.id + "' />";
        }
    };
    
    this.getAsElement = function() {
        if (this.id == 0) {
            // Return facedown card image
            return $('<img/>').addClass('card-render').addClass('no-events').addClass('facedown').attr('src', '/asset/images/card/clans-cardback.jpg');
        } else {
            // Return card render
            return $('<img/>').addClass('card-render').addClass('no-events').attr('src', '/asset/images/card/renders/' + this.id + '.png').data('id', this.id);
        }
    }
}