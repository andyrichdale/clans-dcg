<?php

namespace Models;

/**
 * @Entity
 * @Table(name="matches")
 * @HasLifecycleCallbacks
 */
class Match implements BaseModel
{
    /**
     * @Id
     * @Column(type="integer", name="id")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="\Models\Login", cascade={"persist"})
     * @JoinColumn(name="p1", referencedColumnName="id")
     * @var \Models\Login
     */
    protected $p1;

    /**
     * @ManyToOne(targetEntity="\Models\Login", cascade={"persist"})
     * @JoinColumn(name="p2", referencedColumnName="id")
     * @var \Models\Login
     */
    protected $p2;

    /**
     * @ManyToOne(targetEntity="\Models\Deck", cascade={"persist"})
     * @JoinColumn(name="p1_deck", referencedColumnName="deckID")
     * @var \Models\Deck
     */
    protected $p1Deck;

    /**
     * @ManyToOne(targetEntity="\Models\Deck", cascade={"persist"})
     * @JoinColumn(name="p2_deck", referencedColumnName="deckID")
     * @var \Models\Deck
     */
    protected $p2Deck;

    /**
     * @Column(type="integer", name="p1_time")
     */
    protected $p1Time;

    /**
     * @Column(type="integer", name="p2_time")
     */
    protected $p2Time;

    /**
     * @Column(type="integer", name="p1_wins")
     */
    protected $p1Wins;

    /**
     * @Column(type="integer", name="p2_wins")
     */
    protected $p2Wins;

    /**
     * @Column(type="text", name="create_time")
     */
    protected $createTime;

    /**
     * @Column(type="integer", name="num_games")
     */
    protected $numGames;

    /**
     * @Column(type="integer", name="state")
     */
    protected $state;

    /**
     * @Column(type="integer", name="game_number")
     */
    protected $gameNumber;

    /**
     * @Column(type="integer", name="tick_time")
     */
    protected $tickTime;

    /**
     * @OneToMany(targetEntity="\Models\Game", mappedBy="match", fetch="EAGER")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $games;

    const STATE_CHALLENGE = 0;
    const STATE_STARTED = 1;
    const STATE_FINISHED = 2;
    const STATE_ERROR = 3;

    public function __construct() {
        $this->games = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId() {
        return $this->id;
    }

    public function getP1() {
        return $this->p1;
    }

    public function getP2() {
        return $this->p2;
    }

    public function getP1Deck() {
        return $this->p1Deck;
    }

    public function getP2Deck() {
        return $this->p2Deck;
    }

    public function getP1Time() {
        return $this->p1Time;
    }

    public function getP2Time() {
        return $this->p2Time;
    }

    public function getP1Wins() {
        return $this->p1Wins;
    }

    public function getP2Wins() {
        return $this->p2Wins;
    }

    public function getCreateTime() {
        return $this->createTime;
    }

    public function getNumGames() {
        return $this->numGames;
    }

    public function getState() {
        return $this->state;
    }

    public function getGameNumber() {
        return $this->gameNumber;
    }

    public function getGames() {
        return $this->games;
    }

    /**
     * @return \Models\Game
     */
    public function getCurrentGame() {
        if ($this->getState() === self::STATE_STARTED) {
            foreach ($this->getGames() as $game) {
                /* @var $game \Models\Game */
                if ($game->getGameNumber() === $this->getGameNumber()) {
                    return $game;
                }
            }
        }

        return null;
    }

    public function setP1(\Models\Login $p1) {
        $this->p1 = $p1;
    }

    public function setP2(\Models\Login $p2) {
        $this->p2 = $p2;
    }

    public function setP1Deck(\Models\Deck $p1Deck) {
        $this->p1Deck = $p1Deck;
    }

    public function setP2Deck(\Models\Deck $p2Deck) {
        $this->p2Deck = $p2Deck;
    }

    public function setP1Time($p1Time) {
        $this->p1Time = $p1Time;
    }

    public function setP2Time($p2Time) {
        $this->p2Time = $p2Time;
    }

    public function setP1Wins($p1Wins) {
        $this->p1Wins = $p1Wins;
    }

    public function setP2Wins($p2Wins) {
        $this->p2Wins = $p2Wins;
    }

    public function setCreateTime($createTime) {
        $this->createTime = $createTime;
    }

    public function setNumGames($numGames) {
        $this->numGames = $numGames;
    }

    public function setState($state) {
        $this->state = $state;
    }

    public function setGameNumber($gameNumber) {
        $this->gameNumber = $gameNumber;
    }

    function getTickTime() {
        return $this->tickTime;
    }

    function setTickTime($tickTime) {
        $this->tickTime = $tickTime;
    }

    function toArray($shortform = false, $callingModel = null) {
        $this->session = new \Zend_Session_Namespace('clanslcg');
        $userService = new \Clans\Service\User();

        /** @var $login \Models\Login */
        $login = $this->session->login ? $userService->getLoginByID($this->session->login) : null;

        $result = array();

        $result['id'] = $this->getId();
        $result['p1'] = $this->getP1()->toArray(true);
        $result['p2'] = $this->getP2()->toArray(true);
        if ($login && ($login->getAdmin() || $login->getId() == $this->getP1()->getId())) {
            $result['p1Deck'] = $this->getP1Deck()->toArray(true);
        }
        if ($login && ($login->getAdmin() || $login->getId() == $this->getP2()->getId())) {
            $result['p2Deck'] = $this->getP2Deck()->toArray(true);
        }
        $result['p1Time'] = $this->getP1Time();
        $result['p2Time'] = $this->getP2Time();
        $result['p1Wins'] = $this->getP1Wins();
        $result['p2Wins'] = $this->getP2Wins();
        $result['createTime'] = $this->getCreateTime();
        $result['numGames'] = $this->getNumGames();
        $result['state'] = $this->getState();
        $result['gameNumber'] = $this->getGameNumber();
        $result['tickTime'] = $this->getTickTime();

        if (!$shortform) {
            $result['games'] = array();

            foreach ($this->getGames() as $game) {
                $result['games'][] = $game->toArray(true);
            }
        }

        return $result;
    }
}