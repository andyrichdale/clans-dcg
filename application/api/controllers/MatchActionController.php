<?php
require APPLICATION_PATH . '/api/controllers/BaseController.php';

class Api_MatchActionController extends Api_BaseController
{
    /* @var $matchService \Clans\Service\Match */
    private $matchService;

    public function init() {
        parent::init();

        $this->matchService = new \Clans\Service\Match();
    }

    public function getAction() {
        // TODO return match action history
        $this->returnError('Not implemented');
    }

    public function indexAction() {
        // TODO return match action history
        $this->returnError('Not implemented');
    }

    public function deleteAction() {
        $this->returnError('Permission denied');
    }

    public function postAction() {
        $this->returnError('Permission denied');
    }

    public function putAction() {
        $this->returnError('Not implemented');
    }
}
