<?php
require APPLICATION_PATH . '/api/controllers/BaseController.php';

class Api_MatchController extends Api_BaseController
{
    /* @var $matchService \Clans\Service\Match */
    private $matchService;

    public function init() {
        parent::init();

        $this->matchService = new \Clans\Service\Match();
    }

    public function getAction() {
        if ($this->id) {
            $match = $this->matchService->getMatchByID($this->id);

            if ($match) {
                $this->returnSuccess($match->toArray());
            } else {
                $this->returnError('Match not found');
            }
        } else {
            $this->returnError('ID not present');
        }
    }

    public function indexAction() {
        $matches = array();

        foreach ($this->matchService->getMatches() as $match) {
            $matches[] = $match->toArray();
        }

        $this->returnSuccess($matches);
    }

    public function deleteAction() {
    }

    public function postAction() {
        $this->returnError('Permission denied');
    }

    public function putAction() {
        $this->returnError('Permission denied');
    }
}
