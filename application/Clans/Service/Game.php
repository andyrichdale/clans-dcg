<?php

namespace Clans\Service;

class Game extends BaseService
{
    public $debug = array();

    public function getGames() {
        $qb = $this->em->createQueryBuilder();
        $qb->select('g');
        $qb->from('\Models\Game', 'g');
        $qb->orderBy('g.timeStarted', 'DESC');

        return $this->getQbResult($qb);
    }

    public function saveGame(\Models\Game $game) {
        $this->em->persist($game);
        $this->em->flush();
    }

    public function addGameCard(\Models\Game\Card $gameCard) {
        $this->em->persist($gameCard);
    }

    public function saveGameCards() {
        $this->em->flush();
        $this->em->clear();
    }

    /**
     * @param \Models\Match $match
     * @param integer $number
     * @return \Models\Game
     */
    public function getGame(\Models\Match $match, $number) {
        $qb = $this->em->createQueryBuilder();

        $qb->select('g');
        $qb->from('\Models\Game', 'g');
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq('g.match', $match),
                $qb->expr()->eq('g.number', $number)
            )
        );

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return false;
        }
    }

    /**
     * @param int $gameID
     * @return \Models\Game
     */
    public function getGameByID($gameID) {
        $query = $this->em->createQuery('SELECT g FROM \Models\Game g WHERE g.id = ?1');
        $query->setParameter(1, $gameID);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return false;
        }
    }

    public function startGame(\Models\Game $game) {
        // Load decks
        $this->em->getConnection()->beginTransaction();

        $this->loadDeck($game, \Models\Game::PLAYER_ONE);
        $this->loadDeck($game, \Models\Game::PLAYER_TWO);

        //$this->shuffleDeck($game, \Models\Game::PLAYER_ONE);
        //$this->shuffleDeck($game, \Models\Game::PLAYER_TWO);

        // Set starting numbers
        $game->setp1Life(30);
        $game->setp2Life(30);

        // Draw starting hands for each player
        $this->drawStartingHand($game, \Models\Game::PLAYER_ONE);
        $this->drawStartingHand($game, \Models\Game::PLAYER_TWO);

        $game->setState(\Models\Game::STATE_STARTED);

        $game->setTimeStarted(gmdate("Y-m-d H:i:s", strtotime("+30 seconds")));

        $this->em->flush();
        $this->em->getConnection()->commit();
    }

    /**
     * @param \Models\Game $game
     * @param integer $playerNumber
     * @param integer $numberCards
     */
    public function drawStartingHand(\Models\Game $game, $playerNumber) {
        if ($playerNumber == \Models\Game::PLAYER_ONE) {
            $handZone = \Models\Game\Card::ZONE_P1_HAND;
            $deckZone = \Models\Game\Card::ZONE_P1_DECK;
        } else {
            $handZone = \Models\Game\Card::ZONE_P2_HAND;
            $deckZone = \Models\Game\Card::ZONE_P2_DECK;
        }

        $index = 0;

        $deckGameCards = $this->getGameCardsForZone($game, $deckZone);

        foreach ($deckGameCards as $deckGameCard) {
            /* @var $deckGameCard \Models\Game\Card */
            if ($index >= \Models\Game::STARTING_HAND_SIZE) {
                break;
            } else {
                $deckGameCard->setZone($handZone);
                $deckGameCard->setIndex($index);
                $this->em->persist($deckGameCard);
            }
            $index++;
        }

        $this->em->flush();
    }

    public function loadDeck(\Models\Game $game, $playerNumber) {
        if ($playerNumber == \Models\Game::PLAYER_ONE) {
            $deck = $game->getMatch()->getP1Deck();
            $zone = \Models\Game\Card::ZONE_P1_DECK;
            //echo "<p>Loading deck for player 1 ($playerNumber), zone = $zone</p>";
        } else {
            $deck = $game->getMatch()->getP2Deck();
            $zone = \Models\Game\Card::ZONE_P2_DECK;
            //echo "<p>Loading deck for player 2 ($playerNumber), zone = $zone</p>";
        }

        $count = $deck->getDeckSize();
        $indices = range(1, $count);
        shuffle($indices);

        foreach ($deck->getDeckCards() as $deckCard) {
            /* @var $deckCard \Models\DeckCard */
            for ($i = 0; $i < $deckCard->getQuantity(); $i++) {
                $gameCard = new \Models\Game\Card();
                $gameCard->setIndex(array_pop($indices));
                $gameCard->setCard($deckCard->getCard());
                $gameCard->setGame($game);
                $gameCard->setZone($zone);

                $this->em->persist($gameCard);
            }
        }

        $this->em->flush();
    }

    public function shuffleDeckNOTUSED(\Models\Game $game, $playerNumber) {
        if ($playerNumber == \Models\Game::PLAYER_ONE) {
            $zone = \Models\Game\Card::ZONE_P1_DECK;
            //echo "<p>Shuffling deck for player 1 ($playerNumber), zone = $zone</p>";
        } else {
            $zone = \Models\Game\Card::ZONE_P2_DECK;
            //echo "<p>Shuffling deck for player 2 ($playerNumber), zone = $zone</p>";
        }

        $count = $this->countGameCardsForZone($game, $zone);
        $indices = range(0, $count - 1);
        shuffle($indices);

        //echo "<p>Found $count cards in deck zone</p>";

        $this->em->transactional(function($em) use ($game, $zone, $indices) {
            foreach ($this->getGameCardsForZone($game, $zone) as $gameCard) {
                /* @var $gameCard \Models\Game\Card */
                //echo "<p>Shuffled card " . $gameCard->getCard()->getName() . " with index " . $gameCard->getIndex();
                $index = array_pop($indices);

                $this->debug[] = array($zone, $index);

                $gameCard->setIndex($index);

                $this->em->persist($gameCard);
                //echo " to index " . $gameCard->getIndex() . "</p>";
            }
        });
    }

    /**
     * @param \Models\Game $game
     * @param integer $zone
     * @return integer
     */
    public function countGameCardsForZone(\Models\Game $game, $zone) {
        $qb = $this->em->createQueryBuilder();

        $qb->select('COUNT(c)');
        $qb->from('\Models\Game\Card', 'c');
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq('c.game', '?1'),
                $qb->expr()->eq('c.zone', '?2')
            )
        );
        $qb->setParameter(1, $game);
        $qb->setParameter(2, $zone);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param \Models\Game $game
     * @param integer $zone
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getGameCardsForZone(\Models\Game $game, $zone) {
        $qb = $this->em->createQueryBuilder();

        $qb->select('c');
        $qb->from('\Models\Game\Card', 'c');
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq('c.game', '?1'),
                $qb->expr()->eq('c.zone', '?2')
            )
        );
        $qb->setParameter(1, $game);
        $qb->setParameter(2, $zone);

        $qb->orderBy('c.index', 'ASC');

        return $qb->getQuery()->getResult();
    }

    /**
     *
     * @param \Models\Game $game
     * @param index $zone
     * @return integer
     */
    public function getHighestIndex(\Models\Game $game, $zone) {
        $qb = $this->em->createQueryBuilder();

        $qb->select('MAX(index)');
        $qb->from('\Models\Game\Card', 'c');
        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq('c.game', $game),
                $qb->expr()->eq('c.zone', $zone)
            )
        );

        try {
            return $qb->getQuery()->getSingleScalarResult();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $gameCardID
     * @return \Models\Game\Card
     */
    public function getGameCardByID($gameCardID) {
        $query = $this->em->createQuery('SELECT c FROM \Models\Game\Card c WHERE c.id = ?1');
        $query->setParameter(1, $gameCardID);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return false;
        }
    }

    /**
     * @param \Models\Game\Card $gameCard
     * @param int $zone
     */
    public function moveGameCardToZone(\Models\Game\Card $gameCard, $zone, $index = 0) {
        $gameCard->setZone($zone);

        if ($index == 0) {
            foreach ($gameCard->getGame()->filterGameCardsByZone(array($zone)) as $zoneCard) {
                $index = max(array($index, $zoneCard->getIndex()));
            }
            $index++;
        }

        $gameCard->setIndex($index);

        $this->em->persist($gameCard);
        $this->em->flush();
    }

    /**
     *
     * @param string $action
     * @return \Clans\Game\BaseAction
     */
    public function getGameAction($action, \Models\Game $game, $playerNumber, $params) {
        switch ($action) {
            case 'play-card':
                if (isset($params['gameCardID']) && isset($params['zone']) && isset($params['index'])) {
                    $action = new \Clans\Game\Action\PlayCard($game, $playerNumber);
                    $action->setGameCardID($params['gameCardID']);
                    $action->setZoneID($params['zone']);
                    $action->setSlotID($params['index']);

                    return $action;
                } else {
                    return null;
                }
            case 'draw-card':
                $action = new \Clans\Game\Action\DrawCard($game, $playerNumber);
                //TODO: use constant
                $action->setResourceCost(2);

                return $action;
            default:
                return null;
        }
    }
}