<?php

namespace Clans\Service;

class Effect extends BaseService
{
    public function saveEffect(\Models\Effect $effect) {
        $this->em->persist($effect);
        $this->em->flush();
    }
    
    /**
     * @param \Models\Card $card
     * @param integer $number
     * @return \Models\Effect
     */
    public function getEffectByNumber(\Models\Card $card, $number) {
        $query = $this->em->createQuery('SELECT e FROM \Models\Effect e WHERE e.card = ?1 AND e.id = ?2');
        $query->setParameter(1, $card);
        $query->setParameter(2, $number);
        
        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return false;
        }
    }
}