<?php
require_once 'MyAction.php';

class PhotonController extends MyAction
{
    public function init() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $params = $this->_getAllParams();
        $post = $_POST;
        
        $logFilename = $_SERVER['DOCUMENT_ROOT'] . "/application/photon-logs/" . date("Y-m-d H-i-s") . "-photon-log.txt";
        $logFile = fopen($logFilename, "w");
        
        fwrite($logFile, print_r(array_merge($params, $post), true));
        fclose($logFile);
        
        $response = array('ResultCode' => 0, 'Message' => 'OK');
        echo json_encode($response);
        
        exit;
    }
    
    public function indexAction() {
        
    }
    
    public function gameCloseAction() {
        
    }
    
    public function gameCreateAction() {
        
    }
    
    public function gameEventAction() {
        
    }
    
    public function gamePropertiesAction() {
        
    }
    
    public function gameJoinAction() {
        
    }
    
    public function gameLeaveAction() {
        
    }
}
