Handlebars.registerPartial('card', 'card.tmpl');

Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
    switch (operator) {
        case '==':
            return (v1 == v2) ? options.fn(this) : options.inverse(this);
        case '===':
            return (v1 === v2) ? options.fn(this) : options.inverse(this);
        case '!=':
            return (v1 != v2) ? options.fn(this) : options.inverse(this);
        case '!==':
            return (v1 !== v2) ? options.fn(this) : options.inverse(this);
        case '<':
            return (v1 < v2) ? options.fn(this) : options.inverse(this);
        case '<=':
            return (v1 <= v2) ? options.fn(this) : options.inverse(this);
        case '>':
            return (v1 > v2) ? options.fn(this) : options.inverse(this);
        case '>=':
            return (v1 >= v2) ? options.fn(this) : options.inverse(this);
        case '&&':
            return (v1 && v2) ? options.fn(this) : options.inverse(this);
        case '||':
            return (v1 || v2) ? options.fn(this) : options.inverse(this);
        default:
            return options.inverse(this);
    }
});

Handlebars.registerHelper('gametextAsHTML', function (gametext, options) {
    if (gametext) {
        gametext = gametext.replace(/->/g, "<span class='arrow-right'></span>");
        gametext = gametext.replace(/{A}/g, "<span class='card-icon atk-icon'></span>");
        gametext = gametext.replace(/{D}/g, "<span class='card-icon def-icon'></span>");
        gametext = gametext.replace(/{R}/g, "<span class='card-icon cost-icon'></span>");
        gametext = gametext.replace(/{S}/g, "<span class='card-icon speed-icon'></span>");
        gametext = gametext.replace(/Pain/g, "<span class='card-icon pain-icon'></span>");
        gametext = gametext.replace(/Weakness/g, "<span class='card-icon weakness-icon'></span>");
        gametext = gametext.replace(/Health/g, "<span class='card-icon health-icon'></span>");
        gametext = gametext.replace(/Strength/g, "<span class='card-icon strength-icon'></span>");
        gametext = gametext.replace(/(?:\r\n|\r|\n)/g, "<br />");
    }

    return gametext;
});