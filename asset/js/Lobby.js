function Lobby(pusher) {
    this.pusher = pusher;
    this.lobbyChannel = pusher.subscribe('presence-lobby');
    
    this.lobbyChannel.bind('pusher:subscription_succeeded', function(members) {
        // for example
        // update_member_count(members.count);

        //$('#userlist').html('<h3>Users online</h3>');

        members.each(function(member) {
            addMember(member);
        });
        
        $.get('/play/check-state');
    });
    
    this.lobbyChannel.bind('pusher:member_added', function(member) {
        addMember(member);
        //$('.chat-box .chat').prepend("<div class='alert-row'>" + member.info.username + " has joined the lobby");
    });

    this.lobbyChannel.bind('pusher:member_removed', function(member) {
        removeMember(member);
        //$('.chat-box .chat').prepend("<li class='alert-row'>" + member.info.username + " has left the lobby" + "</li>");
    });

    this.lobbyChannel.bind('pusher:heartbeat', function(data) {
        alert(data.time);
    });

    this.lobbyChannel.bind('new_match', function(matchHtml) {
        $('#lobby-list tr:last').after(matchHtml);
        /*
         $('select[name="deckID"]').each(function() {
         var type = $(this).attr('type');
         var html = $('select[name="new-match-deck"] optgroup[label="' + type + '"]').html();
         
         $(this).find('option').after(html);
         $('select').selectBox();
         });*/
    });

    this.lobbyChannel.bind('chat-message', function(data) {
        $('.chat-box .chat').prepend($('<li></li>').text(data.message));
    });
    
    if ($('#userID').val()) {
        this.privateChannel = pusher.subscribe('private-user-' + $('#userID').val());
    
        this.privateChannel.bind('challenge', function(data) {
            showChallengeButtons(data);
        });
    
        this.privateChannel.bind('challenge-decline', function(data) {
            var challenge = $('.challenge[data-challenger=' + data.challenger + '][data-challengee=' + data.challengee + ']');
            challenge.removeClass('panel-primary').addClass('panel-warning');
            challenge.find('.panel-body').html('').append('<p></p>').html('Challenge declined');
            setTimeout(function() {
                challenge.fadeOut('slow', function() {
                    challenge.remove();
                });
            }, 3000);
        });

        this.privateChannel.bind('match', function(data) {
            document.location = '/match/' + data.match + '/play';
        });
    }
}

function showChallengeButtons(data) {
    if ($('.challenge[data-challenger=' + data.challenger + '][data-challengee=' + data.challengee + ']').length == 0) {
        var panel = $('<div></div>').addClass('panel').addClass('panel-primary').addClass('challenge').attr('data-challenger', data.challenger).attr('data-challengee', data.challengee);
        if (data.challengee == $('#userID').val()) {
            // Show challenge from user, allow user to accept or decline
            panel.addClass('incoming');
            panel.append($('<div></div>').addClass('panel-heading').append($('<h3></h3>').addClass('panel-title').html('Challenge!')));
            var panelBody = $('<div></div>').addClass('panel-body');
            panelBody.append($('<p></p>').html('Incoming challenge from ' + data.challengerUsername));
            panelBody.append($('<button></button>').prop('type', 'button').addClass('btn').addClass('btn-primary').addClass('accept').html('Accept').click(function () {
                $.get('/play/challenge-accept/id/' + $(this).closest('.challenge').data('challenger'));
            }));
            panelBody.append($('<button></button>').prop('type', 'button').addClass('btn').addClass('btn-default').addClass('decline').html('Decline').click(function () {
                $.get('/play/challenge-decline/id/' + $(this).closest('.challenge').data('challenger'));
            }));
        } else {
            // Show notice that challenge has been sent
            panel.append($('<div></div>').addClass('panel-heading').append($('<h3></h3>').addClass('panel-title').html('Challenge sent')));
            var panelBody = $('<div></div>').addClass('panel-body');
            panelBody.append($('<p></p>').html('Challenge sent to ' + data.challengeeUsername));
        }

        panel.append(panelBody);

        $('.players-online').after(panel);
    }
}

function addMember(member) {
    if (member.id > 0) {
        if ($('.players-online .players .user-name[data-id=' + member.id + ']').length == 0) {
            var memberRow = $('<li></li>').attr('data-id', member.id).addClass('user-name').html(member.info.username);
            if ($('#userID').val() && member.id != $('#userID').val()) {
                memberRow.append($('<a></a>').addClass('challenge').html('Challenge'));
            }
            $('.players-online .players').append(memberRow);
            $('.challenge').click(function() {
                $.get('/play/challenge/id/' + $(this).parent().data('id'));
            });
        }
    }
}

function removeMember(member) {
    $('.players-online .players .user-name[data-id=' + member.id + ']').remove();
}