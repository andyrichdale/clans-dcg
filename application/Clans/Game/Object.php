<?php

namespace Clans\Game;

abstract class Object
{
    public function canBeTargetedBy(\Clans\Ability $ability) {
        return false;
    }
}
