<?php

namespace Models;

/**
 * @Entity
 * @Table(name="decks")
 * @HasLifecycleCallbacks
 */
class Deck implements BaseModel
{
    /**
     * @Id
     * @Column(type="integer", name="deckID")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="text", name="name")
     */
    protected $name;

    /**
     * @ManyToOne(targetEntity="\Models\Login", inversedBy="decks")
     * @JoinColumn(name="userID", referencedColumnName="id")
     * @var \Models\Login
     */
    protected $creator;

    /**
     * @OneToMany(targetEntity="\Models\DeckCard", mappedBy="deck", fetch="EAGER")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $deckCards;

    /**
     * @Column(type="text", name="created")
     */
    protected $created;

    /**
     * @Column(type="text", name="modified")
     */
    protected $modified;

    /**
     * @Column(type="integer", name="published")
     */
    protected $published;

    /**
     * @Column(type="text", name="matchDeck")
     */
    protected $matchDeck;

    /**
     * @OneToOne(targetEntity="\Models\Login", mappedBy="selectedDeck")
     **/
    protected $selectedByLogin;

    public function __construct() {
        $this->deckCards = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getCreator() {
        return $this->creator;
    }

    public function setCreator(\Models\Login $creator) {
        $this->creator = $creator;
    }

    public function getCreated() {
        return $this->created;
    }

    public function setCreated($created) {
        $this->created = $created;
    }

    public function getModified() {
        return $this->modified;
    }

    public function setModified($modified) {
        $this->modified = $modified;
    }

    public function getPublished() {
        return $this->published;
    }

    public function setPublished($published) {
        $this->published = $published;
    }

    public function getMatchDeck() {
        return $this->matchDeck;
    }

    public function setMatchDeck($matchDeck) {
        $this->matchDeck = (int) $matchDeck;
    }

    public function getSelectedByLogin() {
        return $this->selectedByLogin;
    }

    public function setSelectedByLogin($selectedByLogin) {
        $this->selectedByLogin = $selectedByLogin;
    }

    public function getDeckCards() {
        return $this->deckCards;
    }

    public function getDeckSize() {
        $size = 0;

        foreach ($this->getDeckCards() as $deckCard) {
            /* @var $deckCard \Models\DeckCard */
            $size += $deckCard->getQuantity();
        }

        return $size;
    }

    public function toArray($shortform = false, $callingModel = null) {
        $result = array();

        $result['id'] = $this->getId();
        $result['name'] = $this->getName();
        $result['creator'] = $this->getCreator()->toArray(true);

        $result['deckCards'] = array();

        foreach ($this->getDeckCards() as $deckCard) {
            /* @var $deckCard \Models\DeckCard */
            $result['deckCards'][] = $deckCard->toArray(true);
        }

        $result['created'] = $this->getCreated();
        $result['modified'] = $this->getModified();
        $result['published'] = $this->getPublished();
        $result['matchDeck'] = $this->getMatchDeck();
        $result['selectedByLogin'] = $this->getSelectedByLogin() ? $this->getSelectedByLogin()->toArray(true) : array();

        return $result;
    }
}