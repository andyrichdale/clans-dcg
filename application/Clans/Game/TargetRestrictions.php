<?php

namespace Clans\Game;

class TargetRestrictions
{
    /**
     * @var \Clans\Type\Zone[]
     */
    protected $targetZones;
    
    /**
     * @var \Clans\Type\Card[]
     */
    protected $cardTypes;
    
    public function getTargetZones() {
        return $this->targetZones;
    }

    public function addTargetZones($targetZone) {
        $this->targetZones[] = $targetZone;
    }

    public function getCardTypes() {
        return $this->cardTypes;
    }

    public function addCardType($cardType) {
        $this->cardTypes[] = $cardType;
    }
}
