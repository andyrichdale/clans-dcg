<?php
require_once 'MyAction.php';

class CardsController extends MyAction
{
    public function allJsAction() {
        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/javascript');

        $this->_helper->layout()->disableLayout();

        $this->view->cards = $this->cardsService->getCards();

        Zend_Controller_Front::getInstance()->registerPlugin(new Application_Plugins_RemoveWhitespace());
    }
    public function gameSparksExportAction() {
        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/javascript');

        $this->_helper->layout()->disableLayout();

        $this->view->cards = $this->cardsService->getCards();
    }
}
