<?php

namespace Models;

/**
 * @Entity
 * @Table(name="`sets`")
 * @HasLifecycleCallbacks
 */
class Set implements BaseModel
{
    /**
     * @Id
     * @Column(type="string", name="abbrev")
     */
    protected $abbrev;
    
    /**
     * @Column(type="text", name="name")
     */
    protected $name;
    
    /**
     * @Column(type="datetime", name="released")
     */
    protected $released;
    
    /**
     * @OneToMany(targetEntity="\Models\Card", mappedBy="set", fetch="EAGER")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $cards;
    
    public function __construct() {
        $this->cards = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function getAbbrev() {
        return $this->abbrev;
    }
    
    public function setAbbrev($abbrev) {
        $this->abbrev = $abbrev;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function setName($name) {
        $this->name = $name;
    }
    
    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCards() {
        return $this->cards;
    }
    
    public function getReleased() {
        return $this->released;
    }
    
    public function setReleased($released) {
        $this->released = $released;
    }
    
    public function toArray($shortform = false, $callingModel = null) {
        $result = array();
        
        $result['abbrev'] = $this->getAbbrev();
        $result['name'] = $this->getName();
        $result['released'] = $this->getReleased();
        
        if (!$shortform) {
            $result['cards'] = array();
            
            foreach ($this->getCards() as $card) {
                $result['cards'][] = $card->toArray(true, $callingModel);
            }
        }
        
        return $result;
    }
}