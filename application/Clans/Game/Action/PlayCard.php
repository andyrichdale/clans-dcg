<?php

namespace Clans\Game\Action;
/**
 * The action used when a player plays a card to the game board
 */

class PlayCard extends \Clans\Game\BaseAction implements \Clans\Game\Action
{
    /* @var @gameCard \Models\Game\Card */
    private $gameCard;

    /* @var $zoneId int */
    private $zoneID;

    /* @var $slotID int */
    private $slotID;

    /* @var $gameCardId int */
    private $gameCardID;

    /* @var $resourceChange int */
    private $resourceChange;

    function setZoneID($zoneID) {
        $this->zoneID = $zoneID;
    }

    function setSlotID($slotID) {
        $this->slotID = $slotID;
    }

    function setGameCardID($gameCardID) {
        $this->gameCardID = $gameCardID;
    }

    public function getKey() {
        return "PlayCard";
    }

    public function getResolvedTriggerKey() {
        return "Deployed";
    }

    public function validateAction() {
        $gamesService = new \Clans\Service\Game();

        // Check if game card id is set
        if (!$this->gameCardID) {
            $this->messages[] = "Game card ID not set";
            return false;
        }

        // Check if zone id is set
        if (!$this->zoneID) {
            $this->messages[] = "Zone ID missing";
            return false;
        }

        // Check if slot id is set
        if (!$this->slotID) {
            $this->messages[] = "Slot ID missing";
            return false;
        }

        // Check if game card exists in db
        if ($gameCard = $gamesService->getGameCardByID($this->gameCardID)) {
            $this->gameCard = $gameCard;
            $playerResources = $this->game->getPlayerCurrentResources($this->playerNumber);

            // Check if player has enough resources to play card
            if ($playerResources >= $gameCard->getCard()->getCost()) {
                // Check if slot is already filled
                foreach ($this->game->getInPlayCards() as $gameCard) {
                    if ($gameCard->getZone() == $this->zoneID && $gameCard->getIndex() == $this->slotID) {
                        $this->messages[] = "Slot is already filled";
                        return false;
                    }
                }
                return true;
            } else {
                $this->messages[] = "Not enough resources";
                return false;
            }
        } else {
            $this->messages[] = "Game card not found";
            return false;
        }
    }

    public function payCosts() {
        $gamesService = new \Clans\Service\Game();

        // Pay resources
        $this->game->changePlayerResourcesUsed($this->playerNumber, $this->gameCard->getCard()->getCost());
        $gamesService->saveGame($this->game);

        $this->resourceChange = $this->gameCard->getCard()->getCost() * -1;
    }

    public function doEffect() {
        $gamesService = new \Clans\Service\Game();

        // Move card
        $gamesService->moveGameCardToZone($this->gameCard, $this->zoneID, $this->slotID);
    }

    public function setUpdates() {
        $this->updated->addUpdated(
            new \Clans\Game\Updated\GameCardMoved(
                $this->gameCard,
                $this->playerNumber == 1 ? \Models\Game\Card::ZONE_P1_HAND : \Models\Game\Card::ZONE_P2_HAND,
                $this->zoneID,
                $this->slotID
            )
        );

        $this->updated->addUpdated(
            new \Clans\Game\Updated\ResourcesUsed(
                $this->resourceChange,
                $this->playerNumber
            )
        );
    }
}
