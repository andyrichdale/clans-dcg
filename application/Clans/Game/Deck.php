<?php

namespace Clans\Game;

class Deck {
    
    /**
     * @var \Models\Deck
     */
    protected $originalDeck;
    
    /**
     * @var \Models\Card[]
     */
    protected $cards;
    
    public function __construct(\Models\Deck $deck) {
        $this->originalDeck = $deck;
        foreach ($deck->getDeckCards() as $deckCard) {
            for ($i = 0; $i < $deckCard->getQuantity(); $i++) {
                $this->cards[] = $deckCard->getCard();
            }
        }
        $this->shuffle();
    }
    
    public function shuffle() {
        /*$this->newCards = new \Doctrine\Common\Collections\ArrayCollection();
        while ($this->cards->count() > 0) {
            $index = mt_rand(0, $this->cards->count() - 1);
            $newCard = $this->cards->get($index);
            $this->newCards->add($newCard);
            $this->cards->remove($index);
        }
        $this->cards = $this->newCards;*/
        $order = array_map(create_function('$val', 'return mt_rand();'), range(1, count($this->cards)));
        array_multisort($order, $this->cards);
    }
    
    /**
     * @return \Models\Card[]
     */
    public function getCards() {
        return $this->cards;
    }
}
