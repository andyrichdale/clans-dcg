<?php

class MatchStates
{
    const ERROR         = -1;
    const CHALLENGE     = 0;
    const PREGAME       = 1;
    const GAME_ONE      = 2;
    const GAME_TWO      = 3;
    const GAME_THREE    = 4;
    const P1_WINS       = 5;
    const P2_WINS       = 6;
}
?>
