<?php
require_once 'MyAction.php';
require_once 'simple_html_dom.php';

class DeckController extends MyAction
{
    /**
     * View the list of decks
     */
    public function indexAction() {
        /*$userID = $this->getRequest()->getParam('user', false);

        // Retrieve decks, optionally filtered by userID
        $this->view->decks = $this->decksTable->fetchAllDecks($userID);

        if ($userID) {
            // Pass filtered user to view
            $this->view->userDecks = $this->loginsTable->find($userID)->current();
        }*/
    }

    /**
     * Add or remove a card from a deck
     */
    public function changeAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if (!$this->login) {
            exit;
        }

        $add = $this->getRequest()->getParam('add', false);
        $id = $this->getRequest()->getParam('id', false);
        $multiple = $this->getRequest()->getParam('multiple', false);
        $deckID = $this->getRequest()->getParam('deckID', false);

        if ($deckID) {
            // Load specific deck
            if ($deck = $this->decksService->getDeckByID($deckID)) {
                if ($deck->getCreator()->getId() != $this->login->getId()) {
                    // Disallow editing of deck by anyone except deck owner
                    exit;
                }
            } else {
                exit;
            }
        } else {
            $deck = $this->login->getSelectedDeck();
        }

        if ($id) {
            // Set created/modified fields
//            $deck->setModified('');

            $amount = $add == 'true' ? ($multiple == 'true' ? 4 : 1) : ($multiple == 'true' ? -4 : -1);

            $this->decksService->addCardToDeck($deck, $id, $amount);
        }

        //die($deck->deckID);
    }

    /**
     * Get the list of cards in a deck
     */
    public function getDeckAction() {
        $this->_helper->layout()->disableLayout();

        $id = $this->getRequest()->getParam('id', false);

        if ($id) {
            $deck = $this->decksTable->find($id)->current();

            if ($deck->userID && $this->session->loginID && $this->session->loginID == $deck->userID) {
                $this->view->myDeck = true;
            } else {
                $this->view->myDeck = false;
            }

            $this->view->deck = $deck;
        }
    }

    /**
     * Get additional deck statistics
     */
    public function getDeckDetailsAction() {
        $this->_helper->layout()->disableLayout();

        $id = $this->getRequest()->getParam('id', false);

        if ($id) {
            $deck = $this->decksTable->find($id)->current();
            $this->view->deck = $deck;
        }
    }

    /**
     * Get compact deck view for sidebar
     */
    public function getMydeckAction() {
        $this->_helper->layout()->disableLayout();

        if ($this->login) {
            if (($deckID = $this->_getParam('deckID', false)) !== false) {
                $deckID = (int) $deckID;
                if ($deckID == 0) {
                    $deck = $this->decksService->createDeck($this->login);
                } elseif ($deck = $this->decksService->getDeckByID($deckID)) {
                    $deck->getCreator()->getId() == $this->login->getId();
                }
                if ($deck) {
                    $this->login = $this->usersService->setSelectedDeck($this->login, $deck);
                }
            }
            if (count($this->login->getDecks()) == 0) {
                $this->login = $this->usersService->setSelectedDeck($this->login, $this->decksService->createDeck($this->login));
            }
            $this->view->deck = $this->login->getSelectedDeck();
        }
    }

    /**
     * Get compact deck list for sidebar
     */
    public function getMydecksAction() {
        $this->_helper->layout()->disableLayout();

        if ($this->login) {
            $this->view->decks = $this->login->getDecks();
        }
    }

    /**
     * View an individual deck
     */
    public function viewAction() {
        $id = $this->getRequest()->getParam('id', false);
        $deckName = $this->getRequest()->getParam('deckName', false);
        $loadDeck = $this->getRequest()->getParam('loadDeck', false);
        $publish = $this->getRequest()->getParam('publish', false);

        if ($id) {
            $deck = $this->decksTable->find($id)->current();

            if ($deck) {
                if ($deck->userID && $this->session->loginID && $this->session->loginID == $deck->userID) {
                    $this->view->myDeck = true;
                } else {
                    $this->view->myDeck = false;
                    if ($deck->deckID == $this->session->deck->deckID) {
                        $this->view->errors[] = "Please <a href='/login'>login</a> to rename and publish your deck";
                    }
                }

                if ($deckName !== false || $publish !== false) {
                    if ($this->view->myDeck) {
                        if ($deckName !== false) {
                            $deck->name = $deckName;
                        }
                        if ($publish !== false) {
                            $deck->published = $publish;
                        }
                        $deck->save();
                        die(json_encode(array('success' => 'Deck updated')));
                    } else {
                        die(json_encode(array('error' => 'Error updating this deck')));
                    }
                }

                if ($loadDeck) {
                    $this->session->deck = $deck;
                    die(json_encode(array('success' => 'Deck loaded')));
                }

                $this->view->deck = $deck;
            }
        }
    }

    /**
     * Start a new "My Deck"
     */
    public function newDeckAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $this->session->deck = $this->decksTable->createRow();
    }
}
