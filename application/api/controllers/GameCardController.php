<?php
require APPLICATION_PATH . '/api/controllers/BaseController.php';

class Api_GameCardController extends Api_BaseController
{
    /* @var $gameService Clans\Service\Game */
    private $gameService;
    
    public function init() {
        parent::init();
        
        $this->gameService = new \Clans\Service\Game();
        $this->gameService->returnObject = false;
    }
    
    public function deleteAction() {
    }

    public function getAction() {
        if ($this->id) {
            $card = $this->gameService->getGameCard($this->id);
            
            if ($card) {
                $this->returnSuccess($card);
            } else {
                $this->returnError('Card not found');
            }
        } else {
            $this->returnError('ID not present');
        }
    }

    public function indexAction() {
        $gameID = $this->_getParam('gameID', false);
        
        if ($gameID) {
            $game = $this->gameService->getGameByID($gameID);
            
            if ($game) {
                $zoneID = $this->_getParam('zoneID', false);
                
                if ($zoneID) {
                    $this->returnSuccess($this->gameService->getGameCardsForZone($game, $zoneID));
                } else {
                    $this->returnSuccess($game->getGameCards());
                }
            } else {
                $this->returnError('Game not found');
            }
        }
        
        
    }

    public function postAction() {
        $this->returnError('Permission denied');
    }

    public function putAction() {
        // TODO: check if allowed
    }
}
