function Match(pusher) {
    var self = this;
    
    this.timeStarted = 0;
    this.timeNow = 0;
    this.tickTime = 5;
    
    this.p1Life = 30;
    this.p2Life = 30;
    this.p1Resources = 0;
    this.p2Resources = 0;
    
    this.id = $('#matchID').val();
    this.gameId = $('#gameID').val();
    this.pusher = pusher;
    this.playerNumber = $('#playerNumber').val();
    this.opponentNumber = this.playerNumber == 1 ? 2 : 1;
    this.opponentHandZone = this.playerNumber == 1 ? 1 : 0;
    this.playerHandZone = this.playerNumber == 1 ? 0 : 1;
    //this.playerSupportZone = this.playerNumber == 1 ? 6 : 7;
    this.playerPlayZone = this.playerNumber == 1 ? 8 : 9;
    this.opponentPlayZone = this.playerNumber == 1 ? 9 : 8;
    this.playerDeckZone = this.playerNumber == 1 ? 2 : 3;
    this.opponentDeckZone = this.playerNumber == 1 ? 3 : 2;
    
    this.playerLifeCounter = $('.playerLife.me');
    this.opponentLifeCounter = $('.playerLife.opponent');
    this.playerResourceCounter = $('.playerResources.me');
    this.opponentResourceCounter = $('.playerResources.opponent');
    
    this.zones = {};
    this.zoneGameCards = {};
    
    this.myHandContainer = this.zones[0 + parseInt(this.playerNumber) - 1] = $('.hand.me');
    this.myDeckContainer = this.zones[2 + parseInt(this.playerNumber) - 1] = $('.deck.me');
    this.myDiscardedContainer = this.zones[4 + parseInt(this.playerNumber) - 1] = $('.discard.me');
    //this.mySupportContainer = this.zones[6 + parseInt(this.playerNumber) - 1] = $('.support.player');
    this.myPlayContainer = this.zones[8 + parseInt(this.playerNumber) - 1] = $('.play.me');
    this.myDeckSizeContainer = $('.deckSize.me');
    
    this.opponentHandContainer = this.zones[2 - parseInt(this.playerNumber)] = $('.hand.opponent');
    this.opponentDeckContainer = $('.deck.opponent');
    this.opponentDiscardedContainer = $('.discarded.opponent');
    //this.opponentSupportContainer = this.zones[8 - parseInt(this.playerNumber)] = $('.support.opponent');
    this.opponentPlayContainer = this.zones[10 - parseInt(this.playerNumber)] = $('.play.opponent');
    this.opponentDeckSizeContainer = $('.deckSize.opponent');
    
    this.tickProgressContainer = $('#gameTimer #tickProgress .bar');
    this.gameTimeContainer = $('#gameTimer #timer');
    
    this.matchChannel = pusher.subscribe('match-' + this.id);
    this.privateChannel = pusher.subscribe('private-user-' + $('#userID').val());

    this.matchChannel.bind('chat-message', function(data) {
        //$('.chat-box .chat').prepend(data.messageHTML);
        appendChat(data.message, data.author, data.type);
    });
    
    this.privateChannel.bind('update', function(data) {
        for (var update in data) {
            switch (update) {
                case "GameCardMoved":
                    // Move a game card from one zone to another
                    var gameCard = data.GameCardMoved.gameCard;
                    var slotTo = data.GameCardMoved.slotTo;
                    var zoneFrom = data.GameCardMoved.zoneFrom;
                    var zoneTo = data.GameCardMoved.zoneTo;

                    // Move card visuals to zone slot
                    var cardRender = $('.card-render[data-id=' + gameCard.id + ']');
                    
                    if (cardRender.length == 0) {
                        // If we are drawing from the deck, the card render will not exist yet - create it
                        var gameCardTemplate = Handlebars.templates['gameCard.tmpl'];
                        var html = gameCardTemplate(gameCard);
                        $('.zone[data-zone=' + zoneFrom + ']').append(html);

                        cardRender = $('.card-render[data-id=' + gameCard.id + ']');
                    } else if (cardRender.find('.card').hasClass('facedown')) {
                        // If we are moving from deck to in play or from opponent's hand to play the card
                        // will need to go from facedown to face up
                        var gameCardTemplate = Handlebars.templates['gameCard.tmpl'];
                        var html = gameCardTemplate(gameCard);
                        $('.card-render[data-id=' + gameCard.id + ']').replaceWith(html);

                        cardRender = $('.card-render[data-id=' + gameCard.id + ']');
                    }

                    var newParent = slotTo ? $('.zone[data-zone=' + zoneTo + '] .playSlot[data-slot=' + slotTo + ']') : $('.zone[data-zone=' + zoneTo + ']');
                    var oldZone = $('.zone[data-zone=' + zoneFrom + ']');

                    console.log('moving ' + cardRender + ' to ' + newParent);

                    if (oldZone.hasClass('deck')) {
                        oldZone.css('position', 'static');
                    }
                    if (newParent.hasClass('playSlot')) {
                        newParent.css('position', 'static');
                    }
                    var startTop = cardRender.position().top;
                    var startLeft = cardRender.position().left;

                    if (oldZone.hasClass('deck')) {
                        oldZone.css('position', 'relative');
                    }
                
                    cardRender.detach().appendTo(newParent);

                    if (newParent.hasClass('hand')) {
                        newParent.attr('data-size', newParent.children().length);
                    }
                    if (oldZone.hasClass('hand')) {
                        oldZone.attr('data-size', oldZone.children().length);
                    }

                    var newTop = cardRender.position().top;
                    var newLeft = cardRender.position().left;
                
                    cardRender.css({ 'position': 'absolute' });
                    cardRender.css({ 'top': startTop, 'left': startLeft });
                
                    cardRender.animate({ 'top': newTop, 'left': newLeft }, 220, function () {
                        cardRender.removeAttr('style');
                        if (newParent.hasClass('playSlot')) {
                            newParent.css('position', 'relative');
                        }
                    });

                    attachEvents(self);
                
                    break;
                case "ResourcesUsed":
                    // Update the resource counter for a player
                    var resourceChange = data.ResourcesUsed.resourceChange;
                    var playerNumber = data.ResourcesUsed.playerNumber;

                    var selector = playerNumber == self.playerNumber ? 'player' : 'opponent';
                    self.changeResources(resourceChange, selector);

                    break;
                default:
                    // Unkown update
                    break;
            }
        }
    });
    
    self.gameTimerUpdate = false;
    
    /*console.log('setting update timer');
    clearInterval(self.gameTimerUpdate);
    self.gameTimerUpdate = setInterval(function() { self.updateGameTimer(); }, 10);*/
    
    this.privateChannel.bind('pusher:subscription_succeeded', function() {
        
    });
    
    $.get('/api/game/' + self.gameId, function (data) {
        self.updateFromAPI(data);
    });

    this.updateFromAPI = function(data) {
        self.timeStarted = data.timeStarted;
        self.tickTime = data.match.tickTime;
        self.p1Life = data.p1Life;
        self.p2Life = data.p2Life;
        self.p1Resources = data.p1Resources;
        self.p2Resources = data.p2Resources;

        // Set clock starting point
        /*var now = Math.floor(Date.now() / 1000);
        var secondsPassed = now - self.timeStarted;*/

        // Start the clock
        console.log('setting update timer');
        clearInterval(self.gameTimerUpdate);
        self.gameTimerUpdate = setInterval(function () { self.updateClock(); }, 10);

        self.zoneGameCards = {};

        for (var i = 0; i < data.gameCards.length; i++) {
            var gameCard = data.gameCards[i];

            if (!self.zoneGameCards[gameCard.zone]) {
                self.zoneGameCards[gameCard.zone] = [];
            }

            self.zoneGameCards[gameCard.zone].push(gameCard);
        }

        console.log(self.zoneGameCards);

        self.drawZones();

        self.highlightCardsValidToPlay();
    };

    this.drawZones = function () {
        var gameCardTemplate = Handlebars.templates['gameCard.tmpl'];
        $('.zone .playSlot, .zone.hand').html('');

        for (var zoneID in self.zoneGameCards) {
            var zone = self.zoneGameCards[zoneID];
            if (zoneID == self.playerDeckZone) {
                self.myDeckSizeContainer.html(zone.length);
            } else if (zoneID == self.opponentDeckZone) {
                self.opponentDeckSizeContainer.html(zone.length);
            } else if (self.zones[zoneID]) {
                for (var j = 0; j < zone.length; j++) {
                    //var cardId = zone[j].card ? zone[j].card.id : 0;
                    //console.log('found cardid ' + cardId + ' for zoneid ' + zoneID);
                    //var dbCard = cardDB[cardId];
                    //var gameCard = new GameCard(zone[j].id, dbCard);
                    //self.zones[zoneID].append(gameCard.render());
                    
                    //TODO: create gameCard template extends card template
                    //var cardData = zone[j].card ? zone[j].card : {};
                    var card = zone[j];

                    var html = gameCardTemplate(card);

                    if (zoneID == self.playerPlayZone || zoneID == self.opponentPlayZone) {
                        self.zones[zoneID].find('.playSlot[data-slot=' + card.index + ']').append(html);
                    } else if (zoneID == self.playerHandZone || zoneID == self.opponentHandZone) {
                        self.zones[zoneID].append(html);
                        self.zones[zoneID].attr('data-size', self.zones[zoneID].find('.card-render').length);
                    }
                }
            }
        }

        attachEvents(self);

        //updateGameZoneScale();
    };
    
    this.updateNOTUSED = function(data) {
        switch (data.u) {
            case 'syncGame':
            case 0:
                for (var i = 0; i < data.p.length; i+=2) {
                    // Loop over each zone
                    var zone = data.p[i];
                    var zoneGameCards = data.p[i+1];
                    
                    if (zone == self.playerDeckZone) {
                        self.myDeckSizeContainer.html(zoneGameCards.length / 3);
                    } else if (zone == self.opponentDeckZone) {
                        self.opponentDeckSizeContainer.html(zoneGameCards.length / 3);
                    } else if (self.zones[zone]) {
                        // TODO: check if this works
                        self.zones[zone].html('');
                        console.log('emptying game zone');
                        for (var j = 0; j < zoneGameCards.length; j+=3) {
                            // Render each card in zone
                            var dbCard = cardDB[zoneGameCards[j]];
                            var gameCardID = zoneGameCards[j+1];
                            var index = zoneGameCards[j+2];

                            if (dbCard) {
                                var gameCard = new GameCard(gameCardID, dbCard);
                                self.zones[zone].append(gameCard.render());
                                console.log('adding card render to game zone');
                            }
                        }
                    }
                }
            break;
            case 'syncZone':
            case 1:
                var zone = data.p.shift();
                // Display zone game cards
                if (zone == self.playerDeckZone) {
                    self.myDeckSizeContainer.html(data.p.length / 2);
                } else if (zone == self.opponentDeckZone) {
                    self.opponentDeckSizeContainer.html(data.p.length / 2);
                } else if (self.zones[zone]) {
                    self.zones[zone].html('');
                    for (var i = 0; i < data.p.length; i+=2) {
                        var dbCard = cardDB[data.p[i]];
                        var index = data.p[i+1];
                        console.log('zone ' + zone + ' has card at index ' + index + ':');
                        console.log(dbCard);
                        if (dbCard) {
                            var card = new Card(dbCard);
                            console.log('rendering card');
                            self.zones[zone].append(card.render());
                        }
                    }
                }
            break;
            /*case 'deckSize' :
                if (data.player && data.player == this.playerNumber) {
                    if (data.size) {
                        this.myDeckContainer.find('deckSize').html(data.size);
                    }
                }
            break;*/
            case 'gameDetails':
            case 3:
                var timeStarted = data.p[0];
                var timeNow = data.p[1];
                var tickTime = data.p[2];
                var p1Life = data.p[3];
                var p2Life = data.p[4];
                var p1Resources = data.p[5];
                var p2Resources = data.p[6];
                
                self.playerLifeCounter.html(p1Life);
                self.opponentLifeCounter.html(p2Life);
                self.playerResourceCounter.html(p1Resources);
                self.opponentResourceCounter.html(p2Resources);
                self.timeStarted = timeStarted;
                self.timeNow = timeNow;
                self.tickTime = tickTime;
                
                //TODO: add on elapsed time
                // current time = timeNow + elapsedTime - timeStarted
            break;
        }
        
        attachEvents(self);
    };
    
    /*this.matchChannel.bind('update', function(data) {
        self.update(data);
    });*/
    
    this.privateChannel.bind('chat-message', function(data) {
        //$('.chat-box .chat').prepend(data.messageHTML);
        appendChat(data.message, data.author, data.type);
    });
    
    // mouse events
    $('.playSlot').hover(
        function(e) {
            // If we are dragging a card and this is a valid zone, highlight this zone
            $('.playSlot').removeClass('highlighted');
            $('.playSlot').removeClass('invalid');
            
            if (getDraggingCard().length > 0) {
                console.log('found dragging card');
                if ($(this).hasClass('valid')) {
                    $(this).addClass('highlighted');
                } else {
                    $(this).addClass('invalid');
                }
            } else {
                console.log('not dragging card');
            }
        },
        function(e) {
            $('.playSlot').removeClass('highlighted');
            $('.playSlot').removeClass('invalid');
        }
    );
    
    $(document).click(function(e) {
        console.log('doc click');
        if ($('.card-render.clicked').length > 0) {
            if ($('.zone.valid.highlighted').length > 0) {
                // Play card to zone
                console.log('playing card to zone');
            } else {
                // Deselect card
                console.log('deselecting card');
                $('.card-render.clicked').removeClass('clicked');
                $('.zone').removeClass("valid");
            }
        }
    });

    this.highlightCardsValidToPlay = function () {
        $('.card-render').removeClass('validToPlay');
        $('.hand.me .card-render').each(function (e) {
            var cost = parseInt($(this).find('.card-resources').html());
            var playerResources = parseInt(self.playerResourceCounter.html());

            if (cost <= playerResources) {
                $(this).addClass('validToPlay');
            }
        });
    };
    
    this.changeResources = function (amount, who) {
        // Update resource counter
        if (!who || who == 'player') {
            var playerNewResources = parseInt(self.playerResourceCounter.html()) + amount;
            self.playerResourceCounter.html(playerNewResources);
        }
        
        if (!who || who == 'opponent') {
            var opponentNewResources = parseInt(self.opponentResourceCounter.html()) + amount;
            self.opponentResourceCounter.html(opponentNewResources);
        }

        self.highlightCardsValidToPlay();
    };
    
    this.hitMaxTick = false;

    this.updateClock = function () {
        var now = Math.floor(Date.now() / 1000);
        var secondsPassed = now - self.timeStarted;

        //console.log(secondsPassed);

        if (secondsPassed < 0) {
            $('#wait').show();
            $('#wait .seconds-to-wait').html(secondsPassed * -1);
        } else {
            $('#wait').hide();
        }

        var currentTickProgress = Math.abs(secondsPassed % self.tickTime);

        // If we've reached a new tick, update local resource counter and refresh game details from server
        if (currentTickProgress == 0 && self.hitMaxTick) {
            console.log('adding 1 to resources');
            self.changeResources(1);
            self.hitMaxTick = false;
            //$.get('/match/get-details/id/' + self.id);
        }

        // If we're one second away from a tick, set hitMaxTick
        if (secondsPassed > 0 && currentTickProgress == self.tickTime - 1) {
            self.hitMaxTick = true;
        }

        var clockSeconds = secondsPassed % 60;
        var angle = clockSeconds * 6;

        var container = document.querySelector('.seconds-container');
        container.angle = angle;
        container.style.transform = 'rotateZ(' + angle + 'deg)';
    };
                
    /*this.updateGameTimer = function() {
        // TESTING
        self.timeStarted = 1472070500;
        self.tickTime = 5;
        // /TESTING
        
        if (self.timeStarted && self.tickTime) {
            var now = Math.floor(Date.now() / 1000);
            var secondsPassed = now - self.timeStarted;
            
            var minutesPassed = Math.floor(secondsPassed / 60);
            var minuteTimer = minutesPassed < 10 ? "0" + minutesPassed : minutesPassed;
            var secondTimer = secondsPassed  % 60;
            secondTimer = (secondTimer < 10 ? "0" + secondTimer : secondTimer);
            self.gameTimeContainer.html(minuteTimer + ":" + secondTimer);
            
            //console.log('Now: ' + now);
            //console.log('Seconds passed: ' + secondsPassed);
            
            // Calculate progression to next tick
            var currentTickProgress = secondsPassed % self.tickTime;
            
            //console.log('Tick progress: ' + currentTickProgress);
            //console.log('Hit max tick: ' + self.hitMaxTick);
            
            // Update tick progress bar
            self.tickProgressContainer.css('height', Math.round((currentTickProgress / self.tickTime) * 100) + '%');
            
            // If we've reached a new tick, update local resource counter and refresh game details from server
            if (currentTickProgress == 0 && self.hitMaxTick) {
                console.log('adding 1 to resources')
                self.changeResources(1);
                self.hitMaxTick = false;
                //$.get('/match/get-details/id/' + self.id);
            }
            
            // If we're one second away from a tick, set hitMaxTick
            // TODO: check if workds
            if (currentTickProgress == self.tickTime - 1) {
                self.hitMaxTick = true;
            }
        } else {
            //console.log('Time and/or tick time not set');
        }
    };*/
    
    // Attach event for clicking draw card button
    $('#drawCardButton').click(function() {
        doGameAction(self.gameId, { 'game-action': 'draw-card' });
    });
}

// Perform a game action
function doGameAction(gameId, params) {
    $.ajax({
        method: 'PATCH',
        url: '/api/game/' + gameId,
        data: params
    }).done(function (returnData) {
        console.log('game action success');
        console.log(returnData);
    }).fail(function (returnData) {
        console.log('game action fail');
        console.log(returnData);
    });
    /*$.post('/match/' + matchId + '/action', params).done(function(data) {
        var data = jQuery.parseJSON(data);
        if (data.success) {
            // TODO: handle update response
            // OR do we handle board updates only from websockets?
        } else {
            for (var i = 0; i < data.messages.length; i++) {
                logMessage('error', data.messages[i]);
            }
        }
    });*/
}

/*var cardClickHandler = function(e) {
    console.log('card click');
    var isPlayer = $(this).parent().hasClass("player");
    var isHand = $(this).parent().hasClass("hand");

    if (isPlayer) {
        if ($(this).hasClass("clicked")) {
            $("img.card-render").removeClass("clicked");
            $('.zone').removeClass("valid");
        } else {
            $("img.card-render").removeClass("clicked");
            $(this).addClass("clicked");

            if (isHand) {
                //TODO: write this function
                $(".support.player").addClass("valid");
                $(".play.player").addClass("valid");
            }
        }
    }

    e.stopPropagation();
};*/
            
function logMessage(type, message) {
    appendChat(message, null, type);
}

function attachEvents(match) {
    // Player clicks on card
    //$("img.card-render.no-events").bind('mouseup', cardClickHandler);
    
    $(".card-render.no-events").draggable({
        containment: "#board",
        opacity: 0.7,
        helper: function (e) {
            console.log(e);
            return $('<svg>' +
                    '<path class="targetArrow targetArrow0" d="M 100 350 l 150 -300" stroke="red" stroke-width="3" fill="none" />' +
                    //'<path class="targetArrow targetArrow1" d="M 100 350 l 150 -300" stroke="red" stroke-width="3" fill="none" />' +
                    //'<path class="targetArrow targetArrow2" d="M 100 350 l 150 -300" stroke="red" stroke-width="3" fill="none" />' +
                    //'<path class="targetArrow targetArrow3" d="M 100 350 l 150 -300" stroke="red" stroke-width="3" fill="none" />' +
                    //'<path class="targetArrow targetArrow4" d="M 100 350 l 150 -300" stroke="red" stroke-width="3" fill="none" />' +
                '</svg>');
            //return $('<div>BLARG</div>');
        },
        appendTo: "#drawings",
        start: function(e, ui) {
            //$(this).unbind('mouseup');
            
            //$(this).data('ogLeft', $(this).css('left'));
            //$(this).data('ogTop', $(this).css('top'));

            // Differentiate cards that require targetting

            $(this).addClass('dragging');
            
            $('.playSlot').removeClass('valid');
            
            var isPlayer = $(this).closest('.zone').hasClass("player");
            var zone = $(this).closest('.zone').data('zone');
            
            //if (isPlayer) {
                switch (zone) {
                    case match.playerHandZone:
                        // Highlight valid play zones
                        //match.mySupportContainer.addClass('valid');
                        console.log('dragging from hand');
                        //match.myPlayContainer.addClass('valid');
                        $('.zone.play.me .playSlot:empty').addClass('valid');
                        break;
                    /*case match.playerSupportZone:
                        // Highlight valid play zones
                        match.myPlayContainer.addClass('valid');
                        match.myHandContainer.addClass('valid');
                        break;*/
                    case match.playerPlayZone:
                        // Highlight valid play zones
                        //match.mySupportContainer.addClass('valid');
                        console.log('dragging from play');
                        match.myHandContainer.addClass('valid');
                        break;
                }
            //}
            
        },
        drag: function(e) {
            // If we are dragging a card and this is a valid zone, highlight this zone
            // TODO: figure out how to grab mouse/touch drag position for zone highlighting
            // so we can move that here
            var xOffset = $(e.target).width();
            var yOffset = $(e.target).height();

            var startX0 = $(e.target).position().left + (xOffset / 2);
            var startY0 = $(e.target).position().top + (yOffset / 2);
            var startX1 = $(e.target).position().left;
            var startY1 = $(e.target).position().top;
            var startX2 = $(e.target).position().left + xOffset;
            var startY2 = $(e.target).position().top;
            var startX3 = $(e.target).position().left + xOffset;
            var startY3 = $(e.target).position().top + yOffset;
            var startX4 = $(e.target).position().left;
            var startY4 = $(e.target).position().top + yOffset;

            var endX = e.pageX - $('#game').offset().left;
            var endY = e.pageY - $('#game').offset().top;

            /*$('.targetArrow1').attr('d', 'M ' + startX1 + ' ' + startY1 + ' L ' + endX + ' ' + endY);
            $('.targetArrow2').attr('d', 'M ' + startX2 + ' ' + startY2 + ' L ' + endX + ' ' + endY);
            $('.targetArrow3').attr('d', 'M ' + startX3 + ' ' + startY3 + ' L ' + endX + ' ' + endY);
            $('.targetArrow4').attr('d', 'M ' + startX4 + ' ' + startY4 + ' L ' + endX + ' ' + endY);*/
            
            $('.targetArrow0').attr('d', 'M ' + startX0 + ' ' + startY0 + ' L ' + endX + ' ' + endY);
        },
        stop: function(e) {
            //$(this).bind('mouseup', cardClickHandler);

            $('.targetArrow').each(function () { $(this).parent().remove() });
                
            // Check if hovered target is valid
            var validHoveredTarget = $('.highlighted.valid');

            if (validHoveredTarget.length > 0) {
                // Determine type of drag/drop
                if (validHoveredTarget.hasClass('playSlot')) {
                    // Get slot data
                    validHoveredTarget = validHoveredTarget.eq(0);
                    var slotID = validHoveredTarget.data('slot');
                    var zoneID = validHoveredTarget.closest('.zone').data('zone');
                    var gameCardID = $(this).data('id');
                
                    var data = { 'game-action': 'play-card', 'gameCardID': gameCardID, 'zone': zoneID, 'index': slotID };
                    //console.log('about to drop');
                    //console.log(data);
                    //return;

                    // Send game action to server
                    doGameAction(match.gameId, data);

                    $(this).removeClass('dragging');
                    $(this).css({ 'left': 0, 'top': 0 });
                }
                        
            } else {
                // Reset to original 
                $(this).removeClass('dragging');
                $(this).css({ 'left': 0, 'top': 0 });
            }
            
            $('.playSlot').removeClass('valid');
        }
    });
    
    $(".card-render.no-events").removeClass("no-events");
}

function appendChat(message, author, type) {
    var chat = $('<li></li>').addClass(type).addClass('chat-row');
    
    if (author) {
        var chatAuthor = $('<div></div>').text(author + ": ").addClass('chat-author');
        chat.append(chatAuthor);
    }
    
    var chatMessage = $('<div></div>').text(message).addClass('chat-message');
    chat.append(chatMessage);
    
    $('.chat-box .chat').prepend(chat);
    
    updateNewChats();
}

function updateNewChats() {
    $('.chat-box .chat-row:visible').addClass('seen');
    
    var number = $('.chat-box .chat-row:not(.seen)').length;
    $('#chat-container .badge').html(number == 0 ? "" : number);
}

function getDraggingCard() {
    return $('.card-render.dragging');
}
/*
function setHandWidth() {
    $('.zone.hand').each(function() {
        var parentWidth = $(this).parent().width();
        var deckWidth = $(this).prev('.deck').width();
        var less = parentWidth / 50;
        
        console.log("parentWidth = " + parentWidth + ", deckWidth = " + deckWidth);
        $(this).width(parentWidth - deckWidth - less);
    });
}*/

$(document).ready(function () {
    if (fullScreenApi.supportsFullScreen) {
        console.log('Fullscreen ahead!');

        $('#ready').on('click', function () {
            $('#readytoplay').hide();
            $('#game').css('opacity', 1);
            fullScreenApi.requestFullScreen(document.getElementById('game'));
            //rescaleGameBoard();
        });
    } else {
        console.log('Fullscreen not supported');
    }

    $('#chat-container .dropdown-toggle').on('shown.bs.dropdown', function(){
        updateNewChats();
    });
});

/*window.onmousemove = function (e) {
    var x = e.clientX,
        y = e.clientY;

    var draggingCard = getDraggingCard();

    if (draggingCard.length > 0) {
        $('.card-render.clicked').css({ 'top': (y + 20), 'left': (x + 20) });
    }
};*/

window.onresize = function (e) {
    //updateGameZoneScale();
    //rescaleGameBoard();
};

function rescaleGameBoard() {
    var sanity = 1000;
    var scale = 1.0;
    var increment = 0.01;

    while ($('#game').height() < window.innerHeight && $('#game').width() < window.innerWidth) {
        scale += increment;
        $('#game').css('transform', 'scale(' + scale + ')');
    }

    scale -= increment;
    $('#game').css('transform', 'scale(' + scale + ')');
}

function updateGameZoneScale() {
    var viewportHeight = window.innerHeight;
    var scale = (viewportHeight * 0.198) / 342;

    var marginX = (254 * (1 - scale)) * -1;
    var marginY = (342 * (1 - scale)) * -1;

    $('.zoneCards .card').css({
        'transform': 'scale(' + scale + ')',
        'margin-right': marginX,
        'margin-bottom': marginY
    });

    /*$('.zoneCards').each(function () {
        var sanity = 1000;
        while ($(this).height() > 260 && sanity > 0) {
            console.log('zoneCards height ' + $(this).height());
            $(this).find('.card').each(function () {
                var marginRight = $(this).css('margin-right').replace(/[^-\d\.]/g, '');
                console.log('decrementing margin ' + marginRight);
                $(this).css('margin-right', marginRight - 1);
            });
            sanity--;
        }
    });*/

    /*var x = viewportHeight / 100;
    var a = 0.616;
    var b = -12.772;
    var c = 69.749;
    var topOffset = (a * Math.pow(x, 2)) + (b * x) + (c);

    $('.zoneCards').css('top', topOffset * -1 + 'vh');*/
}

//window.onresize = setHandWidth;

//TODO: update jquery / jqueryUI and remove these two functions
/*
jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();

$.curCSS = function (element, attrib, val) {
    $(element).css(attrib, val);
};*/
            
// From: http://stackoverflow.com/questions/951021/what-do-i-do-if-i-want-a-javascript-version-of-sleep
function sleepFor( sleepDuration ){
    var now = new Date().getTime();
    while(new Date().getTime() < now + sleepDuration){ /* do nothing */ } 
}

// From: http://stackoverflow.com/a/6333775
function canvas_arrow(context, fromx, fromy, tox, toy){
    var headlen = 10;   // length of head in pixels
    var angle = Math.atan2(toy-fromy,tox-fromx);
    context.moveTo(fromx, fromy);
    context.lineTo(tox, toy);
    context.lineTo(tox-headlen*Math.cos(angle-Math.PI/6),toy-headlen*Math.sin(angle-Math.PI/6));
    context.moveTo(tox, toy);
    context.lineTo(tox-headlen*Math.cos(angle+Math.PI/6),toy-headlen*Math.sin(angle+Math.PI/6));
}

function drawCardAnimation(cardID, isMe) {
    // TODO: use isMe to determine if we're drawing or our opponent is
    var dbCard = cardDB[cardID];
    var board = $('#board');
    
    if (dbCard) {
        var card = new Card(dbCard);
        var theDeckCard = $('<img/>').attr('src', '/asset/images/card/clans-cardback.jpg').addClass('card-render');
        var theHandCard = card.getAsElement();
        
        theDeckCard.css({ 'position': 'absolute', 'bottom': '0px', 'left': '0px' });
        board.append(theDeckCard);
    }
}