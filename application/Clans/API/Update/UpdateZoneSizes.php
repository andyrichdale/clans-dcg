<?php
namespace Clans\API\Update;

// APPLICATION_PATH . "/Classes/API/Update/UpdateBase.php";

//use Clans\API\Update\UpdateBase;

class UpdateZoneSizes extends UpdateBase
{
    public function __construct(\Models\Game $game) {
        $this->id = 2;

        $gamesService = new \Clans\Service\Game();

        $zones = array(
            \Models\Game\Card::ZONE_P1_HAND,
            \Models\Game\Card::ZONE_P1_DECK,
            \Models\Game\Card::ZONE_P1_DISCARDED,
            \Models\Game\Card::ZONE_P2_HAND,
            \Models\Game\Card::ZONE_P2_DECK,
            \Models\Game\Card::ZONE_P2_DISCARDED,
        );

        foreach ($zones as $zoneID) {
            $this->params[] = $zoneID;
            $this->params[] = count($gamesService->getGameCardsForZone($game, $zoneID));
        }
    }
}
