<?php
require_once APPLICATION_PATH . '/api/controllers/BaseController.php';
require_once 'Pusher/Pusher.php';

class Api_GameController extends Api_BaseController
{
    /* @var $gameService Clans\Service\Game */
    private $gameService;

    /* @var $pusher Pusher */
    public $pusher;

    const PUSHER_API_KEY = '643bcb47c8db76202020';
    const PUSHER_SECRET = 'd0d3d71aa508e3cb607a';
    const PUSHER_APP_ID = 6166;

    public function init() {
        parent::init();

        $this->gameService = new Clans\Service\Game();

        $this->pusher = new Pusher(self::PUSHER_API_KEY, self::PUSHER_SECRET, self::PUSHER_APP_ID);
    }

    public function getAction() {
        if ($this->id) {
            $game = $this->gameService->getGameByID($this->id);

            if ($game) {
                $this->returnSuccess($game->toArray());
            } else {
                $this->returnError('Game not found', 404);
            }
        } else {
            $this->returnError('ID not present');
        }
    }

    public function indexAction() {
        $games = array();

        foreach ($this->gameService->getGames() as $game) {
            $games[] = $game->toArray();
        }

        $this->returnSuccess($games);
    }

    public function deleteAction() {
    }

    public function postAction() {
        $this->returnError('Permission denied', 401);
    }

    public function putAction() {
        $this->returnError('Permission denied', 401);
    }

    public function patchAction() {

        // Update a game via a game action
        if ($this->id) {
            $game = $this->gameService->getGameByID($this->id);
            $playerNumber = $this->whosAsking->getId() == $game->getMatch()->getP1()->getId() ? 1 : ($this->whosAsking->getId() == $game->getMatch()->getP2()->getId() ? 2 : 0);

            $p1Channel = "private-user-{$game->getMatch()->getP1()->getId()}";
            $p2Channel = "private-user-{$game->getMatch()->getP2()->getId()}";

            // Check that the asker is participating in the game
            if ($playerNumber > 0) {
                // Get the game action
                $gameActionParam = $this->_getParam('game-action', false);
                try {
                    if ($gameAction = $this->gameService->getGameAction($gameActionParam, $game, $playerNumber, $this->_getAllParams())) {
                        // Execute the action
                        $result = $gameAction->execute();

                        // TODO: think about returning recursive updates, eg effects that trigger more effects
                        // Have to merge all updates together in order
                        if ($result->success) {
                            $formattedList = $result->updatedList->formattedList();
                            $privateFormattedList = $result->updatedList->formattedList(true);

                            if ($playerNumber == \Models\Game::PLAYER_ONE) {
                                $publicChannel = $p1Channel;
                                $privateChannel = $p2Channel;
                            } else {
                                $publicChannel = $p2Channel;
                                $privateChannel = $p1Channel;
                            }

                            $this->pusher->trigger($publicChannel, 'update', $formattedList);
                            $this->pusher->trigger($privateChannel, 'update', $privateFormattedList);

                            $this->returnSuccess(array());
                        } else {
                            $this->returnError(implode(",", $result->messages));
                        }

                        //$syncGame = new \Clans\API\Update\SyncGame($this->_game, $playerNumber);

                        //$this->pusher->trigger($channel, "update", $syncGame->getData());

                        // Return the result
                        //echo json_encode($result);
                    } else {
                        $this->returnError('Game action not found', 404);
                    }
                } catch (\Exception $e) {
                    $this->returnError($e->getMessage());
                }
            } else {
                $this->returnError('Permission denied', 401);
            }
        } else {
            $this->returnError('ID parameter not present', 400);
        }
    }
}
