<?php
namespace Clans\API\Update;

abstract class UpdateBase
{
    /** @var integer */
    public $id;
    
    /** @var array */
    public $params = array();
    
    public function getParams() {
        return $this->params;
    }
    
    public function getData() {
        return array('u' => $this->id, 'p' => $this->params);
    }
}
