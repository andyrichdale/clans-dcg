<?php

namespace Clans\Game;

class Card extends Object {
    
    /**
     * @var \Models\Card
     */
    protected $originalCard;
    
    protected $triggers;
    
    protected $staticAbilites;
    
    /**
     * @var integer
     */
    protected $damage;
    
    /**
     * @var \Clans\Card\Token[]
     */
    protected $tokens;
    
    public function getOriginalCard() {
        return $this->originalCard;
    }

    public function setOriginalCard(\Models\Card $originalCard) {
        $this->originalCard = $originalCard;
    }

    public function getDamage() {
        return $this->damage;
    }

    public function setDamage($damage) {
        $this->damage = $damage;
    }

    public function getTokens() {
        return $this->tokens;
    }

    public function setTokens(\Clans\Card\Token $tokens) {
        $this->tokens = $tokens;
    }

}
