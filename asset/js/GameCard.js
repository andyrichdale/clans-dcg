function GameCard(gameCardID, card) {
    this.id = gameCardID;
    this.card = card;
    
    this.render = function() {
        // TODO: render active game card, IE with tokens, alterations etc
        if (this.card.id == 0) {
            // Return facedown card image
            return "<img class='card-render no-events facedown' src='/asset/images/card/clans-cardback.jpg' />";
        } else {
            // Return card render
            return "<img class='card-render no-events' src='/image.php?id=" + this.id + "&gameCard=1' data-id='" + this.id + "' />";
            //return "<img class='card-render no-events' src='/card/render/" + this.id + "?gameCard=1.png' data-id='" + this.id + "' />";
        }
    };
}