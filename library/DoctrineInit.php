<?php

class DoctrineInit
{
    static function setupDoctrine() {
        require 'Doctrine/Common/ClassLoader.php';
        require 'SQLFileLogger.php';

        $classLoader = new \Doctrine\Common\ClassLoader('Doctrine', APPLICATION_PATH . '/../library');
        $classLoader->register(); // register on SPL autoload stack

        $classLoader = new \Doctrine\Common\ClassLoader('Symfony', APPLICATION_PATH . '/../library/Doctrine');
        $classLoader->register();

        // TODO: sort autoloading
        // remove "Clans" namespace, refactor to filesystem layout?

        foreach (glob(APPLICATION_PATH . "/models/*.php") as $filename) {
            include_once $filename;
        }

        foreach (glob(APPLICATION_PATH . "/models/Game/*.php") as $filename) {
            include_once $filename;
        }

        foreach (glob(APPLICATION_PATH . "/services/*.php") as $filename) {
            include_once $filename;
        }

        foreach (glob(APPLICATION_PATH . "/Classes/*.php") as $filename) {
            include_once $filename;
        }
/*
        foreach (glob(APPLICATION_PATH . "/classes/game/*.php") as $filename) {
            include_once $filename;
        }

        foreach (glob(APPLICATION_PATH . "/classes/game/Action/*.php") as $filename) {
            include_once $filename;
        }

        foreach (glob(APPLICATION_PATH . "/classes/game/Updated/*.php") as $filename) {
            include_once $filename;
        }
    */
        foreach (glob(APPLICATION_PATH . "/Classes/API/Update/*.php") as $filename) {
            include_once $filename;
        }

        foreach (glob(APPLICATION_PATH . "/plugins/*.php") as $filename) {
            include_once $filename;
        }

        $cache = new \Doctrine\Common\Cache\ArrayCache();
        //$cache = new \Doctrine\Common\Cache\ApcCache(); PRODUCTION

        $config = new \Doctrine\ORM\Configuration();
        $config->setMetadataCacheImpl($cache);
        $driverImpl = $config->newDefaultAnnotationDriver(APPLICATION_PATH . '/entities');
        $config->setMetadataDriverImpl($driverImpl);
        $config->setQueryCacheImpl($cache);
        $config->setProxyDir(APPLICATION_PATH . '/proxies');
        $config->setProxyNamespace('Proxies');
        $logger = new SQLFileLogger();
        $config->setSQLLogger($logger);

        $config->setAutoGenerateProxyClasses(true);
        //$config->setAutoGenerateProxyClasses(false); PRODUCTION

        $options = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $resources = $options->getOption('resources');

        $connectionOptions = array(
            'driver' => $resources['db']['adapter'],
            'dbname' => $resources['db']['params']['dbname'],
            'user' => $resources['db']['params']['username'],
            'password' => $resources['db']['params']['password'],
            'host' => $resources['db']['params']['host']
        );

        $em = \Doctrine\ORM\EntityManager::create($connectionOptions, $config);

        Zend_Registry::set('em', $em);

        $tool = new \Doctrine\ORM\Tools\SchemaTool($em);

        /*
        Code to recreate db
        $classes = array(
            $em->getClassMetadata('\Models\Card'),
            $em->getClassMetadata('\Models\Chat'),
            $em->getClassMetadata('\Models\Deck'),
            $em->getClassMetadata('\Models\DeckCard'),
            $em->getClassMetadata('\Models\Effect'),
            $em->getClassMetadata('\Models\Game'),
            $em->getClassMetadata('\Models\Login'),
            $em->getClassMetadata('\Models\Match'),
            $em->getClassMetadata('\Models\Set'),
            $em->getClassMetadata('\Models\Trigger'),
            $em->getClassMetadata('\Models\Game\Card'),
            $em->getClassMetadata('\Models\Game\Token')
        );

        $tool->createSchema($classes);*/
    }
}