<?php

namespace Models;

/**
 * @Entity
 * @Table(name="chat")
 * @HasLifecycleCallbacks
 */
class Chat implements BaseModel
{
    /**
     * @Id
     * @Column(type="integer", name="chatID")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="\Models\Login", inversedBy="chatMessagesSent")
     * @JoinColumn(name="userID", referencedColumnName="id")
     * @var \Models\Login
     */
    protected $author;

    /**
     * @ManyToOne(targetEntity="\Models\Game", inversedBy="chatMessages")
     * @JoinColumn(name="gameID", referencedColumnName="gameID")
     * @var \Models\Game
     */
    protected $game;

    /**
     * @Column(type="datetime", name="created")
     */
    protected $created;

    /**
     * @Column(type="text", name="message")
     */
    protected $message;

    /**
     * @Column(type="integer", name="seen_p1")
     */
    protected $seenP1;

    /**
     * @Column(type="integer", name="seen_p2")
     */
    protected $seenP2;

    function getId() {
        return $this->id;
    }

    function getAuthor() {
        return $this->author;
    }

    function getGame() {
        return $this->game;
    }

    function getCreated() {
        return $this->created;
    }

    function getMessage() {
        return $this->message;
    }

    function setAuthor(\Models\Login $author) {
        $this->author = $author;
    }

    function setGame(\Models\Game $game) {
        $this->game = $game;
    }

    function setCreated($created) {
        $this->created = $created;
    }

    function setMessage($message) {
        $this->message = $message;
    }

    function getSeenP1() {
        return $this->seenP1;
    }

    function getSeenP2() {
        return $this->seenP2;
    }

    function setSeenP1($seenP1) {
        $this->seenP1 = $seenP1;
    }

    function setSeenP2($seenP2) {
        $this->seenP2 = $seenP2;
    }

    function toArray($shortform = false, $callingModel = null) {
        $result = array();

        $result['id'] = $this->getId();
        $result['author'] = $this->getAuthor()->toArray(true);
        $result['created'] = $this->getCreated();
        $result['message'] = $this->getMessage();

        if (!$shortform) {
            $result['game'] = $this->getGame()->toArray(true);
            $result['seenP1'] = $this->getSeenP1();
            $result['seenP2'] = $this->getSeenP2();
        }

        return $result;
    }
}