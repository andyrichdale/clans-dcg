<?php
require_once 'Doctrine/DBAL/Logging/SQLLogger.php';

/**
 * A SQL logger that logs to a sql file
 */
class SQLFileLogger implements \Doctrine\DBAL\Logging\SQLLogger
{
    const LOG_FILE = "sql_log.txt";
    const LONG_QUERY_THRESHOLD = 0.1;

    public $start = null;
    public $logString = "";

    /**
     * {@inheritdoc}
     */
    public function startQuery($sql, array $params = null, array $types = null)
    {
        $this->start = microtime(true);

    	$this->logString = PHP_EOL . $sql . PHP_EOL;

        if ($params) {
            $this->logString .= print_r($params, true);
    	}

        if ($types) {
            $this->logString .= print_r($types, true);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function stopQuery()
    {
        $time = microtime(true) - $this->start;

        $this->logString .= "TIME: " . $time . PHP_EOL;

        if ($time >= self::LONG_QUERY_THRESHOLD) {
            $this->logString .= "LONG QUERY" . PHP_EOL;
        }

        $logFile = fopen(self::LOG_FILE, 'a');
        fwrite($logFile, $this->logString);
    }
}