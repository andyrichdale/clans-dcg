<?php

namespace Clans\Game;

interface Updated
{
    public function getKey();
    
    public function getData();
}
