<?php

require_once "Zend/Controller/Action.php";
require 'DoctrineInit.php';
//require_once APPLICATION_PATH . "/models/Decks.php";
//require_once APPLICATION_PATH . "/models/rows/Cards.php";
//require_once APPLICATION_PATH . "/models/rows/Decks.php";
//require_once APPLICATION_PATH . "/classes/Filter.php";
//require_once APPLICATION_PATH . "/classes/SteamSignIn.php";
require_once "hybridauth/Hybrid/Auth.php";
require_once "hybridauth/Hybrid/User_Profile.php";
require_once "hybridauth/Hybrid/User_Contact.php";

abstract class MyAction extends Zend_Controller_Action
{
    public $loggedIn = false;

    /** @var Models\Login */
    public $login;

    public $filters;

    public $redirector;

    /** @var \Clans\Service\User */
    protected $usersService;

    /** @var \Clans\Service\Card */
    protected $cardsService;

    /** @var \Clans\Service\Effect */
    protected $effectService;

    /** @var \Clans\Service\Trigger */
    protected $triggerService;

    /** @var \Clans\Service\Set */
    protected $setsService;

    /** @var \Clans\Service\Deck */
    protected $decksService;

    /** @var \Clans\Service\Match */
    protected $matchesService;

    /** @var \Clans\Service\Game */
    protected $gamesService;

    public function init() {
        DoctrineInit::setupDoctrine();

        $this->usersService = new \Clans\Service\User();
        $this->cardsService = new \Clans\Service\Card();
        $this->effectService = new \Clans\Service\Effect();
        $this->triggerService = new  \Clans\Service\Trigger();
        $this->setsService = new  \Clans\Service\Set();
        $this->decksService = new  \Clans\Service\Deck();
        $this->matchesService = new  \Clans\Service\Match();
        $this->gamesService = new  \Clans\Service\Game();

		/*        $login = $this->usersService->getLoginByEmail('andyrichdale@gmail.com');

        echo "<h1>Login</h1>";
        echo "<p>Username: {$login->getUsername()}</p>";
        echo "<p>Selected Deck: {$login->getSelectedDeck()->getName()}</p>";

        echo "<ul>";
        foreach ($login->getSelectedDeck()->getDeckCards() as $deckCard) {
		echo "<li>" . $deckCard->getCard()->getName() . " x" . $deckCard->getQuantity() . "</li>";
        }
        echo "</ul>";

        echo "<p>Decks:</p><ul>";

        foreach ($login->getDecks() as $deck) {
		echo "<li>" . $deck->getName();
		echo "<ul>";
		foreach ($deck->getDeckCards() as $deckCard) {
		echo "<li>" . $deckCard->getCard()->getName() . " x" . $deckCard->getQuantity() . "</li>";
		}
		echo "</ul>";
		echo "</li>";
        }

        echo "</ul>";
        exit;*/

        /*$this->cardsTable = new Application_Model_Cards();
        $this->keywordsTable = new Application_Model_Keywords();
        $this->decksTable = new Application_Model_Decks();
        $this->deckCardsTable = new Application_Model_DeckCards();
        $this->loginsTable = new Application_Model_Logins();
        $this->contentTable = new Application_Model_Content();*/

        $this->redirector = $this->_helper->getHelper('Redirector');

        $this->session = new Zend_Session_Namespace('clanslcg');

        if ($this->session->login && !is_int($this->session->login)) {
            $this->session->unsetAll();
        }

        if ($this->session->login) {
            $this->login = $this->view->login = $this->usersService->getLoginByID($this->session->login);
            try {
                if ($this->login->getType() != "Website") {
                    $config = APPLICATION_PATH . '/../library/hybridauth/config.php';
                    $hybridauth = new Hybrid_Auth($config);

                    $auth = $hybridauth->authenticate($this->login->getType());

                    $this->session->loginProfile = $this->view->loginProfile = $auth->getUserProfile();
                    //$this->loggedInUser->friends = $auth->getUserContacts();
                }

            }
			catch (Exception $e) {
                die($e->getMessage());
                $this->session->unsetAll();
                if ($auth) {
                    $auth->logout();
                }
            }
        }

        $this->view->success = array();
        $this->view->errors = array();

        if ($this->getRequest()->getParam('error', false)) {
            $this->view->errors[] = $this->getRequest()->getParam('error');
        }

        $this->view->thin = false;

        $front = Zend_Controller_Front::getInstance();
        $this->controllerName = $front->getRequest()->getControllerName();
        $this->actionName = $front->getRequest()->getActionName();
        $this->view->controllerName = $this->controllerName;
        $this->view->actionName = $this->actionName;

        // Set to thin layout for colorbox popups
        if ($this->getRequest()->getParam('cb', false)) {
            $this->_helper->layout()->setLayout('thin');
            $this->view->thin = true;
        }

        // Setup filters
        $this->filters = $this->getFilters();
        $this->view->filters = $this->filters;
    }

    function getFilters() {
        $filters = array();
        $filters[] = new Clans\Filter('clan', 'Clans', array('Bain' => 'Bain',
                                                         'Monzar' => 'Monzar',
                                                         'Ronmir' => 'Ronmir',
                                                         'Galmath' => 'Galmath'));

        $filters[] = new Clans\Filter('type', 'Types', array('Ally' => 'Ally',
                                                       'Equipment' => 'Equipment',
                                                       'Structure' => 'Structure',
                                                       'Event' => 'Event'));

        /*$keywords = array();
        foreach ($this->keywordsTable->fetchAll() as $keyword) {
		$keywords[$keyword->keyword] = ucwords($keyword->keyword);
        }

        $filters[] = new Filter('keywords', 'Keywords', $keywords);*/

        return $filters;
    }
}