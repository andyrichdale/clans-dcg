<?php
require_once 'MyAction.php';
require_once 'Pusher/Pusher.php';

class ChatController extends MyAction
{
    public $pusher;

    const PUSHER_API_KEY = '643bcb47c8db76202020';
    const PUSHER_SECRET = 'd0d3d71aa508e3cb607a';
    const PUSHER_APP_ID = '6166';

    public function init() {
        parent::init();

        $this->pusher = new Pusher(self::PUSHER_API_KEY, self::PUSHER_SECRET, self::PUSHER_APP_ID);
    }
    
    public function sendAction() { 
        $errors = array();
        
        if ($this->login) {
            $message = $this->_getParam('message', false);
            $matchID = (int) $this->_getParam('matchID', 0);
            $author = $this->login->getUsername();
            $matchValid = true;
            
            if ($matchID) {
                $matchValid = false;
                if ($match = $this->matchesService->getMatchByID($matchID)) {
                    if ($match->getP1()->getId() == $this->login->getId() || $match->getP2()->getId() == $this->login->getId()) {
                        $matchValid = true;
                    }
                }
            }

            if (!$message) {
                $errors[] = 'Message empty';
            }
            if (!$author) {
                $errors[] = 'Author missing';
            }
            if (!$matchValid) {
                $errors[] = 'Invalid match';
            }
            
           // $classes = array();
            $type = 'message';
            
            if (count($errors)) {
                $author = 0;
                $message = implode(",", $errors);
                //$classes[] = 'error';
                $type = 'error';
            }
            
            /*$messageHTML = "
                    <div class='chat-row " . implode(" ", $classes) . "'>" . ($author ? "
                            <div class='chat-author'>$author: </div>" : "") . "
                            <div class='chat-message'>$message</div>
                    </div>";*/

            $data = array('message' => $message, 'author' => $author, 'type' => $type);
            
            if (count($errors)) {
                $this->pusher->trigger('private-user-' . $this->login->getId(), 'chat-message', $data);
                exit;
            }

            $channel = $matchID ? "match-$matchID" : "presence-lobby";

            $this->pusher->trigger($channel, 'chat-message', $data);

            echo $message;
        }
    	exit;
    }
}
