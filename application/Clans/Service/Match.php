<?php

namespace Clans\Service;

class Match extends BaseService
{
    public function getMatches() {
        $qb = $this->em->createQueryBuilder();
        $qb->select('m');
        $qb->from('\Models\Match', 'm');
        $qb->orderBy('m.createTime', 'DESC');

        return $this->getQbResult($qb);
    }

    /**
     * @param integer $id
     * @return \Models\Match
     */
    public function getMatchByID($id) {
        $qb = $this->em->createQueryBuilder();

        $qb->select('m');
        $qb->from('\Models\Match', 'm');

        $qb->where($qb->expr()->eq('m.id', $id));

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {

        }
    }

    /**
     * @param \Models\Login $login
     * @return \Models\Match
     */
    public function getActiveMatch(\Models\Login $login) {
        $qb = $this->em->createQueryBuilder();

        $qb->select('m');
        $qb->from('\Models\Match', 'm');

        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->orX(
                    $qb->expr()->eq('m.p1', '?1'),
                    $qb->expr()->eq('m.p2', '?1')
                ),
                $qb->expr()->eq('m.state', \Models\Match::STATE_STARTED)
            )
        );

        $qb->setParameter(1, $login);

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {

        }
    }

    /**
     * @param \Models\Login $challenger
     * @param \Models\Login $challengee
     * @return \Models\Match
     */
    public function getChallenge(\Models\Login $challenger, \Models\Login $challengee) {
        $qb = $this->em->createQueryBuilder();

        $qb->select('m');
        $qb->from('\Models\Match', 'm');

        $where = $qb->expr()->andX();

        $where->add(
            $qb->expr()->eq('m.p1', '?1')
        );

        $where->add(
            $qb->expr()->eq('m.p2', '?2')
        );

        $where->add(
            $qb->expr()->eq('m.state', \Models\Match::STATE_CHALLENGE)
        );

        $qb->where($where);

        $qb->setParameter(1, $challenger);
        $qb->setParameter(2, $challengee);

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return false;
        }
    }

    /**
     * @param \Models\Login $login
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getChallenges(\Models\Login $login) {
        $qb = $this->em->createQueryBuilder();

        $qb->select('m');
        $qb->from('\Models\Match', 'm');

        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->orX(
                    $qb->expr()->eq('m.p1', '?1'),
                    $qb->expr()->eq('m.p2', '?1')
                ),
                $qb->expr()->eq('m.state', \Models\Match::STATE_CHALLENGE)
            )
        );

        $qb->setParameter(1, $login);

        return $qb->getQuery()->getResult();
    }

    public function clearChallenges(\Models\Login $login1, \Models\Login $login2 = null) {
        $query = $this->em->createQuery();

        $or = array();
        $or[] = 'm.p1 = ?2';
        $or[] = 'm.p2 = ?2';
        $query->setParameter(2, $login1);

        if ($login2) {
            $or[] = 'm.p1 = ?3';
            $or[] = 'm.p2 = ?3';
            $query->setParameter(3, $login2);
        }

        $or = implode(" OR ", $or);

        $query->setDQL('
            DELETE FROM \Models\Match m
            WHERE m.state = ?1
                AND (
                    ' . $or . '
                )
        ');

        $query->setParameter(1, \Models\Match::STATE_CHALLENGE);

        $query->execute();
    }

    public function createChallenge(\Models\Login $p1, \Models\Login $p2) {
        $newMatch = new \Models\Match();

        $newMatch->setP1($p1);
        $newMatch->setP2($p2);
        $newMatch->setP1Time(1200);
        $newMatch->setP2Time(1200);
        $newMatch->setP1Wins(0);
        $newMatch->setP2Wins(0);
        $newMatch->setCreateTime(date("Y-m-d H:i:s"));
        $newMatch->setNumGames(3);
        $newMatch->setState(\Models\Match::STATE_CHALLENGE);
        $newMatch->setGameNumber(1);
        $newMatch->setTickTime(5);

        $this->em->persist($newMatch);
        $this->em->flush();

        return $newMatch;
    }

    public function deleteMatch(\Models\Match $match) {
        $this->em->remove($match);
        $this->em->flush();
    }

    public function saveMatch(\Models\Match $match) {
        $this->em->persist($match);
        $this->em->flush();
    }
}