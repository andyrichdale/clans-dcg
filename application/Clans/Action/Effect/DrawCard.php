<?php

namespace Clans\Action\Effect;

class DrawCard implements Clans\Action\Effect
{
    public function getRealm() {
        return \Clans\Type\Realm::REALM_PLAYER;
    }

    public function getScope() {
        return \Clans\Type\Scope::SCOPE_FRIENDLY;
    }

    public function getAffectsType() {
        return null;
    }
    
    public function getAffectsNumber() {
        return 1;
    }
    
    public function getTargets() {
        return null;
    }

    public function doEffect(\Clans\Board\BoardState $boardState, $playerNumber) {
        // TODO: insert hooks for triggers
        // TODO: are the getRealm, getScope, getAffectsType, getAffectsNumber functions neccessary?
        if ($replacementTrigger = $boardState->getReplacementTrigger($this)) {
            return $replacementTrigger->doEffect($boardState, $playerNumber);
        }
        if ($playerNumber == 1) {
            $card = array_pop($boardState->getP1Deck()->getCards());
            $boardState->addP1Hand($card);
        } else {
            $card = array_pop($boardState->getP2Deck()->getCards());
            $boardState->addP2Hand($card);
        }
        if ($trigger = $boardState->getTrigger($this)) {
            $boardState = $trigger->doEffect($boardState, $playerNumber);
        }
        return $boardState;
    }
}
