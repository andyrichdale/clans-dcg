<?php
require_once 'MyAction.php';
require_once 'Pusher/Pusher.php';

class PlayController extends MyAction
{
    public $pusher;

    const PUSHER_API_KEY = '643bcb47c8db76202020';
    const PUSHER_SECRET = 'd0d3d71aa508e3cb607a';
    const PUSHER_APP_ID = '6166';

    public function init() {
        parent::init();
        
        if (!$this->login) {
            exit;
        }

        $this->pusher = new Pusher(self::PUSHER_API_KEY, self::PUSHER_SECRET, self::PUSHER_APP_ID);
    }
    
    public function challengeAction() { 
    	$id = $this->_getParam('id', false);

        if (!$id) {
            die(json_encode(array('error' => 'Id missing')));
    	}
        
        if (!$challengee = $this->usersService->getLoginByID($id)) {
            $this->getResponse()->setHttpResponseCode(400);
            die(json_encode(array('error' => 'Could not find challengee')));
        }
        
        if ($this->matchesService->getActiveMatch($this->login)) {
            $this->getResponse()->setHttpResponseCode(400);
            die(json_encode(array('error' => 'You are already in an active match')));
        }
        
        if ($this->matchesService->getActiveMatch($challengee)) {
            $this->getResponse()->setHttpResponseCode(400);
            die(json_encode(array('error' => 'Opponent already in an active match')));
        }
        
        if ($this->matchesService->getChallenge($this->login, $challengee)) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->getResponse()->sendHeaders();
            die(json_encode(array('error' => 'You have already challenged this user')));
        }
        
        // Challenge successful, create match
        $this->matchesService->createChallenge($this->login, $challengee);
        
        $data = array(
            'challengerUsername' => $this->login->getUsername(),
            'challenger' => $this->login->getId(),
            'challengeeUsername' => $challengee->getUsername(),
            'challengee' => $challengee->getId()
        );
        
        $this->pusher->trigger("private-user-{$this->login->getId()}", "challenge", $data);
        $this->pusher->trigger("private-user-{$challengee->getId()}", "challenge", $data);
        
    	exit;
    }
    
    public function challengeAcceptAction() {
    	$id = $this->_getParam('id', false);

        if (!$id) {
            die(json_encode(array('error' => 'Id missing')));
    	}
        
        
        if ($challenger = $this->usersService->getLoginByID($id)) {
            if ($match = $this->matchesService->getChallenge($challenger, $this->login)) {
                $match->setState(\Models\Match::STATE_STARTED);
                $match->setP1Deck($match->getP1()->getSelectedDeck());
                $match->setP2Deck($match->getP2()->getSelectedDeck());
                
                for ($i = 1; $i <= $match->getNumGames(); $i++) {
                    $game = new \Models\Game($match, $i);
                    $this->gamesService->saveGame($game);
                }
                
                $this->matchesService->saveMatch($match);
                
                $this->matchesService->clearChallenges($challenger, $this->login);

                $data = array('match' => $match->getId());

                $this->pusher->trigger("private-user-{$challenger->getId()}", "match", $data);
                $this->pusher->trigger("private-user-{$this->login->getId()}", "match", $data);

            } else {
                $this->getResponse()->setHttpResponseCode(400);
                $this->getResponse()->sendHeaders();
                die(json_encode(array('error' => 'Challenge has expired')));
            }
        } else {
            die(json_encode(array('error' => 'Challenger not found')));
        }
        
    	exit;
    }
    
    public function challengeDeclineAction() {
    	$id = $this->_getParam('id', false);

        if (!$id) {
            die('Id missing');
    	}
        
        if ($challenger = $this->usersService->getLoginByID($id)) {
            if ($match = $this->matchesService->getChallenge($challenger, $this->login)) {
                $this->matchesService->deleteMatch($match);

                $data = array('challenger' => $challenger->getId(), 'challengee' => $this->login->getId());

                $this->pusher->trigger("private-user-{$challenger->getId()}", "challenge-decline", $data);
                $this->pusher->trigger("private-user-{$this->login->getId()}", "challenge-decline", $data);

            } else {
                $this->getResponse()->setHttpResponseCode(400);
                $this->getResponse()->sendHeaders();
                die(json_encode(array('error' => 'Challenge has expired')));
            }
        } else {
            die(json_encode(array('error' => 'Challenger not found')));
        }
        
    	exit;
    }
    
    public function checkStateAction() {
        if ($match = $this->matchesService->getActiveMatch($this->login)) {

            $data = array('match' => $match->getId());
            
            $this->pusher->trigger("private-user-{$this->login->getId()}", "match", $data);
            exit;
        }

        foreach ($this->matchesService->getChallenges($this->login) as $challenge) {
            /* @var $challenge \Models\Match */
            $data = array();
            if ($challenge->getP1()->getId() != $this->login->getId()) {
                if ($p1 = $this->usersService->getLoginByID($challenge->getP1()->getId())) {
                    $data['challengerUsername'] = $p1->getUsername();
                    $data['challenger'] = $p1->getId();
                } else {
                    continue;
                }
            } else {
                $data['challengerUsername'] = $this->login->getUsername();
                $data['challenger'] = $this->login->getId();
            }
            
            if ($challenge->getP2()->getId() != $this->login->getId()) {
                if ($p2 = $this->usersService->getLoginByID($challenge->getP2()->getId())) {
                    $data['challengeeUsername'] = $p2->getUsername();
                    $data['challengee'] = $p2->getId();
                } else {
                    continue;
                }
            } else {
                $data['challengeeUsername'] = $this->login->getUsername();
                $data['challengee'] = $this->login->getId();
            }
            
            $this->pusher->trigger("private-user-{$this->login->getId()}", "challenge", $data);
            exit;
        }
        
        exit;
    }
    
    public function matchAction() {
    	$id = $this->_getParam('id', false);

        if (!$id) {
            die('Id missing');
    	}
        
        if ($match = $this->matchesService->getMatchByID($id)) {
            // If match hasn't started and this is the host, start the match
            if ($match->getState() == \Models\Match::STATE_PREGAME && $this->login->getId() == $match->getP1()->getId()) {
                $match->setState(\Models\Match::STATE_STARTED);
                
                // Create board state and assign a random active player
                $boardState = new \Clans\Board\BoardState($match);
                $boardState->setActivePlayer(mt_rand(1, 2));
                
                // Draw hands
            }
            $this->view->match = $match;
        }
        
        $this->getResponse()->setHttpResponseCode(400);
        $this->getResponse()->sendHeaders();
        die(json_encode(array('error' => 'Match not found!')));
    }
}
