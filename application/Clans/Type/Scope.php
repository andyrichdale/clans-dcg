<?php

namespace Clans\Type;

class Scope
{
    const SCOPE_ANY = 0;
    const SCOPE_FRIENDLY = 1;
    const SCOPE_OPPOSING = 2;
}
