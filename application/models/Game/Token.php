<?php

namespace Models\Game;

/**
 * @Entity
 * @Table(name="game_tokens")
 * @HasLifecycleCallbacks
 */
class Token
{
    /**
     * @Id
     * @ManyToOne(targetEntity="\Models\Game\Card", inversedBy="tokens")
     * @JoinColumn(name="gameCardID", referencedColumnName="gameCardID"),
     * @var \Models\Game\Card
     */
    protected $gameCard;

    /**
     * @Id
     * @Column(type="string", name="type")
     */
    protected $type;

    /**
     * @Column(type="integer", name="number")
     */
    protected $number;

    public function getGameCard() {
        return $this->gameCard;
    }

    public function getType() {
        return $this->type;
    }

    public function getNumber() {
        return $this->number;
    }

    public function setGameCard(\Models\Game\Card $gameCard) {
        $this->gameCard = $gameCard;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function setNumber($number) {
        $this->number = $number;
    }

    public function getPublicParams() {
        return array(
            'type' => $this->getType(),
            'number' => $this->getNumber()
        );
    }

    public function getPrivateParams() {
        return array(
            'type' => 0
        );
    }
}