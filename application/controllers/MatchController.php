<?php
require_once 'MyAction.php';
require_once 'Pusher/Pusher.php';

class MatchController extends MyAction
{
    public $pusher;

    /** @var \Models\Match */
    protected $_match;

    /** @var \Models\Game */
    protected $_game;

    protected $_matchNumber;

    protected $_matchChannel;
    protected $_p1Channel;
    protected $_p2Channel;

    const PUSHER_API_KEY = '643bcb47c8db76202020';
    const PUSHER_SECRET = 'd0d3d71aa508e3cb607a';
    const PUSHER_APP_ID = '6166';

    public function init() {
        parent::init();

        if (!$this->login) {
            exit;
        }

        $this->_helper->_layout->setLayout('game');

        $this->pusher = new Pusher(self::PUSHER_API_KEY, self::PUSHER_SECRET, self::PUSHER_APP_ID);

    	$id = $this->_getParam('id', false);

        if (!$id) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->getResponse()->sendHeaders();
            die(json_encode(array('error' => 'Id missing')));
    	}

        if ($match = $this->matchesService->getMatchByID($id)) {
            $this->_match = $this->view->match = $match;
            $this->_game = $this->view->game = $match->getCurrentGame();

            $this->_p1Channel = "private-user-{$match->getP1()->getId()}";
            $this->_p2Channel = "private-user-{$match->getP2()->getId()}";

            $this->_matchChannel = "match-{$match->getId()}";

            $this->checkMatchState();

            //$this->pusher->trigger($this->_p1Channel, "match-state", $this->_match->toArray());
        } else {
            $this->getResponse()->setHttpResponseCode(400);
            $this->getResponse()->sendHeaders();
            die(json_encode(array('error' => 'Match not found!')));
        }
    }

    public function playAction() {
        $this->view->playerNumber = $this->getPlayerNumber();
    }

    public function getGameDetails($viaSockets = true) {
        if ($playerNumber == \Models\Game::PLAYER_ONE) {
            $channel = $this->_p1Channel;
        } else {
            $channel = $this->_p2Channel;
        }

        $gameDetails = new \Clans\API\Update\GameDetails($this->_game);

        if ($viaSockets) {
            $this->pusher->trigger($channel, "update", $gameDetails->getData());
        } else {
            die(json_encode($gameDetails->getData()));
        }
    }

    public function syncAllZones($playerNumber, $viaSockets = true) {
        if ($playerNumber == \Models\Game::PLAYER_ONE) {
            $channel = $this->_p1Channel;
        } else {
            $channel = $this->_p2Channel;
        }
        // Need to think about how different zones are synced (eg private/public, display vs count)
        /*$zones = array(
            \Models\Game\Card::ZONE_P1_HAND,
            \Models\Game\Card::ZONE_P1_DECK,
            \Models\Game\Card::ZONE_P1_DISCARDED,
            \Models\Game\Card::ZONE_P1_SUPPORT,
            \Models\Game\Card::ZONE_P1_ATTACK,
            \Models\Game\Card::ZONE_P2_HAND,
            \Models\Game\Card::ZONE_P2_DECK,
            \Models\Game\Card::ZONE_P2_DISCARDED,
            \Models\Game\Card::ZONE_P2_SUPPORT,
            \Models\Game\Card::ZONE_P2_ATTACK
        );*/

        /*foreach ($zones as $zone) {
            $syncZone = new \Clans\API\Update\SyncZone($this->_game, $zone, $playerNumber);
            $this->pusher->trigger($channel, "update", $syncZone->getData());
        }*/

        // TODO: replace hardcoded "update" (string from SyncGame class?
        $syncGame = new \Clans\API\Update\SyncGame($this->_game, $playerNumber);

        if ($viaSockets) {
            $this->pusher->trigger($channel, "update", $syncGame->getData());
        } else {
            die(json_encode($syncGame->getData()));
        }
    }

    protected function getPlayerNumber() {
        if ($this->_match->getP1()->getId() == $this->login->getId()) {
            return 1;
        } elseif ($this->_match->getP2()->getId() == $this->login->getId()) {
            return 2;
        } else {
            // Spectating
            return 0;
        }
    }

    protected function checkCurrentGameState() {
        switch ($this->_game->getState()) {
            case \Models\Game::STATE_PREGAME:
                // If this is the host, begin the game. If not, wait
                if ($this->getPlayerNumber() === 1) {
                    $this->gamesService->startGame($this->_game);
                }
            break;
            case \Models\Game::STATE_STARTED:

            break;
            case \Models\Game::STATE_FINISHED:

            break;
            case \Models\Game::STATE_ERROR:
                $this->getResponse()->setHttpResponseCode(400);
                $this->getResponse()->sendHeaders();
                die(json_encode(array('error' => 'Game has encountered an error!')));
            break;
        }
    }

    protected function checkMatchState() {
        switch ($this->_match->getState()) {
            case \Models\Match::STATE_STARTED:
                // Check state of current game
                $this->checkCurrentGameState();
            break;
            case \Models\Match::STATE_FINISHED:
                // Do some end of match stuff
            break;
            case \Models\Match::STATE_CHALLENGE:
                $this->getResponse()->setHttpResponseCode(400);
                $this->getResponse()->sendHeaders();
                die(json_encode(array('error' => 'Challenge has not been accepted!')));
            break;
            case \Models\Match::STATE_ERROR:
                $this->getResponse()->setHttpResponseCode(400);
                $this->getResponse()->sendHeaders();
                die(json_encode(array('error' => 'Match has encountered an error!')));
            break;
        }
    }

    public function checkStateAction() {
        $this->syncAllZones($this->getPlayerNumber(), false);
        exit;
    }

    public function getGameDetailsAction() {
        $this->getGameDetails(false);
        exit;
    }

    public function getDetailsAction($viaSockets) {
        $this->syncAllZones($this->getPlayerNumber());
        $this->getGameDetails();
        exit;
    }

    /*protected function sendUpdate() {
        // Example
        $syncZone = new \Clans\API\Update\SyncZone($this->_game, \Models\Game\Card::ZONE_P1_HAND);

        $this->pusher->trigger($this->_p1Channel, "update", $syncZone->getData());
    }*/

    // TODO: api controller for all game actions?

    public function gameActionAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        // Get the action
        if ($gameAction = $this->getGameAction($this->_getParam('game-action', false))) {
            // Execute the action
            $result = $gameAction->execute();

            // TODO: think about returning recursive updates, eg effects that trigger more effects
            // Have to merge all updates together in order

            //$update = $gameAction->getUpdated();

            //$syncGame = new \Clans\API\Update\SyncGame($this->_game, $playerNumber);

            //$this->pusher->trigger($channel, "update", $syncGame->getData());

            // Return the result
            echo json_encode($result);

        } else {
            // Game action not found, return error
            echo json_encode($this->returnFailure('Action not found'));
        }

        exit;
    }

    protected function returnFailure($message) {
        return array(
            'success' => false,
            'messages' => array($message),
            'updated' => array()
        );
    }
}
