<?php

namespace Clans\Game;

abstract class BaseAction
{
    /* @var $game \Models\Game */
    public $game;

    /* @var $updated Updated\UpdatedList */
    public $updated;

    /* @var $playerNumber int */
    public $playerNumber;

    public $messages = array();

    public function __construct(\Models\Game $game, $playerNumber, \Models\Effect $effect = null) {
        $this->game = $game;
        $this->playerNumber = $playerNumber;
        $this->updated = new Updated\UpdatedList();
    }

    /**
     * @param \Clans\Game\Models\Game $game
     * @param int $playerNumber
     * @param \Models\Effect $effect
     * @return \Clans\Game\BaseAction
     */
    public static function makeAction(\Models\Game $game, $playerNumber, \Models\Effect $effect = null) {
        // TODO: can I loop over all classes extending BaseAction instead of hardcoding keys here?

        // TODO: pass effect model?

        switch ($effect->getEffect()) {
            case "Deployed":
                return new Action\PlayCard($game, $playerNumber);
            case "DrawCard":
                return new Action\DrawCard($game, $playerNumber);
        }
    }

    public function returnSuccess() {
        $result = new Result();

        $result->success = true;
        $result->messages = $this->messages;
        $result->updatedList = $this->updated;

        return $result;
    }

    public function returnFailure() {
        $result = new Result();

        $result->success = false;
        $result->messages = $this->messages;
        $result->updatedList = array();

        return $result;
    }

    public function execute() {
        if ($this->validateAction()) {
            $this->payCosts();
            $this->doEffect();
            $this->setUpdates();
            $this->activateTriggers();

            return $this->returnSuccess();
        }

        // TODO: think about how to tell client how to show results of everything that's happened in a nice way
        // (including triggers etc

        return $this->returnFailure();
    }

    public function activateTriggers() {
        if ($this->getResolvedTriggerKey()) {
            foreach ($this->game->getInPlayCards() as $gameCard) {
                /* @var $gameCard \Models\Game\Card */
                foreach ($gameCard->getCard()->getTriggers() as $trigger) {
                    /* @var $trigger \Models\Trigger */
                    // Check if any active triggers match this action key or this action's resolved trigger key
                    if ($trigger->getTrigger() == $this->getKey() || $trigger->getTrigger() == $this->getResolvedTriggerKey()) {
                        // Activate trigger
                        $effectModel = $trigger->getEffect();
                        $action = BaseAction::makeAction($this->game, $this->playerNumber, $effectModel);
                        $action->execute();
                    }
                }
            }
        }
    }

    public function formatUpdated() {
        $formattedResult = array();

        foreach ($this->updated->getUpdated() as $update) {
            /* @var $update \Clans\Game\BaseUpdated */
            $formattedResult[$update->getKey()] = $update->getData();
        }

        return $formattedResult;
    }

    /**
     * @return Updated\UpdatedList
     */
    public function getUpdated() {
        return $this->updated;
    }

    public function getKey() {
        throw new \BadMethodCallException("Action has not implemented 'getKey' method");
    }

    public function getResolvedTriggerKey() {
        throw new \BadMethodCallException("Action has not implemented 'getResolvedTriggerKey' method");
    }

    public function validateAction() {
        throw new \BadMethodCallException("Action has not implemented 'validateAction' method");
    }

    public function doEffect() {
        throw new \BadMethodCallException("Action has not implemented 'doEffect' method");
    }

    public function payCosts() {
        throw new \BadMethodCallException("Action has not implemented 'payCosts' method");
    }

    public function setUpdates() {
        throw new \BadMethodCallException("Action has not implemented 'setUpdates' method");
    }
}
