<?php
require_once 'MyAction.php';
require_once 'simple_html_dom.php';

class IndexController extends MyAction
{
    const TWO_DAYS_IN_SECONDS = 172800;
    
    public function indexAction() {
//        if ($this->loggedInUser) {
//            $deck = $this->loggedInUser->getSelectedDeck();
//            echo "<h1>" . $deck->getName() . "</h1>";
//            echo "<ul>";
//            foreach ($deck->getDeckCards() as $deckCard) {
//                echo "<li>";
//                /* @var $deckCard Models\DeckCard */
//                echo $deckCard->getQuantity() . " " . $deckCard->getCard()->getName();
//                echo "</li>";
//            }
//            echo "</ul>";
//            exit;
//        }
       /* $client = new Zend_Http_Client();
        $client->setUri("http://clans.azurewebsites.net/photon/index/AppId/12345");
        $client->setParameterPost("posta", "hahaha");
        $client->setParameterPost("postb", "muahahaha");
        
        $client->setMethod(Zend_Http_Client::POST);
        
        $client->request();*/
    }
    
    public function newDeckAction() {
        $this->session->deck = $this->decksTable->startNewDeck($this->session->loginID);
        $this->loggedInUser->selectedDeck = $this->session->deck->deckID;
        $this->loggedInUser->save();
        
        header('Location: /');
        exit;
    }
    
    public function selectDeckAction() {
        if ($selectedDeck = $this->_getParam('selectedDeck', false)) {
            $this->loggedInUser->selectedDeck = $selectedDeck;
            $this->loggedInUser->save();
            $this->session->deck = $this->decksTable->getSelectedDeck($this->session->loginID);
        }
        
        header('Location: /');
        exit;
    }
    
    public function dataAction() {
        $this->_helper->layout()->disableLayout();
        
        $filters = $this->getRequest()->getParam('filters', array());
        $search = $this->getRequest()->getParam('search', false);
        $sort = $this->getRequest()->getParam('sort', false);
        $sortDir = $this->getRequest()->getParam('sort-dir', 'ASC');
        $view = $this->getRequest()->getParam('view', 'large-image');

        //$cards = $this->cardsTable->fetchCards($filters, $search, $sort, $sortDir);
        $cards = $this->cardsService->getCards($filters, $search, $sort, $sortDir);
        
        $this->view->cards = $cards;
        $this->view->view = $view;
    }
    
    public function exportAction() {
        $this->_helper->layout()->disableLayout();
        
        $cards = $this->cardsService->getCards();
        
        $this->view->cards = $cards;
    }
    
    public function logoutAction() {
        $config = APPLICATION_PATH . '/../library/hybridauth/config.php';
        $hybridauth = new Hybrid_Auth($config);
        
        $hybridauth->logoutAllProviders();
        $this->session->unsetAll();
        
        header('Location: /');
        exit;
    }
}
