<?php

namespace Clans\Service;

class Set extends BaseService
{
    /**
     * @param string $abbrev
     * @return \Models\Set
     */
    public function getSetByAbbrev($abbrev) {
        $query = $this->em->createQuery('SELECT s FROM \Models\Set s WHERE s.abbrev = ?1');
        $query->setParameter(1, $abbrev);
        
        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return false;
        }
    }
    
    public function getSets() {
        $qb = $this->em->createQueryBuilder();
        $qb->select('s');
        $qb->from('\Models\Set', 's');
        $qb->orderBy('s.released', 'DESC');

        return $this->getQbResult($qb);
    }
}