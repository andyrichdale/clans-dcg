<?php

namespace Clans\Type;

class Card
{
    const CARD_ALLY = 0;
    const CARD_STRUCTURE = 1;
    const CARD_EVENT = 2;
    const CARD_EQUIPMENT = 3;
}
