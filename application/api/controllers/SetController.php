<?php
require APPLICATION_PATH . '/api/controllers/BaseController.php';

class Api_SetController extends Api_BaseController
{
    /* @var $setService Clans\Service\Set */
    private $setService;
    
    public function init() {
        parent::init();
        
        $this->setService = new \Clans\Service\Set();
    }
    
    public function getAction() {
        if ($this->id) {
            $set = $this->setService->getSetByAbbrev($this->id);
            
            if ($set) {
                $this->returnSuccess($set->toArray());
            } else {
                $this->returnError('Set not found');
            }
        } else {
            $this->returnError('Abbrev not present');
        }
    }

    public function indexAction() {
        $sets = array();
        
        foreach ($this->setService->getSets() as $set) {
            $sets[] = $set->toArray(true, $set);
        }
        
        $this->returnSuccess($sets);
    }
    
    public function deleteAction() {
        // Not allowed
    }

    public function postAction() {
        // TODO: check if logged in
        
    }

    public function putAction() {
        // TODO: check if logged in
    }
}
