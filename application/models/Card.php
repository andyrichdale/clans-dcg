<?php

namespace Models;

/**
 * @Entity
 * @Table(name="cards")
 * @HasLifecycleCallbacks
 */
class Card implements BaseModel
{
    /**
     * @Id
     * @Column(type="integer", name="cardID")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="\Models\Set", inversedBy="cards")
     * @JoinColumn(name="set_abbrev", referencedColumnName="abbrev")
     * @var \Models\Set
     */
    protected $set;

    /**
     * @Column(type="integer", name="number")
     */
    protected $number;

    /**
     * @OneToMany(targetEntity="\Models\Effect", mappedBy="card", fetch="EAGER")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $effects;

    /**
     * @OneToMany(targetEntity="\Models\Trigger", mappedBy="card", fetch="EAGER")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $triggers;

    /**
     * @Column(type="text", name="name")
     */
    protected $name;

    /**
     * @Column(type="integer", name="imageId")
     */
    protected $imageId;

    /**
     * @Column(type="integer", name="cost")
     */
    protected $cost;

    /**
     * @Column(type="integer", name="speed")
     */
    protected $speed;

    /**
     * @Column(type="text", name="type")
     */
    protected $type;

    /**
     * @Column(type="text", name="clan")
     */
    protected $clan;

    /**
     * @Column(type="integer", name="attack")
     */
    protected $attack;

    /**
     * @Column(type="integer", name="defense")
     */
    protected $defence;

    /**
     * @Column(type="text", name="gametext")
     */
    protected $gametext;

    /**
     * @Column(type="text", name="lore")
     */
    protected $lore;

    public function getId() {
        return $this->id;
    }

    public function getSet() {
        return $this->set;
    }

    public function setSet(\Models\Set $set) {
        $this->set = $set;
    }

    public function getNumber() {
        return $this->number;
    }

    public function setNumber($number) {
        $this->number = (int) $number;
    }

    public function getImageId() {
        return $this->imageId;
    }

    public function setImageId($imageId) {
        $this->imageId = (int) $imageId;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getCost() {
        return $this->cost;
    }

    public function setCost($cost) {
        $this->cost = (int) $cost;
    }

    public function getSpeed() {
        return $this->speed;
    }

    public function setSpeed($speed) {
        $this->speed = $speed;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function getClan() {
        return $this->clan;
    }

    public function setClan($clan) {
        $this->clan = $clan;
    }

    public function getAttack() {
        return $this->attack;
    }

    public function setAttack($attack) {
        $this->attack = (int) $attack;
    }

    public function getDefence() {
        return $this->defence;
    }

    public function setDefence($defence) {
        $this->defence = (int) $defence;
    }

    public function getGametext() {
        if (substr($this->gametext, 0, 1) == "'") {
            return substr($this->gametext, 1);
        } else {
            return $this->gametext;
        }
    }

    public function getGametextAsHTML() {
        return str_replace(
            array(
                "->",
                "{A}",
                "{D}",
                "{R}",
                "{S}",
                "Pain",
                "Weakness",
                "Health",
                "Strength"
            ),
            array(
                "<span class='arrow-right'></span>",
                "<span class='card-icon atk-icon'></span>",
                "<span class='card-icon def-icon'></span>",
                "<span class='card-icon cost-icon'></span>",
                "<span class='card-icon speed-icon'></span>",
                "<span class='card-icon pain-icon'></span>",
                "<span class='card-icon weakness-icon'></span>",
                "<span class='card-icon health-icon'></span>",
                "<span class='card-icon strength-icon'></span>"
            ),
            nl2br($this->getGametext())
        );
    }

    public function setGametext($gametext) {
        $this->gametext = $gametext;
    }

    public function getLore() {
        return $this->lore;
    }

    public function setLore($lore) {
        $this->lore = $lore;
    }

    public function getEffects() {
        return $this->effects;
    }

    public function getTriggers() {
        return $this->triggers;
    }

    public function getPublicParams() {
        return array(
            'id' => $this->getId()
        );
    }

    public function getPrivateParams() {
        return array(
            'id' => 0
        );
    }

    public function toArray($shortform = false, $callingModel = null, $special = false) {
        $cardService = new \Clans\Service\Card();

        $result = array();

        $result['id'] = $this->getId();
        if (!$callingModel instanceof \Models\Set) {
            // TODO: fix
            $result['set'] = $this->getSet()->toArray(true);
        }
        $result['number'] = $this->getNumber();
        $result['name'] = $this->getName();
        $result['clan'] = $this->getClan();

        //if (!$shortform) {
            $result['effects'] = array();

            foreach ($this->getEffects() as $effect) {
                $result['effects'][] = $effect->toArray(true);
            }

            $result['triggers'] = array();

            foreach ($this->getTriggers() as $trigger) {
                /* @var $trigger \Models\Trigger */
                $result['triggers'][] = $trigger->toArray(true);
            }

            $result['cost'] = $this->getCost();
            $result['speed'] = $this->getSpeed();
            $result['type'] = $this->getType();
            $result['attack'] = $this->getAttack();
            $result['defence'] = $this->getDefence();
            $result['gametext'] = $this->getGametext();
            $result['lore'] = $this->getLore();
        //}

        $result['image'] = $cardService->getImageForCard($this);
        $result['hasAttack'] = $this->getType() == 'Ally';
        $result['hasDefence'] = $this->getType() == 'Ally' || $this->getType() == 'Structure';

        if ($special) {
            $newResult = array(
                'id' => $result['id'],
                'name' => $result['name'],
                'clan' => $result['clan'],
                'cost' => $result['cost'],
                'speed' => $result['speed'],
                'type' => $result['type'],
                'attack' => $result['attack'],
                'defence' => $result['defence'],
                'gametext' => $result['gametext']
            );

            $result = $newResult;
        }

        return $result;
    }
}