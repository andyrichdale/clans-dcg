<?php

namespace Clans\Game\Updated;

class UpdatedList
{
    private $updated = array();

    public function getUpdated() {
        return $this->updated;
    }

    public function addUpdated(\Clans\Game\BaseUpdated $updated) {
        $this->updated[] = $updated;
    }

    public function formattedList($private = false) {
        $formattedResult = array();

        foreach ($this->updated as $updated) {
            /* @var $updated \Clans\Game\BaseUpdated */
            $formattedResult[$updated->getKey()] = $updated->getData($private);
        }

        return $formattedResult;
    }
}
