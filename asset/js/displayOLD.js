var tableFilters = "";
var sort = "Name";
var filterText = "";
var filterTimer;

$(document).ready(function() {
    // Insert card images to DOM if card cell is visible
    setInterval(checkCardCells, 200);
    
    // Replay table filters
    $('.table-filter ul li a').click(function(e) {
        e.stopPropagation();
        
        var checkbox = $(this).find('input');
        checkbox.prop('checked', !checkbox.prop("checked"));
        
        /*var type = $(this).closest('.table-filter').attr('rel');
        var value = $(this).attr('rel');
        
        $(this).closest('.table-filter').find('button span.chosen').html($(this).html());
        $(this).closest('.table-filter').find('ul li').removeClass('active');
        $(this).parent().addClass('active');

        if (value !== -1 && value !== '-1') {
            if (type !== "sort") {
                $(this).closest('.table-filter').find('button').addClass('btn-success');
            }
        } else {
            $(this).closest('.table-filter').find('button').removeClass('btn-success');
            $(this).closest('.table-filter').find('button span.chosen').html($(this).closest('.table-filter').find('button').attr('data-original'));
        }
        
        if (type === "sort") {
            sort = value;
            redrawCards();
            return;
        }
        
        var newFilters = new Object();
        tableFilters[type] = value;
        for (i in tableFilters) {
            if (parseInt(tableFilters[i]) !== -1) {
                newFilters[i] = tableFilters[i];
            }
        }
        tableFilters = newFilters;*/
        
        $('#card-filters li').removeClass('filter-active');
        $('#filter-button').removeClass('btn-success');
        $('#card-filters input:checked').each(function() {
            $(this).closest('.table-filter').addClass('filter-active');
            $('#filter-button').addClass('btn-success');
        });
        
        tableFilters = $('#card-filters').serialize();
        
        redrawCards();
    });
    
    $('.table-filter ul li a input').click(function(e) {
        //$(this).parent().click();
        var checkbox = $(this);
        checkbox.prop('checked', !checkbox.prop("checked"));
    });
    
    $('#filter-text').on('keyup', function() {
        clearTimeout(filterTimer);
        filterTimer = setTimeout(doFilterText, 1000);
    });
    
    $('#filter-text-box .add-on').click(function() {
        $('#filter-text').val('');
        clearTimeout(filterTimer);
        filterText = '';
        redrawCards();
    });
    
    /*if ($('.card-value.notes').length) {
        $.ajax({
            url: '/index/'
        });
    }*/
    
    $('#clearFilters').click(function() {
        tableFilters = "";
        sort = "Shard";
        filterText = "";
        $('#card-filters li').removeClass('filter-active');
        $('#filter-button').removeClass('btn-success');
        $('#filter-text').val('');
        $('.table-filters input').prop('checked', false);
        redrawCards();
    });
    
    /*if (getCookie('hextcgdb_tableFilters')) {
        tableFilters = getCookie('hextcgdb_tableFilters');
        for (i in tableFilters) {
            var filter = $('.table-filter[rel="' + i + '"]');
            var chosen = filter.find('ul li a[rel="' + tableFilters[i] + '"]');
            filter.find('button span.chosen').html(chosen.html());
            filter.find('ul li').removeClass('active');
            chosen.parent().addClass('active');

            if (tableFilters[i] !== -1 && tableFilters[i] !== '-1') {
                filter.closest('.table-filter').find('button').addClass('btn-success');
            } else {
                filter.closest('.table-filter').find('button').removeClass('btn-success');
            }
        }
    }*/
    
   /* if (getCookie('hextcgdb_sort')) {
        sort = JSON.parse(getCookie('hextcgdb_sort'));
        var filter = $('.table-filter[rel="sort"]');
        var chosen = filter.find('ul li a[rel="' + sort + '"]');
        filter.find('button span.chosen').html(chosen.html());
        filter.find('ul li').removeClass('active');
        chosen.parent().addClass('active');
    }*/
    
    /*if (getCookie('hextcgdb_filterText')) {
        filterText = JSON.parse(getCookie('hextcgdb_filterText'));
        $('#filter-text').val(filterText);
    }*/
    
    redrawCards();
    
    getDeck();
});

$(window).unload(function() {
    setCookie('hextcgdb_tableFilters', JSON.stringify(tableFilters));
    setCookie('hextcgdb_sort', JSON.stringify(sort));
    setCookie('hextcgdb_filterText', JSON.stringify(filterText));
});

function getDeck() {
    $.ajax({
        url: '/deck/get-deck',
        success: function(data) {
            $('#mydeck').html(data);
            setupCardDeckButtons();
        }
    });
}

function doFilterText() {
    tableFilters = $('#card-filters').serialize();
    redrawCards();
}

function redrawCards() {
    $.ajax({
        url: '/index/data',
        type: 'post',
        data: tableFilters,
        success: function(data) {
            // Insert card html
            $('#cardgrid').html(data);
            $('.cardcell a[rel="cardgallery"]').colorbox({ iframe: true, width: "380px", height: "580px" });
            
            // Deck buttons
            $('.cardcell').hover(
                function() {
                    $(this).find('a.card-deck-button').fadeIn(150);
                },
                function() {
                    $(this).find('a.card-deck-button').fadeOut(150);
                }
            );
                
            setupCardDeckButtons();
            
            $('#grid-count').html($('.cardcell').length + ' cards found');
        }
    });
}

function setupCardDeckButtons() {
    $('.card-deck-button').click(function() {
        var cardset = $(this).attr('data-set');
        var cardnumber = $(this).attr('data-number');
        cardToDeck($(this).hasClass('add-card'), cardset, cardnumber);
    });
}

function cardToDeck(add, cardset, cardnumber) {
    $.ajax({
        url: '/deck/change',
        data: { add: add, cardset: cardset, cardnumber: cardnumber },
        success: function() {
            getDeck();
        }
    });
}

function checkCardCells() {
    $('.cardcell.inactive').each(function() {
        if (elementInViewport(this)) {
            $(this).removeClass('inactive');
            var imgHref = $(this).attr('data-image');
            $(this).find('a[rel="cardgallery"]').html('<img src="' + imgHref + '" />');
            //$(this).find('img').css('opacity', 0).animate({ 'opacity': 1 }, 1000);
        }
    });
}

function elementInViewport(el) {
    var threshold = 60;
    
    var top = el.offsetTop;
    var left = el.offsetLeft;
    var width = el.offsetWidth;
    var height = el.offsetHeight;

    while(el.offsetParent) {
        el = el.offsetParent;
        top += el.offsetTop;
        left += el.offsetLeft;
    }

    return (
        top >= (window.pageYOffset - threshold) &&
        left >= (window.pageXOffset - threshold) &&
        (top + height) <= (window.pageYOffset + window.innerHeight + threshold) &&
        (left + width) <= (window.pageXOffset + window.innerWidth + threshold)
    );
}

function setCookie(c_name,value,exdays) {
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name) {
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1)
      {
      c_start = c_value.indexOf(c_name + "=");
      }
    if (c_start == -1)
      {
      c_value = null;
      }
    else
      {
      c_start = c_value.indexOf("=", c_start) + 1;
      var c_end = c_value.indexOf(";", c_start);
      if (c_end == -1)
      {
    c_end = c_value.length;
    }
    c_value = unescape(c_value.substring(c_start,c_end));
    }
    return c_value;
}