<?php

namespace Models;

interface BaseModel
{
    /**
     * // TODO: toArray() should take into account the logged-in user's permissions
     * @return array
     */
    public function toArray($shortform = false, $callingModel = null);
}