<?php

namespace Models;

/**
 * @Entity
 * @Table(name="games")
 * @HasLifecycleCallbacks
 */
class Game implements BaseModel
{
    /**
     * @Id
     * @Column(type="integer", name="gameID")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="integer", name="game_number")
     */
    protected $gameNumber;

    /**
     * @ManyToOne(targetEntity="\Models\Match", inversedBy="games")
     * @JoinColumn(name="matchID", referencedColumnName="id")
     * @var \Models\Match
     */
    protected $match;

    /**
     * @Column(type="integer", name="state")
     */
    protected $state;

    /**
     * @OneToMany(targetEntity="\Models\Game\Card", mappedBy="game", fetch="EAGER")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $gameCards;

    /**
     * @Column(type="text", name="board_state")
     */
    protected $boardState;

    /**
     * @Column(type="integer", name="p1_life")
     */
    protected $p1Life;

    /**
     * @Column(type="integer", name="p2_life")
     */
    protected $p2Life;
    /**
     * @Column(type="integer", name="p1_resources_used")
     */
    protected $p1ResourcesUsed;

    /**
     * @Column(type="integer", name="p2_resources_used")
     */
    protected $p2ResourcesUsed;

    /**
     * @Column(type="text", name="time_started")
     */
    protected $timeStarted;

    /**
     * TODO: fix this
     * @OneToMany(targetEntity="\Models\Chat", mappedBy="game")
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $chatMessages;

    const STATE_PREGAME = 0;
    const STATE_STARTED = 1;
    const STATE_FINISHED = 2;
    const STATE_ERROR = 3;

    const PLAYER_ONE = 1;
    const PLAYER_TWO = 2;

    const STARTING_HAND_SIZE = 3;

    public function __construct(\Models\Match $match, $number) {
        $this->setMatch($match);
        $this->setGameNumber($number);
        $this->setState(self::STATE_PREGAME);
        $this->setp1Life(30);
        $this->setp2Life(30);
        $this->setBoardState("");
        $this->setP1ResourcesUsed(0);
        $this->setP2ResourcesUsed(0);
        $this->setTimeStarted(date("Y-m-d H:i:s"));
    }

    public function getId() {
        return $this->id;
    }

    public function getGameNumber() {
        return $this->gameNumber;
    }

    public function setGameNumber($gameNumber) {
        $this->gameNumber = $gameNumber;
    }

    public function getMatch() {
        return $this->match;
    }

    public function getState() {
        return $this->state;
    }

    public function getBoardState() {
        return $this->boardState;
    }

    public function getp1Life() {
        return $this->p1Life;
    }

    public function getp2Life() {
        return $this->p2Life;
    }

    public function setMatch(\Models\Match $match) {
        $this->match = $match;
    }

    public function setState($state) {
        $this->state = $state;
    }

    public function setBoardState($boardState) {
        $this->boardState = $boardState;
    }

    public function getGameCards() {
        return $this->gameCards;
    }

    public function addGameCard(\Models\Game\Card $gameCard) {
        $this->gameCards->add($gameCard);
    }

    public function setp1Life($life) {
        $this->p1Life = $life;
    }

    public function setp2Life($life) {
        $this->p2Life = $life;
    }

    function getP1ResourcesUsed() {
        return $this->p1ResourcesUsed;
    }

    function getP2ResourcesUsed() {
        return $this->p2ResourcesUsed;
    }

    function getTimeStarted() {
        // TODO: check if this is right
        return date("U", strtotime($this->timeStarted));
    }

    function setP1ResourcesUsed($p1Resources) {
        $this->p1ResourcesUsed = $p1Resources;
    }

    function setP2ResourcesUsed($p2Resources) {
        $this->p2ResourcesUsed = $p2Resources;
    }

    function setTimeStarted($timeStarted) {
        $this->timeStarted = $timeStarted;
    }

    function changePlayerResourcesUsed($playerNumber, $amountToChange) {
        $this->setPlayerResourcesUsed($playerNumber, $this->getPlayerResourcesUsed($playerNumber) + $amountToChange);
    }

    function setPlayerResourcesUsed($playerNumber, $numResources) {
        switch ($playerNumber) {
            case 1:
                $this->p1ResourcesUsed = $numResources;
                break;
            case 2:
                $this->p2ResourcesUsed = $numResources;
                break;
            default:
                break;
        }
    }

    function getPlayerResourcesUsed($playerNumber) {
        switch ($playerNumber) {
            case 1:
                return $this->getP1ResourcesUsed();
            case 2:
                return $this->getP2ResourcesUsed();
            default:
                return 0;
        }
    }

    function getTicks() {
        // Check if 'date("U")' is right
        return round((date("U") - $this->getTimeStarted()) / $this->getMatch()->getTickTime());
    }

    function getPlayerCurrentResources($playerNumber) {
        return max(0, $this->getTicks() - $this->getPlayerResourcesUsed($playerNumber));
    }

    function getPlayerLife($playerNumber) {
        switch ($playerNumber) {
            case 1:
                return $this->getp1Life();
            case 2:
                return $this->getp2Life();
            default:
                return 0;
        }
    }

    /**
     * @return \Models\Game\Trigger[]
     */
    public function getTriggersInPlay() {
        $triggers = array();
        foreach ($this->getGameCards() as $gameCard) {
            /* @var $gameCard \Models\Game\Card */
            if ($gameCard->isInPlay()) {
                foreach ($gameCard->getCard()->getTriggers() as $trigger) {
                    $triggers[] = $trigger;
                }
            }
        }
        return $triggers;
    }

    /**
     * Returns all game cards within a list of zones
     * @param array $zones
     */
    public function filterGameCardsByZone($zones) {
        return $this->getGameCards()->filter(function($gameCard) use($zones) {
            return in_array($gameCard->getZone(), $zones);
        });
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getInPlayCards() {
        return $this->filterGameCardsByZone(array(
            \Models\Game\Card::ZONE_P1_ATTACK,
            \Models\Game\Card::ZONE_P1_SUPPORT,
            \Models\Game\Card::ZONE_P2_ATTACK,
            \Models\Game\Card::ZONE_P2_SUPPORT)
        );
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getP1Deck() {
        return $this->filterGameCardsByZone(array(
            \Models\Game\Card::ZONE_P1_DECK)
        );
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getP2Deck() {
        return $this->filterGameCardsByZone(array(
            \Models\Game\Card::ZONE_P2_DECK)
        );
    }

    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    function getChatMessages() {
        return $this->chatMessages;
    }

    public function toArray($shortform = false, $callingModel = null) {
        $this->session = new \Zend_Session_Namespace('clanslcg');
        $userService = new \Clans\Service\User();

        /** @var $login \Models\Login */
        $login = $this->session->login ? $userService->getLoginByID($this->session->login) : null;

        $result = array();

        $result['id'] = $this->getId();
        $result['gameNumber'] = $this->getGameNumber();
        $result['state'] = $this->getState();

        if (!$shortform) {
            $result['match'] = $this->getMatch()->toArray(true);
            $result['gameCards'] = array();

            foreach ($this->getGameCards() as $gameCard) {
                $result['gameCards'][] = $gameCard->toArray(true);
            }

            $result['boardState'] = $this->getBoardState();
            $result['p1Life'] = $this->getp1Life();
            $result['p2Life'] = $this->getp2Life();
            $result['p1ResourcedUsed'] = $this->getP1ResourcesUsed();
            $result['p2ResourcedUsed'] = $this->getP2ResourcesUsed();
            $result['p1Resources'] = $this->getPlayerCurrentResources(1);
            $result['p2Resources'] = $this->getPlayerCurrentResources(2);
            $result['timeStarted'] = $this->getTimeStarted();
            $result['chatMessages'] = array();

            if ($login && ($login->getAdmin() || $login->getId() == $this->getMatch()->getP1()->getId() || $login->getId() == $this->getMatch()->getP2()->getId())) {
                foreach ($this->getChatMessages() as $chatMessage) {
                    $result['chatMessages'][] = $chatMessage->toArray(true);
                }
            }
        }

        return $result;
    }
}