<?php

namespace Clans\Game\Action;
/**
 * The action used when a player draws a card from their deck
 */

class DrawCard extends \Clans\Game\BaseAction implements \Clans\Game\Action
{
    /* @var $resourceCost int */
    private $resourceCost = false;

    /* @var $gameCard \Models\Game\Card */
    private $gameCard;

    function setResourceCost($resourceCost) {
        $this->resourceCost = $resourceCost;
    }

    public function getKey() {
        return "DrawCard";
    }

    public function getResolvedTriggerKey() {
        return "DrewCard";
    }

    public function validateAction() {
        // Check if resource cost is set
        if ($this->resourceCost === false) {
            $this->messages[] = "Resource cost not set";
            return false;
        }

        // Check if there are cards left in the player's deck
        /* @var $deck \Models\Deck */
        $deck = $this->playerNumber == 1 ? $this->game->getP1Deck() : $this->game->getP2Deck();
        $playerResources = $this->game->getPlayerCurrentResources($this->playerNumber);

        if (count($deck)) {
            // Check if the player has enough resources
            // TODO: use constant
            if ($playerResources >= 2) {
                // Set game card
                // TODO: make this work
                $this->gameCard = $deck->first();

                return true;
            } else {
                $this->messages[] = "Not enough resources";
                return false;
            }
        } else {
            $this->messages[] = "Deck is empty";
            return false;
        }
    }

    public function payCosts() {
        // Pay resources
        $this->game->changePlayerResourcesUsed($this->playerNumber, $this->resourceCost);
    }

    public function doEffect() {
        $gamesService = new \Clans\Service\Game();

        // Move card
        $gamesService->moveGameCardToZone($this->gameCard, $this->playerNumber == 1 ? \Models\Game\Card::ZONE_P1_HAND : \Models\Game\Card::ZONE_P2_HAND);
    }

    public function setUpdates() {
        $this->updated->addUpdated(
            new \Clans\Game\Updated\GameCardMoved(
                $this->gameCard,
                $this->playerNumber == 1 ? \Models\Game\Card::ZONE_P1_DECK : \Models\Game\Card::ZONE_P2_DECK,
                $this->playerNumber == 1 ? \Models\Game\Card::ZONE_P1_HAND : \Models\Game\Card::ZONE_P2_HAND
            )
        );

        $this->updated->addUpdated(
            new \Clans\Game\Updated\ResourcesUsed(
                $this->resourceCost * -1,
                $this->playerNumber
            )
        );
    }
}
