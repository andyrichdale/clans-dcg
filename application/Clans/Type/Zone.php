<?php

namespace Clans\Type;

class Zone
{
    const ZONE_HAND = 0;
    const ZONE_PLAY = 1;
    const ZONE_DISCARD = 2;
    const ZONE_DECK = 3;
}
