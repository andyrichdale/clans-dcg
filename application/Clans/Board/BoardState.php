<?php

namespace Clans\Board;

class BoardState {
    
    /**
     * @var integer
     */
    protected $activePlayer;
    
    /**
     * @var integer
     */
    protected $turnPhase;
    
    /**
     * @var \Models\Match
     */
    protected $match;
    
    /**
     * @var \Clans\Board\Deck
     */
    protected $p1Deck;
    
    /**
     * @var \Clans\Board\Deck
     */
    protected $p2Deck;
    
    /**
     * @var \Clans\Board\Card[]
     */
    protected $p1InPlay;
    
    /**
     * @var \Clans\Board\Card[]
     */
    protected $p2InPlay;
    
    /**
     * @var \Models\Card[]
     */
    protected $p1Hand;
    
    /**
     * @var \Models\Card[]
     */
    protected $p2Hand;
    
    const STATE_ACTIVE = 0;
    const STATE_COMBAT_DECLARE = 1;
    const STATE_COMBAT_ATTACK = 2;
    const STATE_COMBAT_DEFEND = 3;
    const STATE_END = 4;
    
    public function __construct(\Models\Match $match) {
        $this->match = $match;
        $this->activePlayer = 0;
        $this->turnPhase = self::STATE_ACTIVE;
        $this->p1Deck = new \Clans\Board\Deck($match->getP1()->getSelectedDeck());
        $this->p2Deck = new \Clans\Board\Deck($match->getP2()->getSelectedDeck());
        $this->p1InPlay = array();
        $this->p2InPlay = array();
        $this->p1Hand = array();
        $this->p2Hand = array();
    }
    
    public function getBoardState() {
        $data = array();
        //$data['match'] = 
    }
    
    public function getBoardStateForP1() {
        $data = array();
        //$data['match'] = $this->match->
    }
    
    public function getBoardStateForP2() {
        
    }
    
    public function getBoardStateForSpectator() {
        
    }
    
    public function getBoardStateForSuperSpectator() {
        
    }
    
    public function getActivePlayer() {
        return $this->activePlayer;
    }

    public function setActivePlayer($activePlayer) {
        $this->activePlayer = $activePlayer;
    }

    public function getTurnPhase() {
        return $this->turnPhase;
    }

    public function setTurnPhase($turnPhase) {
        $this->turnPhase = $turnPhase;
    }
    
    public function getMatch() {
        return $this->match;
    }

    public function setMatch(\Models\Match $match) {
        $this->match = $match;
    }

    /**
     * @return \Clans\Board\Deck
     */
    public function getP1Deck() {
        return $this->p1Deck;
    }

    public function setP1Deck(\Clans\Board\Deck $p1Deck) {
        $this->p1Deck = $p1Deck;
    }

    /**
     * @return \Clans\Board\Deck
     */
    public function getP2Deck() {
        return $this->p2Deck;
    }

    public function setP2Deck(\Clans\Board\Deck $p2Deck) {
        $this->p2Deck = $p2Deck;
    }

    public function getP1InPlay() {
        return $this->p1InPlay;
    }

    public function setP1InPlay(\Clans\Board\Card $p1InPlay) {
        $this->p1InPlay = $p1InPlay;
    }

    public function getP2InPlay() {
        return $this->p2InPlay;
    }

    public function setP2InPlay(\Clans\Board\Card $p2InPlay) {
        $this->p2InPlay = $p2InPlay;
    }

    /**
     * @return \Models\Card[]
     */
    public function getP1Hand() {
        return $this->p1Hand;
    }

    public function addP1Hand(\Models\Card $card) {
        $this->p1Hand[] = $card;
    }

    /**
     * @return \Models\Card[]
     */
    public function getP2Hand() {
        return $this->p2Hand;
    }

    public function addP2Hand(\Models\Card $card) {
        $this->p2Hand[] = $card;
    }
}

