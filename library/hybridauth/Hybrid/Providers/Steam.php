<?php

/* !
 * HybridAuth
 * http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
 * (c) 2009-2012, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html 
 */

/**
 * Hybrid_Providers_Steam provider adapter based on OpenID protocol
 * 
 * http://hybridauth.sourceforge.net/userguide/IDProvider_info_Steam.html
 */
class Hybrid_Providers_Steam extends Hybrid_Provider_Model_OpenID {

    var $openidIdentifier = "http://steamcommunity.com/openid";

    /**
     * finish login step 
     */
    function loginFinish() {
        parent::loginFinish();

        $uid = str_replace("http://steamcommunity.com/openid/id/", "", $this->user->profile->identifier);

        if ($uid) {
            $URL = "http://steamcommunity.com/profiles/{$uid}";
            
            $data = @ file_get_contents("{$URL}/?xml=1");
            $data = @ new SimpleXMLElement($data);

            if (!is_object($data)) {
                return false;
            }

            $this->user->profile->identifier = (string) $data->{'steamID64'};
            $this->user->profile->displayName = (string) $data->{'steamID'};
            $this->user->profile->photoURL = (string) $data->{'avatarMedium'};
            $this->user->profile->description = (string) $data->{'summary'};
            $this->user->profile->profileURL = $URL;

            $realname = (string) $data->{'realname'};

            if ($realname) {
                $this->user->profile->firstName = $realname;
            }

            $customURL = (string) $data->{'customURL'};

            if ($customURL) {
                $this->user->profile->webSiteURL = "http://steamcommunity.com/id/$customURL/";
            }

            // restore the user profile
            Hybrid_Auth::storage()->set("hauth_session.{$this->providerId}.user", $this->user);
        }
    }

    function getUserContacts() {
        $key = $this->config["keys"]["key"];
        $steam_id = end(explode("/", $this->user->profile->identifier));

        $contacts = array();
        
        if ($key && $steam_id) {
            $URL = "http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key=$key&steamid=$steam_id&relationship=friend";

            $client = new Zend_Http_Client($URL);
            $response = $client->request()->getBody();
            $friendsData = json_decode($response);
            
            $friends = array();
            
            foreach ($friendsData->friendslist->friends as $friend) {
                $friends[] = $friend->steamid;
            }
            
            $URL = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=$key&steamids=";
            $URL .= implode(",", $friends);
            
            $client = new Zend_Http_Client($URL);
            $response = $client->request()->getBody();
            $friendsProfiles = json_decode($response);
            
            foreach ($friendsProfiles->response->players as $player) {
                $uc = new Hybrid_User_Contact();
                
                $uc->profileURL = $player->profileurl;
                $uc->displayName = $player->personaname;
                $uc->identifier = "http://steamcommunity.com/openid/id/" . $player->steamid;
                $uc->photoURL = $player->avatarfull;

                $contacts[] = $uc;
            }
        }

        return $contacts;
    }

}