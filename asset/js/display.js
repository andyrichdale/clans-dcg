var pusher = new Pusher('643bcb47c8db76202020');

// Enable pusher logging - don't include this in production
Pusher.log = function(message) {
  if (window.console && window.console.log) window.console.log(message);
};

// Flash fallback logging - don't include this in production
WEB_SOCKET_DEBUG = true;

var tableFilters = "";
//var cardDB = {};

$(document).ready(function () {
    // Get card db
    /*$.get('/api/card', function(data) {
        for (var i = 0; i < data.length; i++) {
            var cardData = data[i];
            cardDB[cardData.id] = cardData;
        }
        console.log(cardDB);
    });*/

    // Change bg dropdown
    $('.change-bg a').click(function() {
        var img = $(this).data('img');
        if (img) {
            $('body').css('background-image', 'url(/asset/images/bgs/' + img + ')');
        }
    });
    
    // Insert card images to DOM if card cell is visible
    setInterval(checkCardCells, 200);
    
    // Replay table filters
    $('.table-filter ul li a').click(function(e) {
        e.stopPropagation();
        
        var checkbox = $(this).find('input[type="checkbox"]');
        checkbox.prop('checked', !checkbox.prop("checked"));
        $(this).find('input[type="radio"]').prop('checked', true);
        
        if ($(this).closest('#card-sort').length == 0) {
            $('#card-filter li').removeClass('filter-active');
            $('#filter-button').removeClass('btn-success');
            $('#card-filter input:checked').each(function() {
                $(this).closest('.table-filter').addClass('filter-active');
                $('#filter-button').addClass('btn-success');
            });
        }
        
        redrawCards();
    });
    
    $('.table-filter ul li a input').click(function(e) {
        var checkbox = $(this);
        checkbox.prop('checked', !checkbox.prop("checked"));
    });
    
    $('#filter-text').on('keyup', redrawCards);
    
    $('#clearFilters').click(function() {
        $('#card-filters li').removeClass('filter-active');
        $('#filter-button').removeClass('btn-success');
        $('#filter-text').val('');
        $('.table-filters input').prop('checked', false);
        $('.table-filters input[type="radio"][name="sort"][value="Clan"]').prop('checked', true);
        $('.table-filters input[type="radio"][name="sort-dir"][value="ASC"]').prop('checked', true);
        $('.table-filters input[type="radio"][name="view"][value="large-image"]').prop('checked', true);
        
        redrawCards();
    });
    
    $('#save-deck-name-button').click(function() {
        $.ajax({
            url: $(this).attr('data-url'),
            data: { deckName: $('#deck-name').val() },
            success: function(data) {
                parseJSONData(data);
            }
        });
    });
    
    $('#load-deck-button').click(function() {
        $.ajax({
            url: $(this).attr('data-url'),
            data: { loadDeck: true },
            success: function(data) {
                parseJSONData(data);
                getDeck();
                getMyDeck();
            }
        });
    });
    
    $('#publish-button, #unpublish-button').click(function() {
        $.ajax({
            url: $(this).attr('data-url'),
            data: { publish: $(this).attr('id') === 'publish-button' ? '1' : '0' },
            success: function(data) {
                parseJSONData(data);
            }
        });
    });
    
    $('#new-deck').click(function() {
        getMyDeck(0);
        return false;
    });
    
    $('.login-menu').click(function(e) {
        e.stopPropagation();
    });
            
    /*$('a[rel="deckgallery"]').colorbox({ iframe: true, width: "380px", height: "580px" });
            
    $('#deck-table-span .card-deck-button').on('click', function(e) {
        var cardset = $(this).attr('data-set');
        var cardnumber = $(this).attr('data-number');
        cardToDeck($(this).hasClass('add-card'), cardset, cardnumber);
    });*/
    
    if (getCookie('clanslcg_filters')) {
        var data = JSON.parse(getCookie('clanslcg_filters'));
        for (i in data) {
            if (data[i]['value'] != "") {
                if (data[i]['name'] == 'search') {
                    $('#card-filters').find('input[name="search"]').val(data[i]['value']);
                } else {
                    $('#card-filters').find('input[name="' + data[i]['name'] + '"][value="' + data[i]['value'] + '"]').parent().click();
                }
            }
        }
    }
    
    getDeck();
    redrawCards();
    getMyDeck();
    getMyDecks();
    
    if ($('.chat-box').length) {
        if ($('#matchID').length) {
            var match = new Match(pusher);
        } else {
            var lobby = new Lobby(pusher);
        }

        function sendChatMessage() {
            var message = $('#chat-text').val();

            $.ajax({
                url: '/chat/send',
                data: {message : message, matchID : match ? $('#matchID').val() : 0}
            });
        }
        
        $('#chat-button').click(function() {
            sendChatMessage();
            $('#chat-text').val('');
            return false;
        });
    }
    
    $('.card').hover(
        function() {
            $(this).addClass('hover');
        },
        function() {
            $(this).removeClass('hover');
        }
    );
});

function parseJSONData(data) {
    if (data !== "") {
        data = JSON.parse(data);
        if (data['success']) {
            if ($('#page-success').length === 0) {
                $('#page').prepend('<div id="page-success" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
            $('#page-success').append('<p>' + data['success'] + '</p>');
            $('.publish-button').toggle();
        }
        if (data['error']) {
            if ($('#page-errors').length === 0) {
                $('#page').prepend('<div id="page-errors" class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
            $('#page-errors').append('<p>' + data['error'] + '</p>');
        }
    }
}

window.onbeforeunload = function() {
    setCookie('clanslcg_filters', JSON.stringify($('#card-filters').serializeArray()));
};

var errorClearTimer;

$(document).ajaxError(function(event, jqxhr, settings, exception) {
    if (jqxhr.responseText) {
        var data = JSON.parse(jqxhr.responseText);
        if (data.error) {
            $('#page-errors').append($('<p></p>').html(data.error));
            $('#page-errors').show();
            clearTimeout(errorClearTimer);
            errorClearTimer = setTimeout(function() {
                $('#page-errors').fadeOut('slow', function() {
                    $('#page-errors p').remove();
                });
            }, 5000);
        }
    }
});

function getDeck(deckID) {
    if ($('#deck').length) {
        var id = $('#deck').attr('data-id');
        $.ajax({
            url: '/deck/get-deck',
            data: { id: deckID },
            success: function(data) {
                $('#deck').html(data);

                $('#deck .card-deck-button').on('click', function(e) {
                    var cardset = $(this).attr('data-set');
                    var cardnumber = $(this).attr('data-number');
                    cardToDeck($(this).hasClass('add-card'), $(this).hasClass('multiple'), cardset, cardnumber, deckID);
                });

                $('#deck a[rel="deckgallery"]').colorbox({ iframe: true, width: "380px", height: "580px" });
            }
        });
        $.ajax({
            url: '/deck/get-deck-details',
            data: { id: deckID },
            success: function(data) {
                $('#deck-details').html(data);
            }
        });
    }
}

function getMyDeck(deckID) {
    if ($('#mydeck').length) {
        var data = {};
        if (typeof deckID !== "undefined") {
            data.deckID = deckID;
        }
        $.ajax({
            url: '/deck/get-mydeck',
            data: data,
            success: function(data) {
                $('#mydeck').html(data);

                $('#mydeck .card-deck-button').on('click', function(e) {
                    var cardset = $(this).attr('data-set');
                    var cardnumber = $(this).attr('data-number');
                    cardToDeck($(this).hasClass('add-card'), $(this).hasClass('multiple'), cardset, cardnumber);
                });

                $('#mydeck a[rel="mydeckgallery"]').colorbox({ iframe: true, width: "380px", height: "580px" });

                if (deckID == 0) {
                    getMyDecks();
                }
            }
        });
    }
}

function getMyDecks() {
    $.ajax({
        url: '/deck/get-mydecks',
        success: function(data) {
            $('#choose-new-deck select').html(data);
            
            $('#choose-new-deck select').on('change', function(e) {
                var deckid = $(this).val()
                getMyDeck(deckid);
            });
        }
    });
}

function doFilterText() {
    redrawCards();
}

var redrawTimer;

function redrawCards() {
    if ($('#cardgrid').length) {
        $('#card-grid-loading-bar').show();
        $('#cardgrid').hide();
        tableFilters = $('#card-filters').serialize();
        clearTimeout(redrawTimer);
        redrawTimer = setTimeout(doRedrawCards, 500);
    }
}

function doRedrawCards() {
    $.ajax({
        url: '/api/card',
        type: 'get',
        filters: tableFilters,
        success: function(data) {
            // Insert card html
            $('#card-grid-loading-bar').hide();
            
            $('#cardgrid').html('');

            var cardTemplate = Handlebars.templates['card.tmpl'];

            for (var i = 0; i < data.length; i++) {
                var html = cardTemplate({'card': data[i]});
                $('#cardgrid').append(html);

                /*var img = $('<img></img>').addClass('card').attr('src', '/asset/images/card/renders/' + cardData.id + '.png');
                $('#cardgrid').append(img);*/
            }

            $('#cardgrid').show();
            
            $('.cardcell a[rel="cardgallery"]').colorbox({ iframe: true, width: "380px", height: "580px" });
            
            // Deck buttons
            $('.cardcell').hover(
                function() {
                    $(this).find('a.card-deck-button').fadeIn(150);
                },
                function() {
                    $(this).find('a.card-deck-button').fadeOut(150);
                }
            );
                
            $('#cardgrid .card-deck-button').on('click', function(e) {
                var id = $(this).attr('data-id');
                cardToDeck($(this).hasClass('add-card'), $(this).hasClass('multiple'), id);
            });
            
            $('#grid-count').html($('.cardcell').length + ' cards found');
        }
    });
}

function setupCardDeckButtons() {
    $('.card-deck-button').not('.live').on('click', function(e) {
        $(this).addClass('live');
        var id = $(this).attr('data-id');
        cardToDeck($(this).hasClass('add-card'), $(this).hasClass('multiple'), id);
    });
}

function cardToDeck(add, multiple, id, deckID) {
    var deckID = deckID != null ? deckID : 0;
    $.ajax({
        url: '/deck/change',
        data: { add: add, multiple: multiple, id: id, deckID: deckID },
        success: function(deckID) {
            getMyDeck();
            //getDeck();
           /* if ($('#view-deck').length == 0) {
                $('#mydeck-header .deck-links').prepend('<a id="view-deck" class="deck-link" href="/deck/view/id/' + deckID + '">View</a> | ')
            }*/
        }
    });
}

function checkCardCells() {
    $('.cardcell.inactive').each(function() {
        if (elementInViewport(this)) {
            $(this).removeClass('inactive');
            var imgHref = $(this).attr('data-image');
            var clan = $(this).data('clan');
            //$(this).find('a[rel="cardgallery"]').css('background-image', 'url(http://hextcgdb.com/asset/images/card-border.png), url("' + imgHref + '")');
            //$(this).find('a[rel="cardgallery"]').css('background-image', 'url("/asset/images/card-bg-' + clan + '-81.png"), url("' + imgHref + '")');
            /*$(this).find('a[rel="cardgallery"]').hover(
                function() {
                    $(this).css('background-image', 'url(http://hextcgdb.com/asset/images/card-border.png), url("/asset/images/card-bg-' + clan + '-noborder.png"), url("' + imgHref + '")');
                },
                function() {
                    $(this).css('background-image', 'url(http://hextcgdb.com/asset/images/card-border.png), url("' + imgHref + '")');
                }
            );*/
        }
    });
}

function elementInViewport(el) {
    var threshold = 280;
    
    var top = el.offsetTop;
    var left = el.offsetLeft;
    var width = el.offsetWidth;
    var height = el.offsetHeight;

    while(el.offsetParent) {
        el = el.offsetParent;
        top += el.offsetTop;
        left += el.offsetLeft;
    }

    return (
        top >= (window.pageYOffset - threshold) &&
        left >= (window.pageXOffset - threshold) &&
        (top + height) <= (window.pageYOffset + window.innerHeight + threshold) &&
        (left + width) <= (window.pageXOffset + window.innerWidth + threshold)
    );
}

function setCookie(c_name,value,exdays) {
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name) {
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1)
      {
      c_start = c_value.indexOf(c_name + "=");
      }
    if (c_start == -1)
      {
      c_value = null;
      }
    else
      {
      c_start = c_value.indexOf("=", c_start) + 1;
      var c_end = c_value.indexOf(";", c_start);
      if (c_end == -1)
      {
    c_end = c_value.length;
    }
    c_value = unescape(c_value.substring(c_start,c_end));
    }
    return c_value;
}

$.fn.values = function(data) {
   var inps = $(this).find(":input").get();

    if(typeof data != "object") {
       // return all data
        data = {};

        $.each(inps, function() {
            if (this.name && (this.checked
                        || /select|textarea/i.test(this.nodeName)
                        || /text|hidden|password/i.test(this.type))) {
                data[this.name] = $(this).val();
            }
        });
        return data;
    } else {
        $.each(inps, function() {
            if (this.name && data[this.name]) {
                if(this.type == "checkbox" || this.type == "radio") {
                    $(this).prop("checked", (data[this.name] == $(this).val()));
                } else {
                    $(this).val(data[this.name]);
                }
            } else if (this.type == "checkbox") {
                $(this).prop("checked", false);
            }
       });
       return $(this);
    }
};