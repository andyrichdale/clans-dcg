<?php

namespace Classes;

include_once APPLICATION_PATH . "/Classes/Action.php";

abstract class Ability extends Action
{
    /**
     * @var \Clans\Game\Player
     */
    protected $activatingPlayer;
    
    /**
     * @var \Clans\Game\Player
     */
    protected $targetingPlayer;
    
    /**
     * @var \Clans\Action\Cost[] 
     */
    protected $costs;
    
    /**
     * @var \Clans\Game\TargetRestrictions 
     */
    private $targetRestrictions;
    
    /**
     * @var \Clans\Game\Object[]
     */
    private $targetsChosen;
    
    protected $description;
    protected $restrictions;
    protected $conditions;
    
    public function __construct(\Clans\Game\Card $sourceCard, $costs) {
        $this->hostCard = $sourceCard;
        $this->costs = $costs;
        $this->targetRestrictions = null;
    }

    public function usesTargeting() {
        return $this->targetRestrictions != null;
    }
    
    /**
     * Checks whether this ability can be activated
     * @return boolean
     */
    public abstract function canPlay();
    
    /**
     * Resolve the ability
     */
    public abstract function resolve();
    
    public function canTarget(\Clans\Game\Object $object) {
        
    }
    
    /**
     * @return \Clans\Game\Player
     */
    public function getActivatingPlayer() {
        return $this->activatingPlayer;
    }

    /**
     * @param \Clans\Game\Player $activatingPlayer
     */
    public function setActivatingPlayer(\Clans\Game\Player $activatingPlayer) {
        $this->activatingPlayer = $activatingPlayer;
    }
    
    /**
     * @return \Clans\Game\Player
     */
    public function getTargetingPlayer() {
        return $this->targetingPlayer;
    }

    /**
     * @param \Clans\Game\Player $targetingPlayer
     */
    public function setTargetingPlayer(\Clans\Game\Player $targetingPlayer) {
        $this->targetingPlayer = $targetingPlayer;
    }
    
    /**
     * @return \Clans\Action\Cost[]
     */
    public function getCosts() {
        return $this->costs;
    }
    
    /**
     * @param \Clans\Action\Cost $cost
     */
    public function addCost(\Clans\Action\Cost $cost) {
        $this->costs[] = $cost;
    }
}
