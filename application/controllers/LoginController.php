<?php
require_once 'MyAction.php';
require_once 'simple_html_dom.php';

class LoginController extends MyAction
{
    const SALT = "Not actually a person";
    
    /**
     * Login from within website
     */
    public function indexAction() {
        $email = $this->getRequest()->getParam('email', false);
        $password = $this->getRequest()->getParam('password', false);
        
        if ($this->getRequest()->getMethod() == 'POST') {
            if ($email && $password) {
                $user = $this->usersService->getLoginByEmail($email);
                if ($user) {
                    if ($user->getHash() == md5($password . self::SALT)) {
                        // Login new user
                        //$deckCards = $user->getSelectedDeck()->getDeckCards();
                        //var_dump($deckCards);exit;
                        $this->session->login = $user->getId();

                        // Redirect to home page
                        header('Location: /');
                        exit;
                    } else {
                        $this->view->errors[] = "Incorrect password";
                    }
                } else {
                    $this->view->errors[] = "Could not find this email";
                }
            } else {
                $this->view->errors[] = "You must complete all fields";
            }
        }
        
        $this->view->email = $email;
    }
    
    /**
     * Register a new user
     */
    public function registerAction() {
        $this->view->title = "Register";
        
        $this->view->testSum = rand(2, 5);
        $this->view->testFirst = min($this->view->testSum, rand(0, 5));
        
        $email = $this->getRequest()->getParam('email', false);
        $password = $this->getRequest()->getParam('password', false);
        $username = $this->getRequest()->getParam('username', false);
        $test = $this->getRequest()->getParam('test', '');
        $testAnswer = $this->getRequest()->getParam('a', '');
        
        if ($this->getRequest()->getMethod() == 'POST') {
            if ($email && $password && $username) {
                if ($test == $testAnswer) {
                    if (!$this->usersService->getLoginByUsername($username)) {
                        if (!$this->usersService->getLoginByEmail($email)) {
                            // Save new user
                            $newLogin = new \Models\Login();
                            $newLogin->setType('Website');
                            $newLogin->setIdentifier('Website');
                            $newLogin->setPremium('');
                            $newLogin->setAdmin(0);
                            $newLogin->setEmail($email);
                            $newLogin->setUsername($username);
                            $newLogin->setHash(md5($password . self::SALT));
                            $newLogin->setFirstLogin(date("Y-m-d"));
                            $newLogin->setLastLogin(date("Y-m-d"));
                            
                            $this->usersService->saveLogin($newLogin);

                            // Login new user
                            $this->session->login = $newLogin->getId();

                            // Redirect to home page
                            header('Location: /');
                            exit;
                        } else {
                            $this->view->errors[] = "This email already exists, please try another";
                            $email = "";
                        }
                    } else {
                        $this->view->errors[] = "This username already exists, please try another";
                        $username = "";
                    }
                } else {
                    $this->view->errors[] = "The answer to the question was wrong, try again";
                }
            } else {
                $this->view->errors[] = "You must complete all fields";
            }
        }
        
        $this->view->email = $email;
        $this->view->username = $username;
    }
    
    /**
     * Logout user
     */
    public function logoutAction() {
        $config = APPLICATION_PATH . '/../library/hybridauth/config.php';
        $hybridauth = new Hybrid_Auth($config);
        
        $hybridauth->logoutAllProviders();
        $this->session->unsetAll();
        
        header('Location: /');
        exit;
    }
    
    /**
     * View/update user details
     */
    public function profileAction() {
        $username = $this->getRequest()->getParam('username', false);
        $email = $this->getRequest()->getParam('email', false);
        $password = $this->getRequest()->getParam('password', false);
        $passwordRetype = $this->getRequest()->getParam('password-retype', false);
        
        if ($this->loggedInUser) {
            if ($username !== false && $username !== $this->loggedInUser->username) {
                if (strlen($username) >= 3 || strlen($username) <= 32) {
                    if (!$this->loginsTable->fetchUserByType('username', $username)) {
                        $this->loggedInUser->username = $username;
                        $this->loggedInUser->save();
                        $this->view->success[] = "Username updated";
                    } else {
                        $this->view->errors[] = "Sorry, that username already exists";
                    }
                } else {
                    $this->view->errors[] = "Usernames must be between 3 and 32 characters";
                }
            }
            if ($email !== false && $email !== $this->loggedInUser->email) {
                if (!$this->loginsTable->fetchUserByType('email', $email)) {
                    $this->loggedInUser->email = $email;
                    $this->loggedInUser->save();
                    $this->view->success[] = "Email updated";
                } else {
                    $this->view->errors[] = "Sorry, that email already exists";
                }
            }
            if ($password !== false && $password !== "") {
                if (strlen($password) >= 3 || strlen($password) <= 32) {
                    if ($password === $passwordRetype) {
                        $this->loggedInUser->hash = md5($password . self::SALT);
                        $this->loggedInUser->save();
                        $this->view->success[] = "Password updated";
                    } else {
                        $this->view->errors[] = "The passwords did not match";
                    }
                } else {
                    $this->view->errors[] = "Passwords must be between 3 and 32 characters";
                }
            }
        }
        
        $this->view->email = $email;
        $this->view->username = $username;
    }
    
    /**
     * Reset pasword and email it to the user
     */
    public function forgotPasswordAction() {
        $email = $this->getRequest()->getParam('email', false);
        
        if ($email) {
            if ($user = $this->loginsTable->fetchUserByType('email', $email)) {
                // Reset password
                $newPassword = substr(md5(rand(1000, 100000) . " New pword plz"), 0, 8);
                $user->hash = md5($newPassword . self::SALT);
                $user->save();
                
                // Send email to user
                $mail = new Zend_Mail();
                $mail->setFrom('info@hextcgdb.com', 'Hex TCG DB');
                $mail->addTo($email);
                $mail->setSubject('Your password at hextcgdb.com has been reset');
                $mail->setBodyText('Your password at hextcgdb.com has been reset. Your new password is ' . $newPassword);
                $mail->send();
                
                $this->view->success[] = "Your password has been reset, check your email for the new password";
            } else {
                $this->view->errors[] = "Email not found";
            }
        }
    }
}
