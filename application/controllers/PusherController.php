<?php

require_once 'MyAction.php';
require_once 'Pusher/Pusher.php';

class PusherController extends MyAction {

    public $pusher;

    const PUSHER_API_KEY = '643bcb47c8db76202020';
    const PUSHER_SECRET = 'd0d3d71aa508e3cb607a';
    const PUSHER_APP_ID = '6166';

    public function init() {
        parent::init();

        $this->pusher = new Pusher(self::PUSHER_API_KEY, self::PUSHER_SECRET, self::PUSHER_APP_ID);
    }

    public function authAction() {
        $channel_name = $this->_getParam('channel_name', false);
        $socket_id = $this->_getParam('socket_id', false);
        
        if ($this->login) {
            $nameParts = explode("-", $channel_name);
            $channelType = isset($nameParts[1]) ? $nameParts[1] : 'chat';

            $userHTML = "<li class='user-name'>{$this->login->getUsername()}</li>";

            $userDetails = array('username' => $this->login->getUsername(),
                'userHTML' => $userHTML);

            /*if ($channelType == 'room') {
                $userDetails['host'] = false;
                $roomID = isset($nameParts[2]) ? $nameParts[2] : false;
                if ($roomID) {
                    $room = new Application_Model_Room();
                    $this->rooms->find($roomID, $room);

                    if ($this->user->userID == $room->host) {
                        $userDetails['host'] = true;
                    }
                }
            }*/

            if ($channel_name && $socket_id) {
                die($this->pusher->presence_auth($channel_name, $socket_id, $this->login->getId(), $userDetails));
            }
        } elseif ($channel_name == 'presence-lobby') {
            die($this->pusher->presence_auth($channel_name, $socket_id, 0, array('username' => 'Guest')));
        }
        exit;
    }
}

