<?php

namespace Clans;

class Filter
{
    public $name;
    public $displayName;
    public $options;
    
    public function __construct($name, $displayName, $options) {
        $this->name = $name;
        $this->displayName = $displayName;
        $this->options = $options;
    }
}
