<?php

namespace Clans\Service;

class Trigger extends BaseService
{
    public function saveTrigger(\Models\Trigger $trigger) {
        $this->em->persist($trigger);
        $this->em->flush();
        $this->em->clear();
    }
    
    /**
     * @param \Models\Card $card
     * @param integer $number
     * @return \Models\Trigger
     */
    public function getTriggerByNumber(\Models\Card $card, $number) {
        $query = $this->em->createQuery('SELECT t FROM \Models\Trigger t WHERE t.card = ?1 AND t.id = ?2');
        $query->setParameter(1, $card);
        $query->setParameter(2, $number);
        
        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return false;
        }
    }
}