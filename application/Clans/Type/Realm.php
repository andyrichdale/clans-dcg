<?php

namespace Clans\Type;

class Realm
{
    const REALM_PLAYER = 0;
    const REALM_CARD = 1;
}
